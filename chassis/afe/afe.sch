EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5150 1650 1100 3700
U 616DC966
F0 "baseplate" 50
F1 "baseplate.sch" 50
F2 "RFFQ" I L 5150 3600 50 
F3 "RFFI" I L 5150 3500 50 
F4 "RTFQ" I L 5150 3700 50 
F5 "RTFI" I L 5150 3800 50 
F6 "Q7Q" I L 5150 3350 50 
F7 "Q7I" I L 5150 3250 50 
F8 "Q6Q" I L 5150 3150 50 
F9 "Q6I" I L 5150 3050 50 
F10 "Q5Q" I L 5150 2950 50 
F11 "Q5I" I L 5150 2850 50 
F12 "Q4Q" I L 5150 2750 50 
F13 "Q4I" I L 5150 2650 50 
F14 "Q3Q" I L 5150 2500 50 
F15 "Q3I" I L 5150 2400 50 
F16 "Q2Q" I L 5150 2300 50 
F17 "Q2I" I L 5150 2200 50 
F18 "Q1Q" I L 5150 2100 50 
F19 "Q1I" I L 5150 2000 50 
F20 "Q0Q" I L 5150 1900 50 
F21 "Q0I" I L 5150 1800 50 
F22 "CLK1" O L 5150 4550 50 
F23 "CLK2" O L 5150 4650 50 
F24 "Q0D" O R 6250 1800 50 
F25 "Q1D" O R 6250 1900 50 
F26 "Q2D" O R 6250 2000 50 
F27 "Q3D" O R 6250 2100 50 
F28 "Q4D" O R 6250 2200 50 
F29 "Q5D" O R 6250 2300 50 
F30 "Q6D" O R 6250 2400 50 
F31 "Q7D" O R 6250 2500 50 
F32 "RFF" I R 6250 2650 50 
F33 "RTF" O R 6250 2750 50 
F34 "DC12V5A" I R 6250 2850 50 
F35 "USBEXTRAPOWER" I R 6250 2950 50 
F36 "USBTOCOMPUTER" I R 6250 3050 50 
F37 "USBDAISYCHAIN" I R 6250 3150 50 
$EndSheet
$Sheet
S 3500 1650 1400 3800
U 616DDDEB
F0 "fp" 50
F1 "frontpanel.sch" 50
F2 "Q0I" I L 3500 1800 50 
F3 "_Q0I" O R 4900 1800 50 
F4 "Q0Q" I L 3500 1900 50 
F5 "_Q0Q" O R 4900 1900 50 
F6 "Q1I" I L 3500 2000 50 
F7 "_Q1I" O R 4900 2000 50 
F8 "Q1Q" I L 3500 2100 50 
F9 "_Q1Q" O R 4900 2100 50 
F10 "Q2I" I L 3500 2200 50 
F11 "_Q2I" O R 4900 2200 50 
F12 "Q2Q" I L 3500 2300 50 
F13 "_Q2Q" O R 4900 2300 50 
F14 "Q3I" I L 3500 2400 50 
F15 "_Q3I" O R 4900 2400 50 
F16 "Q3Q" I L 3500 2500 50 
F17 "_Q3Q" O R 4900 2500 50 
F18 "Q4I" I L 3500 2650 50 
F19 "_Q4I" O R 4900 2650 50 
F20 "Q4Q" I L 3500 2750 50 
F21 "_Q4Q" O R 4900 2750 50 
F22 "Q5I" I L 3500 2850 50 
F23 "_Q5I" O R 4900 2850 50 
F24 "Q5Q" I L 3500 2950 50 
F25 "_Q5Q" O R 4900 2950 50 
F26 "Q6I" I L 3500 3050 50 
F27 "_Q6I" O R 4900 3050 50 
F28 "Q6Q" I L 3500 3150 50 
F29 "_Q6Q" O R 4900 3150 50 
F30 "Q7I" I L 3500 3250 50 
F31 "_Q7I" O R 4900 3250 50 
F32 "Q7Q" I L 3500 3350 50 
F33 "_Q7Q" O R 4900 3350 50 
F34 "RTFI" I L 3500 3500 50 
F35 "_RTFI" O R 4900 3500 50 
F36 "RTFQ" I L 3500 3600 50 
F37 "_RTFQ" O R 4900 3600 50 
F38 "RFFI" I L 3500 3700 50 
F39 "_RFFI" O R 4900 3700 50 
F40 "RFFQ" I L 3500 3800 50 
F41 "_RFFQ" O R 4900 3800 50 
F42 "QLO1" I L 3500 3950 50 
F43 "_QLO1" O R 4900 3950 50 
F44 "QLO2" I L 3500 4050 50 
F45 "_QLO2" O R 4900 4050 50 
F46 "QLO3" I L 3500 4150 50 
F47 "_QLO3" O R 4900 4150 50 
F48 "QLO4" I L 3500 4250 50 
F49 "_QLO4" O R 4900 4250 50 
F50 "RLO1" I L 3500 4350 50 
F51 "_RLO1" O R 4900 4350 50 
F52 "RLO2" I L 3500 4450 50 
F53 "_RLO2" O R 4900 4450 50 
F54 "CLK1" I L 3500 4550 50 
F55 "_CLK1" O R 4900 4550 50 
F56 "CLK2" I L 3500 4650 50 
F57 "_CLK2" O R 4900 4650 50 
F58 "SYSREF1" I L 3500 4750 50 
F59 "_SYSREF1" O R 4900 4750 50 
F60 "SYSREF2" I L 3500 4850 50 
F61 "_SYSREF2" O R 4900 4850 50 
F62 "SPARE1" I L 3500 5000 50 
F63 "_SPARE1" O R 4900 5000 50 
F64 "SPARE2" I L 3500 5100 50 
F65 "_SPARE2" O R 4900 5100 50 
F66 "SPARE3" I L 3500 5200 50 
F67 "_SPARE3" O R 4900 5200 50 
F68 "SPARE4" I L 3500 5300 50 
F69 "_SPARE4" O R 4900 5300 50 
$EndSheet
$Sheet
S 6500 1650 1550 3750
U 616DF26F
F0 "rp" 50
F1 "rearpanel.sch" 50
F2 "Q0D" I R 8050 1800 50 
F3 "_Q0D" O L 6500 1800 50 
F4 "Q1D" I R 8050 1900 50 
F5 "_Q1D" O L 6500 1900 50 
F6 "Q2D" I R 8050 2000 50 
F7 "_Q2D" O L 6500 2000 50 
F8 "Q3D" I R 8050 2100 50 
F9 "_Q3D" O L 6500 2100 50 
F10 "Q4D" I R 8050 2200 50 
F11 "_Q4D" O L 6500 2200 50 
F12 "Q5D" I R 8050 2300 50 
F13 "_Q5D" O L 6500 2300 50 
F14 "Q6D" I R 8050 2400 50 
F15 "_Q6D" O L 6500 2400 50 
F16 "Q7D" I R 8050 2500 50 
F17 "_Q7D" O L 6500 2500 50 
F18 "RFF" I R 8050 2650 50 
F19 "_RFF" O L 6500 2650 50 
F20 "RTF" I R 8050 2700 50 
F21 "_RTF" O L 6500 2750 50 
F22 "DC12V5A" I R 8050 2850 50 
F23 "_DC12V5A" O L 6500 2850 50 
F24 "USBEXTRAPOWER" I R 8050 2950 50 
F25 "_USBEXTRAPOWER" O L 6500 2950 50 
F26 "USBTOCOMPUTER" I R 8050 3050 50 
F27 "_USBTOCOMPUTER" I L 6500 3050 50 
F28 "USBDAISYCHAIN" I R 8050 3150 50 
F29 "_USBDAISYCHAIN" I L 6500 3150 50 
F30 "Spare1" I R 8050 3350 50 
F31 "_Spare1" O L 6500 3350 50 
F32 "Spare2" I R 8050 3450 50 
F33 "_Spare2" O L 6500 3450 50 
F34 "Spare3" I R 8050 3550 50 
F35 "_Spare3" O L 6500 3550 50 
F36 "Spare4" I R 8050 3650 50 
F37 "_Spare4" O L 6500 3650 50 
$EndSheet
Wire Wire Line
	4900 1800 5150 1800
Wire Wire Line
	4900 1900 5150 1900
Wire Wire Line
	4900 2000 5150 2000
Wire Wire Line
	4900 2100 5150 2100
Wire Wire Line
	4900 2200 5150 2200
Wire Wire Line
	4900 2300 5150 2300
Wire Wire Line
	4900 2400 5150 2400
Wire Wire Line
	4900 2500 5150 2500
Wire Wire Line
	4900 2650 5150 2650
Wire Wire Line
	4900 2750 5150 2750
Wire Wire Line
	4900 2850 5150 2850
Wire Wire Line
	4900 2950 5150 2950
Wire Wire Line
	4900 3050 5150 3050
Wire Wire Line
	4900 3150 5150 3150
Wire Wire Line
	4900 3250 5150 3250
Wire Wire Line
	4900 3350 5150 3350
Wire Wire Line
	4900 3500 5150 3500
Wire Wire Line
	4900 3600 5150 3600
Wire Wire Line
	4900 3700 5150 3700
Wire Wire Line
	4900 3800 5150 3800
Wire Wire Line
	4900 4550 5150 4550
Wire Wire Line
	4900 4650 5150 4650
Wire Wire Line
	6250 1800 6500 1800
Wire Wire Line
	6250 1900 6500 1900
Wire Wire Line
	6250 2000 6500 2000
Wire Wire Line
	6250 2100 6500 2100
Wire Wire Line
	6250 2200 6500 2200
Wire Wire Line
	6250 2300 6500 2300
Wire Wire Line
	6250 2400 6500 2400
Wire Wire Line
	6250 2500 6500 2500
Wire Wire Line
	6250 2650 6500 2650
Wire Wire Line
	6250 2750 6500 2750
Wire Wire Line
	6250 2850 6500 2850
Wire Wire Line
	6250 2950 6500 2950
Wire Wire Line
	6250 3050 6500 3050
Wire Wire Line
	6250 3150 6500 3150
NoConn ~ 4900 3950
NoConn ~ 4900 4050
NoConn ~ 4900 4150
NoConn ~ 4900 4250
NoConn ~ 4900 4350
NoConn ~ 4900 4450
NoConn ~ 4900 4750
NoConn ~ 4900 4850
NoConn ~ 4900 5000
NoConn ~ 4900 5100
NoConn ~ 4900 5200
NoConn ~ 4900 5300
NoConn ~ 3500 5300
NoConn ~ 3500 5200
NoConn ~ 3500 5100
NoConn ~ 3500 5000
NoConn ~ 3500 4850
NoConn ~ 3500 4750
NoConn ~ 3500 4650
NoConn ~ 3500 4550
NoConn ~ 3500 4450
NoConn ~ 3500 4350
NoConn ~ 3500 4250
NoConn ~ 3500 4150
NoConn ~ 3500 4050
NoConn ~ 3500 3950
NoConn ~ 3500 3800
NoConn ~ 3500 3700
NoConn ~ 3500 3600
NoConn ~ 3500 3500
NoConn ~ 3500 3350
NoConn ~ 3500 3250
NoConn ~ 3500 3150
NoConn ~ 3500 3050
NoConn ~ 3500 2950
NoConn ~ 3500 2850
NoConn ~ 3500 2750
NoConn ~ 3500 2650
NoConn ~ 3500 2500
NoConn ~ 3500 2400
NoConn ~ 3500 2300
NoConn ~ 3500 2200
NoConn ~ 3500 2100
NoConn ~ 3500 2000
NoConn ~ 3500 1900
NoConn ~ 3500 1800
NoConn ~ 6500 3350
NoConn ~ 6500 3450
NoConn ~ 6500 3550
NoConn ~ 6500 3650
NoConn ~ 8050 3650
NoConn ~ 8050 3550
NoConn ~ 8050 3450
NoConn ~ 8050 3350
NoConn ~ 8050 3150
NoConn ~ 8050 3050
NoConn ~ 8050 2950
NoConn ~ 8050 2850
NoConn ~ 8050 2700
NoConn ~ 8050 2650
NoConn ~ 8050 2500
NoConn ~ 8050 2400
NoConn ~ 8050 2300
NoConn ~ 8050 2200
NoConn ~ 8050 2100
NoConn ~ 8050 2000
NoConn ~ 8050 1900
NoConn ~ 8050 1800
$EndSCHEMATC
