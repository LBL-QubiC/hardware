EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 000_symbol:Powerdistributionboard U203
U 1 1 6170CEB6
P 4750 2300
F 0 "U203" H 5455 2135 50  0000 C CNN
F 1 "Powerdistributionboard" H 5455 2226 50  0000 C CNN
F 2 "" H 4750 2300 50  0001 C CNN
F 3 "" H 4750 2300 50  0001 C CNN
F 4 "LBNL" H 4750 2300 50  0001 C CNN "MFR"
	1    4750 2300
	-1   0    0    1   
$EndComp
Text Label 2700 2850 0    50   ~ 0
DC6V3
Wire Wire Line
	3300 2850 2700 2850
Wire Wire Line
	3300 2950 2700 2950
Text Label 2700 2950 0    50   ~ 0
DC15V
Wire Wire Line
	3300 2550 2700 2550
$Comp
L 000_symbol:LMX2595EVM B201
U 1 1 6170CED4
P 7750 1700
F 0 "B201" H 9094 2146 50  0000 L CNN
F 1 "LMX2595EVM" H 9094 2055 50  0000 L CNN
F 2 "" H 7750 1700 50  0001 C CNN
F 3 "" H 7750 1700 50  0001 C CNN
F 4 "LBL" H 7750 1700 50  0001 C CNN "MFR"
F 5 "LMX2595EVM" H 7750 1700 50  0001 C CNN "MPN"
F 6 "LMX2595EVM+3Dprinted case" H 7750 1700 50  0001 C CNN "Notes"
	1    7750 1700
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:LMX2595EVM B202
U 1 1 6170CEDA
P 7750 3550
F 0 "B202" H 9094 3996 50  0000 L CNN
F 1 "LMX2595EVM" H 9094 3905 50  0000 L CNN
F 2 "" H 7750 3550 50  0001 C CNN
F 3 "" H 7750 3550 50  0001 C CNN
F 4 "LBL" H 7750 3550 50  0001 C CNN "MFR"
F 5 "LMX2595EVM" H 7750 3550 50  0001 C CNN "MPN"
F 6 "LMX2595EVM+3Dprinted case" H 7750 3550 50  0001 C CNN "Notes"
	1    7750 3550
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Wenzel_Oscillator_501-16843 U201
U 1 1 6170CEE6
P 3400 1450
F 0 "U201" H 3925 2015 50  0000 C CNN
F 1 "Wenzel_Oscillator_501-16843" H 3925 1924 50  0000 C CNN
F 2 "" H 3400 1450 50  0001 C CNN
F 3 "" H 3400 1450 50  0001 C CNN
F 4 "Wenzel  Associates" H 3400 1450 50  0001 C CNN "MFR"
F 5 "501-16843" H 3400 1450 50  0001 C CNN "MPN"
	1    3400 1450
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:LMX2595PROG U202
U 1 1 6170CEEC
P 3800 4400
F 0 "U202" H 4200 5165 50  0000 C CNN
F 1 "LMX2595PROG" H 4200 5074 50  0000 C CNN
F 2 "" H 3800 4400 50  0001 C CNN
F 3 "" H 3800 4400 50  0001 C CNN
F 4 "LBNL" H 3800 4400 50  0001 C CNN "MFR"
	1    3800 4400
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Cable Cable201
U 1 1 6170CEFB
P 2750 1300
F 0 "Cable201" H 2975 1525 50  0000 C CNN
F 1 "Cable 15V Power pin-solder  <12in" H 2975 1434 50  0000 C CNN
F 2 "" H 2750 1300 50  0001 C CNN
F 3 "" H 2750 1300 50  0001 C CNN
F 4 "LBL" H 2750 1300 50  0001 C CNN "MFR"
F 5 "Molex 5556/45750; WM2501CT-ND" H 2750 1300 50  0001 C CNN "Digi-Key_PN"
F 6 "Molex 5556/45750 terminal" H 2750 1300 50  0001 C CNN "Notes"
	1    2750 1300
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable207
U 1 1 6170CF01
P 4650 1450
F 0 "Cable207" H 4875 1725 50  0000 C CNN
F 1 "SMAcable_MM_3in" H 4875 1634 50  0000 C CNN
F 2 "" H 4650 1450 50  0001 C CNN
F 3 "" H 4650 1450 50  0001 C CNN
F 4 "minicircuits" H 4650 1450 50  0001 C CNN "MFR"
F 5 "086-3SM+" H 4650 1450 50  0001 C CNN "MPN"
	1    4650 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 700  8100 700 
Wire Wire Line
	7550 700  7550 1000
Connection ~ 7950 700 
Wire Wire Line
	7950 700  7550 700 
Connection ~ 8100 700 
Wire Wire Line
	8100 700  7950 700 
Wire Wire Line
	7550 700  7400 700 
Connection ~ 7550 700 
Text Label 7400 700  0    50   ~ 0
PWRSPI1
Wire Wire Line
	8250 2550 8100 2550
Wire Wire Line
	7550 2550 7550 2850
Connection ~ 7950 2550
Wire Wire Line
	7950 2550 7550 2550
Connection ~ 8100 2550
Wire Wire Line
	8100 2550 7950 2550
Wire Wire Line
	7550 2550 7400 2550
Connection ~ 7550 2550
Text Label 7400 2550 0    50   ~ 0
PWRSPI2
$Comp
L 000_symbol:Cable Cable202
U 1 1 6170CF22
P 3150 4150
F 0 "Cable202" H 3375 4375 50  0000 C CNN
F 1 "Cable USB A to micro/mini? < 20 in" H 3375 4284 50  0000 C CNN
F 2 "" H 3150 4150 50  0001 C CNN
F 3 "" H 3150 4150 50  0001 C CNN
F 4 "380-1428-ND; AE10339-ND" H 3150 4150 50  0001 C CNN "Digi-Key_PN"
	1    3150 4150
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Cable Cable203
U 1 1 6170CF28
P 3150 4350
F 0 "Cable203" H 3375 4575 50  0000 C CNN
F 1 "Cable 12V power  barrel_pin < 12 in" H 3375 4484 50  0000 C CNN
F 2 "" H 3150 4350 50  0001 C CNN
F 3 "" H 3150 4350 50  0001 C CNN
F 4 "Molex 5556/45750; 10-01780" H 3150 4350 50  0001 C CNN "Digi-Key_PN"
F 5 "LBL" H 3150 4350 50  0001 C CNN "MFR"
	1    3150 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4350 2800 4350
Text Label 2800 4350 0    50   ~ 0
DC12V
Wire Wire Line
	7250 1450 7550 1450
Wire Wire Line
	7000 1500 7000 3300
$Comp
L 000_symbol:50_OHM J203
U 1 1 6170CF3E
P 7350 3450
F 0 "J203" H 7433 3675 50  0000 C CNN
F 1 "50_OHM" H 7433 3584 50  0000 C CNN
F 2 "" H 7550 3650 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 7550 3750 60  0001 L CNN
F 4 "ACX1251-ND" H 7550 3850 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 7550 3950 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 7550 4050 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 7550 4150 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 7550 4250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 7550 4350 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 7550 4450 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 7550 4550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7550 4650 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 7350 3450 50  0001 C CNN "MFR"
	1    7350 3450
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:50_OHM J202
U 1 1 6170CF4D
P 7350 1600
F 0 "J202" H 7433 1825 50  0000 C CNN
F 1 "50_OHM" H 7433 1734 50  0000 C CNN
F 2 "" H 7550 1800 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 7550 1900 60  0001 L CNN
F 4 "ACX1251-ND" H 7550 2000 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 7550 2100 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 7550 2200 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 7550 2300 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 7550 2400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 7550 2500 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 7550 2600 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 7550 2700 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7550 2800 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 7350 1600 50  0001 C CNN "MFR"
	1    7350 1600
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable213
U 1 1 6170CF53
P 6150 1550
F 0 "Cable213" H 6375 1825 50  0000 C CNN
F 1 "SMAcable_MM_8in_RA_RA" H 6375 1734 50  0000 C CNN
F 2 "" H 6150 1550 50  0001 C CNN
F 3 "" H 6150 1550 50  0001 C CNN
F 4 "minicircuits" H 6150 1550 50  0001 C CNN "MFR"
F 5 "086-8SMR+" H 6150 1550 50  0001 C CNN "MPN"
	1    6150 1550
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable214
U 1 1 6170CF59
P 6150 1750
F 0 "Cable214" H 6375 2025 50  0000 C CNN
F 1 "SMAcable_MM_8in_RA_RA" H 6375 1934 50  0000 C CNN
F 2 "" H 6150 1750 50  0001 C CNN
F 3 "" H 6150 1750 50  0001 C CNN
F 4 "minicircuits" H 6150 1750 50  0001 C CNN "MFR"
F 5 "086-8SMR+" H 6150 1750 50  0001 C CNN "MPN"
	1    6150 1750
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable212
U 1 1 6170CF5F
P 6150 1350
F 0 "Cable212" H 6375 1625 50  0000 C CNN
F 1 "SMAcable_MM_8in_RA_RA" H 6375 1534 50  0000 C CNN
F 2 "" H 6150 1350 50  0001 C CNN
F 3 "" H 6150 1350 50  0001 C CNN
F 4 "minicircuits" H 6150 1350 50  0001 C CNN "MFR"
F 5 "086-8SMR+" H 6150 1350 50  0001 C CNN "MPN"
	1    6150 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 1300 6700 1300
Wire Wire Line
	6700 1500 7000 1500
Wire Wire Line
	6850 1700 6700 1700
$Comp
L 000_symbol:50_OHM J210
U 1 1 6170CFEC
P 9250 3300
F 0 "J210" H 9333 3525 50  0000 C CNN
F 1 "50_OHM" H 9333 3434 50  0000 C CNN
F 2 "" H 9450 3500 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 3600 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 3700 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 3800 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 3900 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 4000 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 4100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 4200 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 4300 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 4400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 4500 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 3300 50  0001 C CNN "MFR"
	1    9250 3300
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J209
U 1 1 6170CFFB
P 9250 3150
F 0 "J209" H 9333 3375 50  0000 C CNN
F 1 "50_OHM" H 9333 3284 50  0000 C CNN
F 2 "" H 9450 3350 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 3450 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 3550 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 3650 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 3750 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 3850 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 3950 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 4050 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 4150 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 4250 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 4350 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 3150 50  0001 C CNN "MFR"
	1    9250 3150
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J208
U 1 1 6170D00A
P 9250 3000
F 0 "J208" H 9333 3225 50  0000 C CNN
F 1 "50_OHM" H 9333 3134 50  0000 C CNN
F 2 "" H 9450 3200 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 3300 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 3400 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 3500 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 3600 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 3700 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 3800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 3900 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 4000 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 4100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 4200 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 3000 50  0001 C CNN "MFR"
	1    9250 3000
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable216
U 1 1 6170D010
P 9150 2900
F 0 "Cable216" H 9375 3175 50  0000 C CNN
F 1 "SMAcable_MM_3in_RA_RA" H 9375 3084 50  0000 C CNN
F 2 "" H 9150 2900 50  0001 C CNN
F 3 "" H 9150 2900 50  0001 C CNN
F 4 "minicircuits" H 9150 2900 50  0001 C CNN "MFR"
F 5 "086-3SMR+" H 9150 2900 50  0001 C CNN "MPN"
	1    9150 2900
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:50_OHM J207
U 1 1 6170D025
P 9250 1450
F 0 "J207" H 9333 1675 50  0000 C CNN
F 1 "50_OHM" H 9333 1584 50  0000 C CNN
F 2 "" H 9450 1650 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 1750 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 1850 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 1950 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 2050 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 2150 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 2250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 2350 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 2450 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 2550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 2650 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 1450 50  0001 C CNN "MFR"
	1    9250 1450
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J206
U 1 1 6170D034
P 9250 1300
F 0 "J206" H 9333 1525 50  0000 C CNN
F 1 "50_OHM" H 9333 1434 50  0000 C CNN
F 2 "" H 9450 1500 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 1600 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 1700 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 1800 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 1900 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 2000 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 2100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 2200 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 2300 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 2400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 2500 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 1300 50  0001 C CNN "MFR"
	1    9250 1300
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J205
U 1 1 6170D043
P 9250 1150
F 0 "J205" H 9333 1375 50  0000 C CNN
F 1 "50_OHM" H 9333 1284 50  0000 C CNN
F 2 "" H 9450 1350 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 1450 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 1550 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 1650 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 1750 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 1850 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 1950 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 2050 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 2150 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 2250 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 2350 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 1150 50  0001 C CNN "MFR"
	1    9250 1150
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:Splitter_2way U206
U 1 1 6170D049
P 10300 1950
F 0 "U206" H 10200 2150 50  0000 R CNN
F 1 "ZX10-2-12-S+" H 10600 2250 50  0000 R CNN
F 2 "FL2227" H 10150 1500 50  0001 C CNN
F 3 "" H 10250 1600 50  0001 C CNN
F 4 "minicircuits" H 10300 1950 50  0001 C CNN "MFR"
F 5 "Zx10-2-12-S+" H 10300 1950 50  0001 C CNN "MPN"
	1    10300 1950
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable215
U 1 1 6170D04F
P 9150 1050
F 0 "Cable215" H 9375 1325 50  0000 C CNN
F 1 "SMAcable_MM_8in_RA_RA" H 9375 1234 50  0000 C CNN
F 2 "" H 9150 1050 50  0001 C CNN
F 3 "" H 9150 1050 50  0001 C CNN
F 4 "minicircuits" H 9150 1050 50  0001 C CNN "MFR"
F 5 "086-8SMR+" H 9150 1050 50  0001 C CNN "MPN"
	1    9150 1050
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Minicircuits_VBFZ-6260-S+ U207
U 1 1 6170D05B
P 10300 2850
F 0 "U207" H 10550 3075 50  0000 C CNN
F 1 "Minicircuits_VBFZ-6260-S+" H 10550 2984 50  0000 C CNN
F 2 "" H 10300 2850 50  0001 C CNN
F 3 "" H 10300 2850 50  0001 C CNN
F 4 "minicircuits" H 10300 2850 50  0001 C CNN "MFR"
F 5 "VBFZ-6262-S+" H 10300 2850 50  0001 C CNN "MPN"
	1    10300 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	10400 2850 10750 2850
Wire Wire Line
	10750 2850 10750 3350
Wire Wire Line
	10750 3350 9600 3350
Wire Wire Line
	9600 3350 9600 4000
Wire Wire Line
	9700 1000 9750 1000
Wire Wire Line
	9750 1000 9750 1950
$Comp
L 000_symbol:SMAcable_MM Cable218
U 1 1 6170D075
P 10850 1900
F 0 "Cable218" H 11075 2175 50  0000 C CNN
F 1 "SMAcable_MM_6in_RA_S" H 11075 2084 50  0000 C CNN
F 2 "" H 10850 1900 50  0001 C CNN
F 3 "" H 10850 1900 50  0001 C CNN
F 4 "minicircuits" H 10850 1900 50  0001 C CNN "MFR"
F 5 "086-6SMRSM+" H 10850 1900 50  0001 C CNN "MPN"
	1    10850 1900
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable219
U 1 1 6170D07B
P 10850 2100
F 0 "Cable219" H 11075 2375 50  0000 C CNN
F 1 "SMAcable_MM_6in_RA_S" H 11075 2284 50  0000 C CNN
F 2 "" H 10850 2100 50  0001 C CNN
F 3 "" H 10850 2100 50  0001 C CNN
F 4 "minicircuits" H 10850 2100 50  0001 C CNN "MFR"
F 5 "086-6SMRSM+" H 10850 2100 50  0001 C CNN "MPN"
	1    10850 2100
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Splitter_2way U205
U 1 1 6170D08D
P 10050 4000
F 0 "U205" H 10050 4250 50  0000 R CNN
F 1 "ZX10-2-852-S+" H 10350 4350 50  0000 R CNN
F 2 "" H 9900 3550 50  0001 C CNN
F 3 "" H 10000 3650 50  0001 C CNN
F 4 "minicircuits" H 10050 4000 50  0001 C CNN "MFR"
F 5 "ZX10-2-852-S+" H 10050 4000 50  0001 C CNN "MPN"
	1    10050 4000
	1    0    0    -1  
$EndComp
Text Label 10750 4100 0    50   ~ 0
RLO1
Text Label 10750 3900 0    50   ~ 0
RLO0
Wire Wire Line
	10500 4100 10750 4100
Wire Wire Line
	10500 3900 10750 3900
Text Label 2700 2550 0    50   ~ 0
DC12V
$Comp
L 000_symbol:50_OHM J201
U 1 1 6170D137
P 6250 1100
F 0 "J201" H 6333 1325 50  0000 C CNN
F 1 "50_OHM" H 6333 1234 50  0000 C CNN
F 2 "" H 6450 1300 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 6450 1400 60  0001 L CNN
F 4 "ACX1251-ND" H 6450 1500 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 6450 1600 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 6450 1700 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 6450 1800 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 6450 1900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 6450 2000 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 6450 2100 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 6450 2200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6450 2300 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 6250 1100 50  0001 C CNN "MFR"
	1    6250 1100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 4350 5800 4350
Wire Wire Line
	5350 4250 5800 4250
Wire Wire Line
	5350 4150 5800 4150
Text Label 5800 4350 0    50   ~ 0
PWRSPI1
Text Label 5800 4250 0    50   ~ 0
PWRSPI2
Text Label 5800 4150 0    50   ~ 0
PWRSPI3
$Comp
L 000_symbol:Cable Cable208
U 1 1 6170D143
P 4800 4150
F 0 "Cable208" H 5025 4375 50  0000 C CNN
F 1 "Cable 5 pin to 2pin SMApower, 3pinspi < 10in" H 5025 4284 50  0000 C CNN
F 2 "" H 4800 4150 50  0001 C CNN
F 3 "" H 4800 4150 50  0001 C CNN
F 4 "LBL" H 4800 4150 50  0001 C CNN "MFR"
	1    4800 4150
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Cable Cable209
U 1 1 6170D149
P 4800 4250
F 0 "Cable209" H 5025 4475 50  0000 C CNN
F 1 "Cable 5 pin to 2pin SMApower, 3pinspi < 10in" H 5025 4384 50  0000 C CNN
F 2 "" H 4800 4250 50  0001 C CNN
F 3 "" H 4800 4250 50  0001 C CNN
F 4 "LBL" H 4800 4250 50  0001 C CNN "MFR"
	1    4800 4250
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Cable Cable210
U 1 1 6170D14F
P 4800 4350
F 0 "Cable210" H 5025 4575 50  0000 C CNN
F 1 "Cable 5 pin to 2pin SMApower, 3pinspi < 10in" H 5025 4484 50  0000 C CNN
F 2 "" H 4800 4350 50  0001 C CNN
F 3 "" H 4800 4350 50  0001 C CNN
F 4 "LBL" H 4800 4350 50  0001 C CNN "MFR"
	1    4800 4350
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:USBHUB7 P201
U 1 1 6170D155
P 3300 5900
F 0 "P201" V 3746 5831 50  0000 R CNN
F 1 "USBHUB7" V 3837 5831 50  0000 R CNN
F 2 "" H 3300 5900 50  0001 C CNN
F 3 "" H 3300 5900 50  0001 C CNN
F 4 "2671-USBG-BREC307-ND" H 3300 5900 50  0001 C CNN "Digi-Key_PN"
F 5 "Coolgear" H 3300 5900 50  0001 C CNN "MFR"
F 6 "USBG-BREC307" H 3300 5900 50  0001 C CNN "MPN"
	1    3300 5900
	0    1    1    0   
$EndComp
$Comp
L 000_symbol:Cable Cable205
U 1 1 6170D15B
P 3500 5250
F 0 "Cable205" H 3725 5475 50  0000 C CNN
F 1 "Cable Power 5V barrel _solder or pin <12 in" H 3725 5384 50  0000 C CNN
F 2 "" H 3500 5250 50  0001 C CNN
F 3 "" H 3500 5250 50  0001 C CNN
F 4 "Molex 5556/45750; 10-01780" H 3500 5250 50  0001 C CNN "Digi-Key_PN"
F 5 "LBL" H 3500 5250 50  0001 C CNN "MFR"
	1    3500 5250
	0    1    1    0   
$EndComp
$Comp
L 000_symbol:Cable Cable204
U 1 1 6170D161
P 3400 5250
F 0 "Cable204" H 3625 5475 50  0000 C CNN
F 1 "Cable USB A to B ~10in" H 3700 5650 50  0000 C CNN
F 2 "" H 3400 5250 50  0001 C CNN
F 3 "" H 3400 5250 50  0001 C CNN
F 4 "Q364-ND;	Q545-ND" H 3400 5250 50  0001 C CNN "Digi-Key_PN"
	1    3400 5250
	0    1    1    0   
$EndComp
$Comp
L 000_symbol:Cable Cable206
U 1 1 6170D167
P 4200 6250
F 0 "Cable206" H 4425 6475 50  0000 C CNN
F 1 "Cable USB A to B ~10in" H 4425 6384 50  0000 C CNN
F 2 "" H 4200 6250 50  0001 C CNN
F 3 "" H 4200 6250 50  0001 C CNN
F 4 "Q364-ND;	Q545-ND" H 4200 6250 50  0001 C CNN "Digi-Key_PN"
	1    4200 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3050 4150 2800 4150
Text Label 2800 4150 0    50   ~ 0
USB1
Text Label 3650 6150 0    50   ~ 0
USB1
$Sheet
S 6350 6800 950  650 
U 617D32B4
F0 "READDNIQ" 50
F1 "DNIQ.sch" 50
F2 "IF_1" O L 6350 6900 50 
F3 "IF_2" O L 6350 7000 50 
F4 "DC6V3" I L 6350 7350 50 
F5 "LO" I L 6350 7150 50 
F6 "RF" I R 7300 6900 50 
$EndSheet
$Sheet
S 6400 9350 850  600 
U 61823757
F0 "UPIQ2" 50
F1 "UPIQ.sch" 50
F2 "IF_1" I L 6400 9450 50 
F3 "IF_2" I L 6400 9550 50 
F4 "DC6V3" I L 6400 9750 50 
F5 "LO" I L 6400 9650 50 
F6 "RF" O R 7250 9500 50 
F7 "DCPM1v8" I L 6400 9850 50 
$EndSheet
$Sheet
S 6400 10150 850  600 
U 6183EB5D
F0 "UPIQ3" 50
F1 "UPIQR.sch" 50
F2 "IF_1" I L 6400 10250 50 
F3 "IF_2" I L 6400 10350 50 
F4 "DC6V3" I L 6400 10550 50 
F5 "LO" I L 6400 10450 50 
F6 "RF" O R 7250 10300 50 
F7 "DCPM1v8" I L 6400 10650 50 
$EndSheet
$Sheet
S 8950 7700 850  600 
U 6183EB65
F0 "UPIQ4" 50
F1 "UPIQ.sch" 50
F2 "IF_1" I L 8950 7800 50 
F3 "IF_2" I L 8950 7900 50 
F4 "DC6V3" I L 8950 8100 50 
F5 "LO" I L 8950 8000 50 
F6 "RF" O R 9800 7850 50 
F7 "DCPM1v8" I L 8950 8200 50 
$EndSheet
$Sheet
S 8950 8500 850  600 
U 61843FF2
F0 "UPIQ5" 50
F1 "UPIQR.sch" 50
F2 "IF_1" I L 8950 8600 50 
F3 "IF_2" I L 8950 8700 50 
F4 "DC6V3" I L 8950 8900 50 
F5 "LO" I L 8950 8800 50 
F6 "RF" O R 9800 8650 50 
F7 "DCPM1v8" I L 8950 9000 50 
$EndSheet
$Sheet
S 8950 9300 850  600 
U 61843FFA
F0 "UPIQ6" 50
F1 "UPIQ.sch" 50
F2 "IF_1" I L 8950 9400 50 
F3 "IF_2" I L 8950 9500 50 
F4 "DC6V3" I L 8950 9700 50 
F5 "LO" I L 8950 9600 50 
F6 "RF" O R 9800 9450 50 
F7 "DCPM1v8" I L 8950 9800 50 
$EndSheet
$Sheet
S 8950 10100 850  600 
U 61844002
F0 "UPIQ7" 50
F1 "UPIQR.sch" 50
F2 "IF_1" I L 8950 10200 50 
F3 "IF_2" I L 8950 10300 50 
F4 "DC6V3" I L 8950 10500 50 
F5 "LO" I L 8950 10400 50 
F6 "RF" O R 9800 10250 50 
F7 "DCPM1v8" I L 8950 10600 50 
$EndSheet
$Sheet
S 6400 7750 850  600 
U 6184400A
F0 "UPIQ0" 50
F1 "UPIQ.sch" 50
F2 "IF_1" I L 6400 7850 50 
F3 "IF_2" I L 6400 7950 50 
F4 "DC6V3" I L 6400 8150 50 
F5 "LO" I L 6400 8050 50 
F6 "RF" O R 7250 7900 50 
F7 "DCPM1v8" I L 6400 8250 50 
$EndSheet
Text HLabel 6350 7000 0    50   Input ~ 0
RFFQ
Text HLabel 6350 6900 0    50   Input ~ 0
RFFI
Text HLabel 8900 7000 0    50   Input ~ 0
RTFQ
Text HLabel 8900 6900 0    50   Input ~ 0
RTFI
Text HLabel 8950 10300 0    50   Input ~ 0
Q7Q
Text HLabel 8950 10200 0    50   Input ~ 0
Q7I
Text HLabel 8950 9500 0    50   Input ~ 0
Q6Q
Text HLabel 8950 9400 0    50   Input ~ 0
Q6I
Text HLabel 8950 8700 0    50   Input ~ 0
Q5Q
Text HLabel 8950 8600 0    50   Input ~ 0
Q5I
Text HLabel 8950 7900 0    50   Input ~ 0
Q4Q
Text HLabel 8950 7800 0    50   Input ~ 0
Q4I
Text HLabel 6400 10350 0    50   Input ~ 0
Q3Q
Text HLabel 6400 10250 0    50   Input ~ 0
Q3I
Text HLabel 6400 9550 0    50   Input ~ 0
Q2Q
Text HLabel 6400 9450 0    50   Input ~ 0
Q2I
Text HLabel 6400 8750 0    50   Input ~ 0
Q1Q
Text HLabel 6400 8650 0    50   Input ~ 0
Q1I
Text HLabel 6400 7950 0    50   Input ~ 0
Q0Q
Text HLabel 6400 7850 0    50   Input ~ 0
Q0I
Wire Wire Line
	8950 10400 8650 10400
Wire Wire Line
	8950 9600 8650 9600
Wire Wire Line
	8950 8800 8650 8800
Wire Wire Line
	8950 8000 8650 8000
Wire Wire Line
	6400 10450 6100 10450
Wire Wire Line
	6400 9650 6100 9650
Wire Wire Line
	6400 8850 6100 8850
Wire Wire Line
	6400 8050 6100 8050
Text Label 8650 10400 0    50   ~ 0
QDRVLO7
Text Label 8650 9600 0    50   ~ 0
QDRVLO6
Text Label 8650 8800 0    50   ~ 0
QDRVLO5
Text Label 8650 8000 0    50   ~ 0
QDRVLO4
Text Label 6100 10450 0    50   ~ 0
QDRVLO3
Text Label 6100 9650 0    50   ~ 0
QDRVLO2
Text Label 6100 8850 0    50   ~ 0
QDRVLO1
Text Label 6100 8050 0    50   ~ 0
QDRVLO0
Text Label 5800 8150 0    50   ~ 0
DC6V3
Wire Wire Line
	6400 8150 5800 8150
Wire Wire Line
	6400 8250 5800 8250
Text Label 5800 8250 0    50   ~ 0
DCPM1V8
Text Label 5800 8950 0    50   ~ 0
DC6V3
Wire Wire Line
	6400 8950 5800 8950
Wire Wire Line
	6400 9050 5800 9050
Text Label 5800 9050 0    50   ~ 0
DCPM1V8
Text Label 5800 9750 0    50   ~ 0
DC6V3
Wire Wire Line
	6400 9750 5800 9750
Wire Wire Line
	6400 9850 5800 9850
Text Label 5800 9850 0    50   ~ 0
DCPM1V8
Text Label 5800 10550 0    50   ~ 0
DC6V3
Wire Wire Line
	6400 10550 5800 10550
Wire Wire Line
	6400 10650 5800 10650
Text Label 5800 10650 0    50   ~ 0
DCPM1V8
Text Label 8350 8100 0    50   ~ 0
DC6V3
Wire Wire Line
	8950 8100 8350 8100
Wire Wire Line
	8950 8200 8350 8200
Text Label 8350 8200 0    50   ~ 0
DCPM1V8
Text Label 8350 8900 0    50   ~ 0
DC6V3
Wire Wire Line
	8950 8900 8350 8900
Wire Wire Line
	8950 9000 8350 9000
Text Label 8350 9000 0    50   ~ 0
DCPM1V8
Text Label 8350 9700 0    50   ~ 0
DC6V3
Wire Wire Line
	8950 9700 8350 9700
Wire Wire Line
	8950 9800 8350 9800
Text Label 8350 9800 0    50   ~ 0
DCPM1V8
Text Label 8350 10500 0    50   ~ 0
DC6V3
Wire Wire Line
	8950 10500 8350 10500
Wire Wire Line
	8950 10600 8350 10600
Text Label 8350 10600 0    50   ~ 0
DCPM1V8
Text Label 5750 7350 0    50   ~ 0
DC6V3
Wire Wire Line
	6350 7350 5750 7350
Text Label 8300 7200 0    50   ~ 0
DC6V3
Wire Wire Line
	8900 7200 8300 7200
Wire Wire Line
	8900 7300 8300 7300
Text Label 8300 7300 0    50   ~ 0
DCPM1V8
Wire Wire Line
	3300 2750 2700 2750
Text Label 2700 2750 0    50   ~ 0
DCPM1V8
Text Label 8650 7100 2    50   ~ 0
RLO0
Wire Wire Line
	8900 7100 8650 7100
Wire Wire Line
	6350 7150 6100 7150
Text Label 6100 7150 2    50   ~ 0
RLO1
Text HLabel 11700 1850 2    50   Output ~ 0
CLK1
Text HLabel 11700 2050 2    50   Output ~ 0
CLK2
Text Label 14750 5000 0    50   ~ 0
QDRVLO0
Text Label 14750 5200 0    50   ~ 0
QDRVLO1
Text Label 14750 5400 0    50   ~ 0
QDRVLO2
Text Label 14750 5600 0    50   ~ 0
QDRVLO3
Text Label 14750 5800 0    50   ~ 0
QDRVLO4
Text Label 14750 6000 0    50   ~ 0
QDRVLO5
Text Label 14750 6200 0    50   ~ 0
QDRVLO6
Text Label 14750 6400 0    50   ~ 0
QDRVLO7
Wire Wire Line
	15050 5000 14750 5000
Wire Wire Line
	15050 5200 14750 5200
Wire Wire Line
	15050 5400 14750 5400
Wire Wire Line
	15050 5600 14750 5600
Wire Wire Line
	15050 5800 14750 5800
Wire Wire Line
	15050 6000 14750 6000
Wire Wire Line
	15050 6200 14750 6200
Wire Wire Line
	15050 6400 14750 6400
$Comp
L 000_symbol:SMAcable_MM Cable220
U 1 1 6170D067
P 11100 5350
F 0 "Cable220" H 11325 5625 50  0000 C CNN
F 1 "SMAcable_MM_3in_RA_RA" H 11325 5534 50  0000 C CNN
F 2 "" H 11100 5350 50  0001 C CNN
F 3 "" H 11100 5350 50  0001 C CNN
F 4 "minicircuits" H 11100 5350 50  0001 C CNN "MFR"
F 5 "086-3SMR+" H 11100 5350 50  0001 C CNN "MPN"
	1    11100 5350
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:Minicircuits_VBFZ-5500-S+ U208
U 1 1 6170D055
P 10300 5300
F 0 "U208" H 10550 5525 50  0000 C CNN
F 1 "Minicircuits_VBFZ-5500-S+" H 10550 5434 50  0000 C CNN
F 2 "" H 10300 5300 50  0001 C CNN
F 3 "" H 10300 5300 50  0001 C CNN
F 4 "minicircuits" H 10300 5300 50  0001 C CNN "MFR"
F 5 "VBFZ-5500-S+" H 10300 5300 50  0001 C CNN "MPN"
	1    10300 5300
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable217
U 1 1 6170D016
P 9150 5350
F 0 "Cable217" H 9375 5625 50  0000 C CNN
F 1 "SMAcable_MM_3in_RA_RA" H 9375 5534 50  0000 C CNN
F 2 "" H 9150 5350 50  0001 C CNN
F 3 "" H 9150 5350 50  0001 C CNN
F 4 "minicircuits" H 9150 5350 50  0001 C CNN "MFR"
F 5 "086-3SMR+" H 9150 5350 50  0001 C CNN "MPN"
	1    9150 5350
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:50_OHM J211
U 1 1 6170CFD5
P 9250 5450
F 0 "J211" H 9333 5675 50  0000 C CNN
F 1 "50_OHM" H 9333 5584 50  0000 C CNN
F 2 "" H 9450 5650 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 5750 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 5850 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 5950 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 6050 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 6150 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 6250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 6350 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 6450 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 6550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 6650 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 5450 50  0001 C CNN "MFR"
	1    9250 5450
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J212
U 1 1 6170CFC6
P 9250 5600
F 0 "J212" H 9333 5825 50  0000 C CNN
F 1 "50_OHM" H 9333 5734 50  0000 C CNN
F 2 "" H 9450 5800 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 5900 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 6000 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 6100 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 6200 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 6300 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 6400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 6500 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 6600 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 6700 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 6800 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 5600 50  0001 C CNN "MFR"
	1    9250 5600
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J213
U 1 1 6170CFB7
P 9250 5750
F 0 "J213" H 9333 5975 50  0000 C CNN
F 1 "50_OHM" H 9333 5884 50  0000 C CNN
F 2 "" H 9450 5950 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 6050 60  0001 L CNN
F 4 "ACX1251-ND" H 9450 6150 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 9450 6250 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9450 6350 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 9450 6450 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 9450 6550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 9450 6650 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 9450 6750 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 9450 6850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9450 6950 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 9250 5750 50  0001 C CNN "MFR"
	1    9250 5750
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J216
U 1 1 6170CFA8
P 12700 5400
F 0 "J216" H 12783 5625 50  0000 C CNN
F 1 "50_OHM" H 12783 5534 50  0000 C CNN
F 2 "" H 12900 5600 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 12900 5700 60  0001 L CNN
F 4 "ACX1251-ND" H 12900 5800 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 12900 5900 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 12900 6000 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 12900 6100 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 12900 6200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 12900 6300 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 12900 6400 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 12900 6500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 12900 6600 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 12700 5400 50  0001 C CNN "MFR"
	1    12700 5400
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J215
U 1 1 6170CF99
P 12700 5200
F 0 "J215" H 12783 5425 50  0000 C CNN
F 1 "50_OHM" H 12783 5334 50  0000 C CNN
F 2 "" H 12900 5400 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 12900 5500 60  0001 L CNN
F 4 "ACX1251-ND" H 12900 5600 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 12900 5700 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 12900 5800 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 12900 5900 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 12900 6000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 12900 6100 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 12900 6200 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 12900 6300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 12900 6400 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 12700 5200 50  0001 C CNN "MFR"
	1    12700 5200
	-1   0    0    1   
$EndComp
$Comp
L 000_symbol:50_OHM J214
U 1 1 6170CF8A
P 12700 5000
F 0 "J214" H 12783 5225 50  0000 C CNN
F 1 "50_OHM" H 12783 5134 50  0000 C CNN
F 2 "" H 12900 5200 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 12900 5300 60  0001 L CNN
F 4 "ACX1251-ND" H 12900 5400 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 12900 5500 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 12900 5600 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 12900 5700 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 12900 5800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 12900 5900 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 12900 6000 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 12900 6100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 12900 6200 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 12700 5000 50  0001 C CNN "MFR"
	1    12700 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 5750 7550 5750
$Comp
L 000_symbol:50_OHM J204
U 1 1 6170CF71
P 7350 5900
F 0 "J204" H 7433 6125 50  0000 C CNN
F 1 "50_OHM" H 7433 6034 50  0000 C CNN
F 2 "" H 7550 6100 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 7550 6200 60  0001 L CNN
F 4 "ACX1251-ND" H 7550 6300 60  0001 L CNN "Digi-Key_PN"
F 5 "132360" H 7550 6400 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 7550 6500 60  0001 L CNN "Category"
F 7 "Coaxial Connectors (RF)" H 7550 6600 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1909763&DocType=Customer+Drawing&DocLang=English" H 7550 6700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/1909763-1/A118077CT-ND/4729711" H 7550 6800 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN UMCC JACK STR 50OHM SMD" H 7550 6900 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 7550 7000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7550 7100 60  0001 L CNN "Status"
F 13 "Amphenol RF" H 7350 5900 50  0001 C CNN "MFR"
	1    7350 5900
	1    0    0    -1  
$EndComp
Text Label 7400 5000 0    50   ~ 0
PWRSPI3
Connection ~ 7550 5000
Wire Wire Line
	7550 5000 7400 5000
Wire Wire Line
	8100 5000 7950 5000
Connection ~ 8100 5000
Wire Wire Line
	7950 5000 7550 5000
Connection ~ 7950 5000
Wire Wire Line
	7550 5000 7550 5300
Wire Wire Line
	8250 5000 8100 5000
$Comp
L 000_symbol:LMX2595EVM B203
U 1 1 6170CEE0
P 7750 6000
F 0 "B203" H 9094 6446 50  0000 L CNN
F 1 "LMX2595EVM" H 9094 6355 50  0000 L CNN
F 2 "" H 7750 6000 50  0001 C CNN
F 3 "" H 7750 6000 50  0001 C CNN
F 4 "LBL" H 7750 6000 50  0001 C CNN "MFR"
F 5 "LMX2595EVM" H 7750 6000 50  0001 C CNN "MPN"
F 6 "LMX2595EVM+3Dprinted case" H 7750 6000 50  0001 C CNN "Notes"
	1    7750 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 1700 6850 5750
Text HLabel 7250 7900 2    50   Output ~ 0
Q0D
Text HLabel 7250 8700 2    50   Output ~ 0
Q1D
Text HLabel 7250 9500 2    50   Output ~ 0
Q2D
Text HLabel 7250 10300 2    50   Output ~ 0
Q3D
Text HLabel 9800 7850 2    50   Output ~ 0
Q4D
Text HLabel 9800 8650 2    50   Output ~ 0
Q5D
Text HLabel 9800 9450 2    50   Output ~ 0
Q6D
Text HLabel 9800 10250 2    50   Output ~ 0
Q7D
Text HLabel 7300 6900 2    50   Input ~ 0
RFF
Text HLabel 9750 6950 2    50   Output ~ 0
RTF
Text HLabel 5500 2950 2    50   Input ~ 0
DC12V5A
Text HLabel 2800 5000 0    50   Input ~ 0
USBEXTRAPOWER
Text HLabel 2900 5150 0    50   Input ~ 0
USBTOCOMPUTER
Text HLabel 4650 6250 2    50   Input ~ 0
USBDAISYCHAIN
$Comp
L 000_symbol:Cable Cable211
U 1 1 6179F480
P 4950 2950
F 0 "Cable211" H 5175 3175 50  0000 C CNN
F 1 "Cable 12V power barrel_solder < 12 in" H 5175 3084 50  0000 C CNN
F 2 "" H 4950 2950 50  0001 C CNN
F 3 "" H 4950 2950 50  0001 C CNN
F 4 "Molex 5556/45750; 10-01780" H 4950 2950 50  0001 C CNN "Digi-Key_PN"
F 5 "LBL" H 4950 2950 50  0001 C CNN "MFR"
	1    4950 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 5150 2900 5150
Wire Wire Line
	2800 5000 3500 5000
Wire Wire Line
	3500 5000 3500 5150
$Sheet
S 8900 6800 850  600 
U 6184E58A
F0 "READUPIQ" 50
F1 "READUPIQ.sch" 50
F2 "IF_1" I L 8900 6900 50 
F3 "IF_2" I L 8900 7000 50 
F4 "DC6V3" I L 8900 7200 50 
F5 "LO" I L 8900 7100 50 
F6 "RF" O R 9750 6950 50 
F7 "DCPM1v8" I L 8900 7300 50 
$EndSheet
Wire Wire Line
	7550 3300 7000 3300
Wire Wire Line
	11400 1850 11700 1850
Wire Wire Line
	11400 2050 11700 2050
Wire Wire Line
	4300 6250 4650 6250
NoConn ~ 7550 5450
NoConn ~ 7550 5600
NoConn ~ 8500 5000
NoConn ~ 7950 6200
NoConn ~ 8100 6200
NoConn ~ 8250 6200
NoConn ~ 8400 6200
NoConn ~ 8500 700 
NoConn ~ 8400 1900
NoConn ~ 8250 1900
NoConn ~ 8100 1900
NoConn ~ 7950 1900
NoConn ~ 7550 1300
NoConn ~ 7550 1150
NoConn ~ 8500 2550
NoConn ~ 7550 3000
NoConn ~ 7550 3150
NoConn ~ 7950 3750
NoConn ~ 8100 3750
NoConn ~ 8250 3750
NoConn ~ 8400 3750
NoConn ~ 4450 3150
NoConn ~ 4300 3150
NoConn ~ 3300 2350
NoConn ~ 3300 2450
Wire Wire Line
	9850 1950 9750 1950
NoConn ~ 3300 1200
NoConn ~ 3300 1400
$Comp
L 000_symbol:minicircuits_ZN4PD1-842-S+_Splitter_4way U209
U 1 1 616DA362
P 12050 5300
F 0 "U209" H 12075 6067 50  0000 C CNN
F 1 "minicircuits_ZN4PD1-842-S+_Splitter_4way" H 12075 5976 50  0000 C CNN
F 2 "" H 11900 4850 50  0001 C CNN
F 3 "" H 12000 4950 50  0001 C CNN
F 4 "minicircuits" H 12050 5300 10  0001 C CNN "MFR"
F 5 "ZFSC-4-1-S+" H 12050 5300 10  0001 C CNN "model"
F 6 "1-1000" H 12050 5300 10  0001 C CNN "freq"
F 7 "ZN4PD1-842-S+" H 12050 5300 50  0001 C CNN "MPN"
	1    12050 5300
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:minicircuits_ZB4PD-52-20W+_Splitter_4way U204
U 1 1 616EAEE1
P 5600 1400
F 0 "U204" H 5625 2167 50  0000 C CNN
F 1 "minicircuits_ZB4PD-52-20W+_Splitter_4way" H 5625 2076 50  0000 C CNN
F 2 "" H 5450 950 50  0001 C CNN
F 3 "" H 5550 1050 50  0001 C CNN
F 4 "minicircuits" H 5600 1400 10  0001 C CNN "MFR"
F 5 "ZFSC-4-1-S+" H 5600 1400 10  0001 C CNN "model"
F 6 "1-1000" H 5600 1400 10  0001 C CNN "freq"
F 7 "ZB4PD-52-20W-S+" H 5600 1400 50  0001 C CNN "MPN"
	1    5600 1400
	1    0    0    -1  
$EndComp
$Comp
L 000_symbol:SMAcable_MM Cable222
U 1 1 6171FD95
P 12600 5650
F 0 "Cable222" H 12825 5925 50  0000 C CNN
F 1 "SMAcable_MM_3in_RA_RA" H 12825 5834 50  0000 C CNN
F 2 "" H 12600 5650 50  0001 C CNN
F 3 "" H 12600 5650 50  0001 C CNN
F 4 "minicircuits" H 12600 5650 50  0001 C CNN "MFR"
F 5 "086-3SMR+" H 12600 5650 50  0001 C CNN "MPN"
	1    12600 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2650 3300 2650
Text Label 2700 2650 0    50   ~ 0
DC5V
Text Label 10700 4400 0    50   ~ 0
DC5V
$Comp
L 000_symbol:Splitter_8way U211
U 1 1 61742AEE
P 14050 6500
F 0 "U211" H 14300 8467 50  0000 C CNN
F 1 "ZN8PD-113-S+" H 14300 8376 50  0000 C CNN
F 2 "" H 14150 6450 50  0001 C CNN
F 3 "" H 14250 6550 50  0001 C CNN
F 4 "minicircuits" H 14050 6500 50  0001 C CNN "MFR"
F 5 "ZN8PD-113-S+" H 14050 6500 50  0001 C CNN "MPN"
	1    14050 6500
	1    0    0    -1  
$EndComp
$Sheet
S 6400 8550 850  600 
U 617EFFAC
F0 "UPIQ1" 50
F1 "UPIQR.sch" 50
F2 "IF_1" I L 6400 8650 50 
F3 "IF_2" I L 6400 8750 50 
F4 "DC6V3" I L 6400 8950 50 
F5 "LO" I L 6400 8850 50 
F6 "RF" O R 7250 8700 50 
F7 "DCPM1v8" I L 6400 9050 50 
$EndSheet
$Comp
L 000_symbol:FixedAttenuator U212
U 1 1 61E63ACC
P 13750 5600
F 0 "U212" H 14002 5385 50  0000 C CNN
F 1 "FixedAttenuator_3dB" H 14002 5476 50  0000 C CNN
F 2 "" H 13750 5600 50  0001 C CNN
F 3 "" H 13750 5600 50  0001 C CNN
F 4 "minicircuits" H 13750 5600 50  0001 C CNN "MFR"
F 5 "FW-3+" H 13750 5600 50  0001 C CNN "MPN"
	1    13750 5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 1300 7250 1450
$Comp
L 000_symbol:Cable Cable221
U 1 1 617295E8
P 10700 4950
F 0 "Cable221" H 10925 5175 50  0000 C CNN
F 1 "Cable 5V power pin_solder < 20 in" H 10925 5084 50  0000 C CNN
F 2 "" H 10700 4950 50  0001 C CNN
F 3 "" H 10700 4950 50  0001 C CNN
F 4 "10-01780" H 10700 4950 50  0001 C CNN "Digi-Key_PN"
F 5 "LBL" H 10700 4950 50  0001 C CNN "MFR"
	1    10700 4950
	0    -1   -1   0   
$EndComp
$Comp
L 000_symbol:ZX60-83LN-S+ U210
U 1 1 61712763
P 10700 5350
F 0 "U210" H 11044 5453 60  0000 L CNN
F 1 "ZX60-83LN-S+" H 10400 5050 60  0000 L CNN
F 2 "" H 10900 5550 60  0001 L CNN
F 3 "" H 10900 5650 60  0001 L CNN
F 4 "RF Amplifiers" H 10900 6050 60  0001 L CNN "Family"
F 5 "minicircuits" H 10700 5350 50  0001 C CNN "MFR"
F 6 "ZX60-83LN-S+" H 10700 5350 50  0001 C CNN "MPN"
	1    10700 5350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
