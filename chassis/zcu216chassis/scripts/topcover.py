import tbox
import cut 

 
import tbox
import os
import FreeCAD
import numpy
import cut 
def topcover(filepath,xycuts,material=["5052 H32 Aluminum","0.040"]):
    basename=os.path.splitext(os.path.basename(filepath))[0]
    dirname=os.path.dirname(filepath)
    doc=FreeCAD.newDocument(basename)
    mpara=tbox.materialinfo[material[0]][material[1]]
    thickness=mpara['thickness']
    bendallowance90=90/180*numpy.pi*(mpara["Effective bend radius @ 90"]+mpara["K Factor"]*mpara["thickness"])
    benddeduction90=2*(mpara["Effective bend radius @ 90"]+mpara["thickness"])*numpy.tan(90/180*numpy.pi/2)-bendallowance90


    body=doc.addObject('PartDesign::Body',basename)
    x=cut.xch+2*thickness+1
    y=cut.ych
    outlinept=[(-x/2,-y/2),(-x/2,y/2),(x/2,y/2),(x/2,-y/2),(-x/2,-y/2)]
    bobj=tbox.outlinepad(doc,body,x=0,y=0,outlinept=outlinept,thickness=thickness)
    bobj=topbend(bobj,mpara)
    bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=2)
    bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=0.5,alledge=True)
        
    xyhole=tbox.panelparts(baseobj=bobj,parts=xycuts,plane='XY_Plane')
    bobj=tbox.pockethole(baseobj=bobj,name='cutxy',pocketprofile=xyhole,pocketreversed=1) 
    #xzhole=tbox.panelparts(baseobj=bobj,parts=xzcuts,plane='XZ_Plane')
    #bobj=tbox.pockethole(baseobj=bobj,name='cutxz',pocketprofile=xzhole,pocketreversed=1) 
    doc.recompute()
    bobj.Shape.exportStep(os.path.join(dirname,basename+'.step')) 
    doc.saveAs(os.path.join(dirname,basename+'.FCStd'))
    return doc
def topbend(bobj,mpara):
    height=7.5
    if 1:
        face='Face%d'%(tbox.pointinfaceindex(bobj,-209.542,-31.637,0.600273)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=height,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,209.542,-31.637,0.600273)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=height,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90,mpara=mpara)
    if 0:
        pass
#    bobj=bend(baseobj=bobj,face='Face41',length=30,radius=bendingradius,gap1=0,gap2=0,extend1=0,extend2=0,angle=90)
#    bend(baseobj,face,length,gap1,gap2,extend1,extend2,angle=90,invert=False,radius=1)
    return bobj

dirname='../part/'
filepath=dirname+'topcover.FCStd'
#doc=topcover(x=cut.xch,z=cut.ych,filepath=dirname+'topcover.FCStd',parts=cut.tpparts,rcorner=2,mid=1,material=["5052 H32 Aluminum","0.040"])
doc=topcover(filepath,xycuts=cut.tpparts,material=["5052 H32 Aluminum","0.040"])
