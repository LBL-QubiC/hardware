import tbox
import os
import numpy
import FreeCAD
#import FreeCADGui
import Part
import PartDesign
import importDXF
#gui import ImportGui
import platform
import SheetMetalCmd
#    FreeCADGui.activateWorkbench("SMWorkbench")
partdict={'powerbutton':dict(cuttype='roundhole',para=dict(r=8))}
topthickness=0.04*25.4
#xhole=[-199.7,199.7]
xhole=[-197.0,197.0]
yhole=-153/2*3+numpy.arange(4)*153
rhole=0.189*25.4/2
xch=16.38*25.4
zch=3.3*25.4
zhole=[zch/2-48.41/2,zch/2+48.41/2]
ych=22*25.4
fpparts=[]
for y in [35,55]:
    for x in numpy.linspace(-7*25.4,-1*25.4,8):
        fpparts.append(('SMA',x,y,{}))
    for x in numpy.linspace(1*25.4,7*25.4,8):
        fpparts.append(('SMA',x,y,{}))
for x in numpy.array((0,))*25.4:
#    for y in [10.5,zch-10.5]:
    for y in [0.063*25.4+7.6+0.07*25.4,zch-topthickness-10.5]:
#        fpparts.append(('BRACKET617',x,y,{}))
        fpparts.append(('screw6-32freefit',x,y,{}))
tpparts=[]
for x in numpy.arange(-6,7,2)*25.4:
    for y in [-ych/2+10.5,ych/2-10.5]: # using thread side of bracket 4334
        #tpparts.append(('BRACKET4334',x,y,{}))
        tpparts.append(('screw6-32freefit',x,y,{}))
#    for x in xhole:
#        for y in yhole:
#            tpparts.append(('keyhole632',x,y))
#for x in numpy.arange(-320/2,320/2,10):
#    for y in numpy.arange(-ych/2+50,ych/2-50,10):
#        tpparts.append(('VENTHOLE30',x,y))
rearpanelpowerparts=[]
rpparts=[]
for x in range(-185,-100,18):
    rpparts.append(('SMA',x,70,{}))
for x in range(-89,-0,18):
    rpparts.append(('SMA',x,70,{}))
rpparts.extend(((partdict['powerbutton']['cuttype'],172,68,partdict['powerbutton']['para']),))
#    rpparts.append(('powerbutton',172,68,{}))
rpparts.append(('rj45holder_x',172,47,{}))
#rpparts.append(('power6p_1',172,20,{}))
rpparts.extend(tbox.power6p_1(172,20))
rpparts.extend(tbox.zcu216rp(-186.63,27))
rearpanelpowerparts.extend(tbox.zcu216rp(-186.63+14,27,tight=True))
#rearpanelpowerparts.extend(tbox.pws3501h(134.12428,61.524))
#rearpanelpowerparts.append(('rectcut',134.12428,61.524,dict(dx=100,dy=40)))  # Power supply place holder
rearpanelpowerparts.append(('roundhole',155,28,dict(r=8)))  #power button
rearpanelpowerparts.extend(tbox.power6p_2(155,10))
rearpanelpowerparts.extend(tbox.rj45holder(180,20,direction='y'))
for x in numpy.arange(-3,1)*21-17:
    rearpanelpowerparts.extend(tbox.rj45holder(x,65,direction='y'))
    pass
for x in numpy.array([0,1,2.25])*21+17:
    rearpanelpowerparts.extend(tbox.rj45holder(x,65,direction='y'))
    pass
for x in -185+numpy.arange(5)*18:
    rearpanelpowerparts.extend(tbox.smapanelmount(x,67))
    pass
#rpparts.append(('zcu216rp',-186.63,27,{}))
rpparts.append(('powerholder',-15,8,{}))
for x in range(17,100,23):
    rpparts.append(('rj45holder_y',x,60,{}))
for x in range(117,147,23):
    rpparts.append(('rj45holder_y',x,60,{}))
for x in range(140,147,23):
    rpparts.append(('rj45holder_y',x,23,{}))
for x in numpy.array((-4,0,4))*25.4:
#    for y in [zch-10.5]:
    for y in [zch-topthickness-10.5]:
        #rpparts.append(('BRACKET617',x,y,{}))
        rpparts.append(('screw6-32freefit',x,y,{}))
for x in numpy.array((-4,0))*25.4:
    #for y in [zch-10.5]:
    for y in [zch-topthickness-10.5]:
        #rearpanelpowerparts.append(('BRACKET4334',x,y,{}))
        rearpanelpowerparts.append(('screw6-32freefit',x,y,{}))
for x in numpy.array((-6,0,4))*25.4:
    for y in [7.6+(0.063+0.07)*25.4]:
        #rpparts.append(('BRACKET617',x,y,{}))
        #rearpanelpowerparts.append(('BRACKET617',x,y,{}))
        rpparts.append(('screw6-32freefit',x,y,{}))
        pass
        rearpanelpowerparts.append(('screw6-32freefit',x,y,{}))
brkparts=[]
brkparts.append(('rackmounthole',(3.5*25.4)/2+1.5*25.4,20.67875,{}))
brkparts.append(('rackmounthole',(3.5*25.4)/2-1.5*25.4,20.67875,{}))
brkparts.append(('rackmounthole',(3.5*25.4)/2+0.875*25.4,20.67875,{}))
brkparts.append(('rackmounthole',(3.5*25.4)/2-0.875*25.4,20.67875,{}))
brkparts.append(('rackmounthole',(3.5*25.4)/2+0.25*25.4,20.67875,{}))
brkparts.append(('rackmounthole',(3.5*25.4)/2-0.25*25.4,20.67875,{}))
brkparts.append(('handlehole',(3.5*25.4)/2,10.2,{}))
chassismountparts=[]
for yp in -ych/2-3.05-2.16+numpy.array([14.22,2*25.4,77.6]):
    for zp in 1.75*25.4+33.63*numpy.array([-0.5,0.5]):
        chassismountparts.append(('screw6-32freefit',yp,zp,{}))
        #rackbracketsketch.addGeometry(Part.Circle(FreeCAD.Vector(yp,zp,0),FreeCAD.Vector(0,0,1),r),False)


dcafeparts=[]
for x,y in [(0,0),(0,297.94),(258.42,0),(258.42,297.94),(246.06,98.81),(246.06,268.71)]:
    dcafeparts.append(('screw4-40freefit',x,y,{}))
for xh in [0,114.3,152.4]:
    for yh in [0,241.3]:
        dcafeparts.append(('screw4-40freefit',xh+204.78,yh+65.96,{}))  # ZCU216 connection
#    dcafeparts.append(('zcu216standoff',0,0,{}))
#    dcafeparts.append(('diffcarrier',204.78,65.96,{}))
#    dcafeparts.append(('gridhole',18.9,60.51,{}))
#geoList.extend(gridhole(x=x,y=y,Nx=14,Ny=3,dx=25.4,dy=4*25.4,r=2.2))
for ixh in [0,1,2,6,7,8,11,12,13]:#,range(14):
    for iyh in [0,1,2,3,4,8,9,10]: #range(11):
        xh=18.9+ixh*25.4
        yh=9.71+14+iyh*25.4
        dcafeparts.append(('screw6-32freefit',xh,yh,{}))
#            geoList.extend(hole(x+ixh*dx,y+iyh*dy,r=r))
dcafeoutline=[
        (0,0),(268,0),(268,67),(369,67),(369,265.06+53),(200,265.06+53),(200,311.06),(0,311.06),(0,215),(30,215),(30,115),(0,115),(0,0)
        ]
fprpmounthole=[]
rpapciparts=[]
powerbaseparts=[]
powerup1parts=[]
for x in numpy.array([-0.75,-0.25,0.25,0.75]): #[-0.5*25.4,0*25.4,0.5*25.4]:
    for y in numpy.arange(7):
        powerbaseparts.append(('screw6-32freefit',x*25.4,(y+0.125)*25.4,{}))
for x in numpy.array([-2,-1.5,-1,-0.5,0])+0.25:
    for y in numpy.arange(10):
        if y in [1,2,3] and x>-1.4:
            pass
        else:
            powerup1parts.append(('screw6-32freefit',x*25.4,(y+0.5)*25.4,{}))
powerswipmibaseparts=[]
for (x,y) in [(-1,0.25),(1,0.25),(0,1.25),(0,2.25),(0,3.25),(0,4.25),(-1,5.25),(1,5.25)]:
    powerswipmibaseparts.append(('screw4-40freefit',x*25.4,y*25.4,{}))
for x in numpy.array([-0.5,0.5])*25.4: #[-0.5*25.4,0*25.4,0.5*25.4]:
    for y in (numpy.array([1,3,5]))*25.4:
#        powerswipmibaseparts.append(('screw6-32freefit',x-6.6,y+3.8,{}))
        powerswipmibaseparts.append(('holeslotx', x-6.6+4,y+3.8,dict(dx=8,r=0.1495/2*25.4)))
#    for x in numpy.array([-0.75,-0.25,0.25,0.75])*25.4: #[-0.5*25.4,0*25.4,0.5*25.4]:
#        for y in (numpy.arange(7)+0.25)*25.4:
#            powerswipmibaseparts.append(('screw6-32freefit',x,y,{}))
powerup1parts.append(('rectcut',-0.*25.4,2.5*25.4,dict(dx=2.5*25.4,dy=3.5*25.4)))
for x in xhole:
    for y in zhole:
        fprpmounthole.append(('holeslotx',x,y,dict(dx=0.15*25.4,r=0.1875/2*25.4)))
        fpparts.append(('holeslotx',x,y,dict(dx=0.15*25.4,r=0.1875/2*25.4)))
        rpapciparts.append(('holeslotx',x,y,dict(dx=0.15*25.4,r=0.1875/2*25.4)))
        rearpanelpowerparts.append(('holeslotx',x,y,dict(dx=0.15*25.4,r=0.1875/2*25.4)))
        #fprpmounthole.append(('screw6-32selfclinchingnut',x,y,{}))
        #fpparts.append(('screw6-32freefit',x,y,{}))
        #rpapciparts.append(('screw6-32freefit',x,y,{}))
        #rearpanelpowerparts.append(('screw6-32freefit',x,y,{}))
for y in zhole:
    for x in [-197.0+9,197.0-9]:
        rpapciparts.append(('screw6-32freefit',x,y,{}))
for x in [-5.75*25.4,-1.75*25.4,1.75*25.4,5.75*25.4]:
    rpapciparts.append(('rectcut',x,45.1282,dict(dx=3.00*25.4,dy=zch-30+1)))
    rpapciparts.append(('rectcut',x,12.8782,dict(dx=3.00*25.4,dy=3.3)))
    rpapciparts.append(('rectcut',x,77.3782,dict(dx=3.00*25.4,dy=3.5)))
for x in numpy.array((-4,0,4))*25.4:
#    for y in [zch-10.5]:
    for y in [zch-topthickness-10.5]:
        #rpapciparts.append(('BRACKET617',x,y,{}))
        rpapciparts.append(('screw6-32freefit',x,y,{}))
for x in numpy.array((-4,0,4))*25.4:
#    for y in [10.5]:
    for y in [0.063*25.4+7.6+0.07*25.4]:#,
        #rpapciparts.append(('BRACKET617',x,y,{}))
        rpapciparts.append(('screw6-32freefit',x,y,{}))
#    rpapciparts.append(('rectcut',-1.75*25.4,zch/2,dict(dx=3*25.4,dy=30)))
#    rpapciparts.append(('rectcut',1.75*25.4,zch/2,dict(dx=3*25.4,dy=30)))
#    rpapciparts.append(('rectcut',5.75*25.4,zch/2,dict(dx=3*25.4,dy=30)))
fprpbrackethole=[]
for x in numpy.arange(-6,7,2)*25.4:
    for y in [-ych/2+8.4,ych/2-8.4]:
        fprpbrackethole.append(('screw6-32selfclinchingnut',x,y,{}))
topmounthole=[]
for x in xhole:
    for y in yhole:
        topmounthole.append(('screw6-32selfclinchingnut',x,y,{}))
        tpparts.append(('keyhole632',x,y,{}))
basegridhole=[]
for x in numpy.array([-7,-6,-4,-2,0,2,4,6,7])*25.4:
    for y in numpy.array([-10,-8,-6,-4,-2,0,2,4,6,8,10])*25.4:
        basegridhole.append(('screw6-32selfclinchingnut',x,y,{}))
sidehole=[]
rackbkzcenter=1.75*25.4*2/2 #(z+topthickness)/2
geoList=[]
for x in [-ych/2+14.22,-ych/2+2*25.4,-ych/2+77.6,ych/2-14.22,ych/2-2*25.4,ych/2-77.6]: # rack bracket hole
    for y in rackbkzcenter+33.63*numpy.array([-0.5,0.5]):
        sidehole.append(('screw6-32selfclinchingnut',x,y,{}))
for x in [-120,0,120]:
    for y in [(zch-topthickness)/2]:
        sidehole.append(('fan60',x,y,{}))
#    sidehole.append(('rslide22',ych/2,1.75/2.0*25.4,{}))
for x in [-ych/2+26,-ych/2+26+39+89+288]:
    for y in [1.75/2.0*25.4]:
        sidehole.append(('screw8-32selfclinchingnut',x,y,{}))
        sidehole.append(('screw8-32selfclinchingnut',-x,y,{}))
        #sidehole.append(('roundhole',x,y,{'r':4/2}))
        #sidehole.append(('roundhole',-x,y,{'r':4/2}))
for x in [
        #-ych/2+26+9
#,
-ych/2+26+39+89+288+9
#,-ych/2+26+39+89+288+75+11
          ]:
    for y in [1.75/2.0*25.4]:
        sidehole.append(('holesloty',x,y,dict(dy=0.2*25.4,r=0.17/2*25.4)))
        sidehole.append(('holesloty',-x,y,dict(dy=0.2*25.4,r=0.17/2*25.4)))
for x in [-ych/2+26+39,-ych/2+26+39+89+288+75]:
    for y in [1.75/2.0*25.4]:
        sidehole.append(('holeslotx',x,y,dict(dx=0.2*25.4,r=0.17/2*25.4)))
        sidehole.append(('holeslotx',-x,y,dict(dx=0.2*25.4,r=0.17/2*25.4)))
ape4ampcut=[]
ape8smacut=[]
ape8mountcut=[]
apecutyz=[]
apecutxy=[]
for x in [0]:
    for y in [24.5,24.5+11.5*1,24.5+11.5*2,24.5+11.5*3]:
        ape4ampcut.append(('rectcut',x,y,dict(dx=6.55,dy=6.55)))
for y in [4.5,69]:
    for x in [5,15]:
        apecutxy.append(('screw4-40tap',x,y,{}))
    apecutyz.append(('screw4-40tap',y,-6.5,{}))
    for x in [-6.3,6.3]:
        ape8mountcut.append(('screw4-40tap',x,y,{}))

for x in [-6.3,6.3]:
    #for y in [24.5,24.5+11.5*1,24.5+11.5*2,24.5+11.5*3]:
    for y in numpy.arange(4)*12.5+18:#[24.5,24.5+11.5*1,24.5+11.5*2,24.5+11.5*3]:
        ape8smacut.append(('SMA',x,y,{}))

    
powercagexycuts=[]
powercagexzcuts=[]
for x in (-50.366,50.366):
    for y in numpy.array([1,3,5,6])*25.4:
        powercagexycuts.append(('screw4-40freefit',x,y,{}))
        powerup1parts.append(('screw4-40selfclinchingnut',x,y-0.5*25.4,{}))
    #for z in [-15]:
    for z in [-13.035]:
#        for z in [-15.575]:
        powercagexzcuts.append(('screw4-40selfclinchingnut',x,z,{}))
        rearpanelpowerparts.append(('holesloty',x+133.366,z+39.1922+1.143+33.1596,dict(dy=2,r=0.1495/2*25.4)))
for x in (-27.5,27.5):
    for y in [185]:
        powercagexycuts.append(('screw4-40freefit',x,y,{}))
        powerup1parts.append(('screw4-40selfclinchingnut',x,y-0.5*25.4,{}))
rearpanelpowerparts2=[]
for x,y in [#(50.366-90,20.3),
            (50.366,4.25),(50.366,36.25)
            ,(50.366-75.9+4.4,36)
            ,(50.366-75.9+15.2,37)
            ]:
    rearpanelpowerparts2.append(('screw6-32freefit',x+133.366,y+39.1922+1.143,{}))
for x,y in [(50.366-90,20.3),]:
#    rearpanelpowerparts2.append(('screw6-32freefit',x+133.366,y+39.1922+1.143,{}))
    rearpanelpowerparts.append(('holesloty',        x+133.366,y+39.1922+1.143,dict(dy=2,r=0.1495/2*25.4)))
    pass

xy=(
 (50.366-45-37,17.5-15.25)
,(50.366-45-37,20+31/2)
,(50.366-45+0,20+31/2)
,(50.366-45+0,20+35/2)
,(50.366-45+12,20+35/2)
,(50.366-45+12,17.5-15.25)
#,(50.366-45+0,20-35/2)
#,(50.366-45+0,20-31/2)
#,(50.366-75.9+15+23.5/2, 17.5-15.25)
#,(50.366-75.9+15+23.5/2, 17.5-15.25)
#,(50.366-75.9+15-23.5/2, 17.5-15.25)
#,(50.366-75.9+15-23.5/2, 20-31/2)
,(50.366-45-37,17.5-15.25)
#,(50.366-45-37,20-31/2)
)
rearpanelpowerparts.append(('multilinecut',133.366,39.1922+1.143,dict(xys=xy)))

xy=(
(50.366-45+40,20+35/2)
,(50.366-45+40,17.5-15.25)
,(50.366-45+15,17.5-15.25)
,(50.366-45+15,20+35/2)
,(50.366-45+40,20+35/2)
)
rearpanelpowerparts.append(('multilinecut',133.366,39.1922+1.143,dict(xys=xy)))
