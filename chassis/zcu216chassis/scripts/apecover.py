import tbox
import os
import FreeCAD
import numpy
import cut 
import importDXF

def apecover2(x,z,cutlength,filepath,material,partsxy=[],rcorner=0,outeredge=None,partsyz=[]):
    mpara=tbox.materialinfo[material[0]][material[1]]#["5052 H32 Aluminum"]["0.040"]
    thickness=mpara['thickness']
    outlinept=[
            (0,0)
,(-12.7,0)
,(-12.7,74.0)
,(12.7,74.0)
,(12.7,0)
            ,(0,0)
            ]
    basename=os.path.splitext(os.path.basename(filepath))[0]
    dirname=os.path.dirname(filepath)
    
    doc=FreeCAD.newDocument(basename)
    body=doc.addObject('PartDesign::Body',basename)

    bobj=tbox.outlinepad(doc,body,x=x,y=z,outlinept=outlinept,thickness=thickness)
    bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=2)
    if isinstance(partsxy[0][0],list) or isinstance(partsxy[0][0],tuple):
        partxylist=partsxy
    else:
        partxylist=[partsxy]
    for p in partxylist:
        partxy=tbox.panelparts(baseobj=bobj,parts=p,plane='XY_Plane')
        bobj=tbox.pockethole(baseobj=bobj,name='partcutyz',pocketprofile=partxy,pocketreversed=1) #,length=None,pockettype=1,pocketmidplane=1):

    bobj.Visibility=True
    doc.recompute()
    bobj.Shape.exportStep(os.path.join(dirname,basename+'.step'))
    importDXF.export([bobj],os.path.join(dirname,basename+'.dxf')) #"/home/ghuang/tmp/zcu216chassis/frontpanel.dxf")
    doc.saveAs(os.path.join(dirname,basename+'.FCStd'))
        #gui ImportGui.export([doc.getObject(basename)],os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/chassisbox.step")
    
    return doc

def apecover3(x,z,cutlength,filepath,material,partsxy=[],rcorner=0,outeredge=None,partsyz=[]):
    mpara=tbox.materialinfo[material[0]][material[1]]#["5052 H32 Aluminum"]["0.040"]
    thickness=mpara['thickness']
    ybend=9.5
    xbend=-3+2.22
    outlinept=[
            (6.01+0,0)
,(xbend,0)
,(xbend,ybend)
,(6.01-12.7,ybend)
,(6.01-12.7,74-ybend)
,(xbend,74-ybend)
,(xbend,74.0)
,(6.01+12.7,74.0)
,(6.01+12.7,0)
            ,(6.01+0,0)
            ]
    basename=os.path.splitext(os.path.basename(filepath))[0]
    dirname=os.path.dirname(filepath)
    
    doc=FreeCAD.newDocument(basename)
    body=doc.addObject('PartDesign::Body',basename)

    bobj=tbox.outlinepad(doc,body,x=x,y=z,outlinept=outlinept,thickness=thickness)
    bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,-0.78,69.25,0.762)+1),     length=10,gap1=1,gap2=0,extend1=0,extend2=0,angle=-90,mpara=mpara)
    bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,-0.78,4.75,0.762)+1),      length=10,gap1=0,gap2=1,extend1=0,extend2=0,angle=-90,mpara=mpara)
    bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=2)
    if isinstance(partsxy[0][0],list) or isinstance(partsxy[0][0],tuple):
        partxylist=partsxy
    else:
        partxylist=[partsxy]
    for p in partxylist:
        partxy=tbox.panelparts(baseobj=bobj,parts=p,plane='XY_Plane')
        bobj=tbox.pockethole(baseobj=bobj,name='partcutyz',pocketprofile=partxy,pocketreversed=1) #,length=None,pockettype=1,pocketmidplane=1):

    if isinstance(partsyz[0][0],list) or isinstance(partsyz[0][0],tuple):
        partyzlist=partsyz
    else:
        partyzlist=[partsyz]
    for p in partyzlist:
        partyz=tbox.panelparts(baseobj=bobj,parts=p,plane='YZ_Plane')
        bobj=tbox.pockethole(baseobj=bobj,name='partcutyz',pocketprofile=partyz,pocketreversed=1) #,length=None,pockettype=1,pocketmidplane=1):
    
    bobj.Visibility=True
    doc.recompute()
    bobj.Shape.exportStep(os.path.join(dirname,basename+'.step'))
    importDXF.export([bobj],os.path.join(dirname,basename+'.dxf')) #"/home/ghuang/tmp/zcu216chassis/frontpanel.dxf")
    doc.saveAs(os.path.join(dirname,basename+'.FCStd'))
        #gui ImportGui.export([doc.getObject(basename)],os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/chassisbox.step")
    
    return doc

