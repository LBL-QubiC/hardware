import re
from datetime import datetime

def parse_and_replace_date_time(file_content, new_date_time_str):
    # Extract HEADER section
    header_section = re.search(r'HEADER;(.*?ENDSEC;)', file_content, re.DOTALL)
    if header_section:
        header_content = header_section.group(1).strip()
        # Find and replace the date and time string in FILE_NAME
        new_header_content = re.sub(r'(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})', new_date_time_str, header_content)
        file_content = file_content.replace(header_content, new_header_content)

    return file_content

def main(filepath,timestr):
    with open(filepath) as fin:
        step_file_content = fin.read()

    #new_date_time_str = '2025-01-01T12:00:00'
    updated_content = parse_and_replace_date_time(step_file_content, timestr)

    with open(filepath, 'w') as file:
        file.write(updated_content)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='filepath', help='step or stp file path')
    parser.add_argument(dest='timestr', help='timestamp string, 1900-01-01T00:00:00')
    clargs=parser.parse_args()
    main(clargs.filepath,clargs.timestr)
