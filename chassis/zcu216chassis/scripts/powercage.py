import tbox
import os
import FreeCAD
import numpy
import cut 
def powercage(filepath,xycuts,xzcuts,material=["5052 H32 Aluminum","0.040"]):
    basename=os.path.splitext(os.path.basename(filepath))[0]
    dirname=os.path.dirname(filepath)
    doc=FreeCAD.newDocument(basename)
    mpara=tbox.materialinfo[material[0]][material[1]]
    thickness=mpara['thickness']
    bendallowance90=90/180*numpy.pi*(mpara["Effective bend radius @ 90"]+mpara["K Factor"]*mpara["thickness"])
    benddeduction90=2*(mpara["Effective bend radius @ 90"]+mpara["thickness"])*numpy.tan(90/180*numpy.pi/2)-bendallowance90


    body=doc.addObject('PartDesign::Body',basename)
    x2=43-benddeduction90/2
    outlinept=[(-x2,0),(x2,0),(x2,177),(12,177),(12,176),(-12,176),(-12,177),(-x2,177),(-x2,0)]
    bobj=tbox.outlinepad(doc,body,x=0,y=0,outlinept=outlinept,thickness=thickness)
    bobj=powercagebend(bobj,mpara)
    bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=2)
    bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=0.5,alledge=True)
        
    xyhole=tbox.panelparts(baseobj=bobj,parts=xycuts,plane='XY_Plane')
    bobj=tbox.pockethole(baseobj=bobj,name='cutxy',pocketprofile=xyhole,pocketreversed=1) 
    xzhole=tbox.panelparts(baseobj=bobj,parts=xzcuts,plane='XZ_Plane')
    bobj=tbox.pockethole(baseobj=bobj,name='cutxz',pocketprofile=xzhole,pocketreversed=1) 
    doc.recompute()
    doc.saveAs(os.path.join(dirname,basename+'.FCStd'))
    bobj.Shape.exportStep(os.path.join(dirname,basename+'.step')) 
    return doc
def powercagebend(bobj,mpara):
    height=32.55-mpara["Effective bend radius @ 90"]-mpara['thickness']
    print(f'height {height}')
    flange=12.7
    if 1:
        #bobj=    tbox.bend(baseobj=baseobj,face='Face%d'%(tbox.pointinfaceindex(baseobj,-207.26033,0,0.8)+1),     length=z-2*bendingradius-2*thickness-topthickness,gap1=bendingradius+thickness,gap2=bendingradius+thickness,extend1=0,extend2=0,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,-42.2123,33.9148,0.497079)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=height,gap1=3,gap2=3,extend1=0,extend2=0,angle=-90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,-43.2862,75.8794,-31.534)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=flange,gap1=3,gap2=3,extend1=0,extend2=0,angle=90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,-43.3879,3,-17.3827)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=flange,gap1=0,gap2=0,extend1=0,extend2=0,angle=90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,42.2123,49.1967,0.447211)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=height,gap1=3,gap2=3,extend1=0,extend2=0,angle=-90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,43.2853,75.9595,-31.534)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=flange,gap1=3,gap2=3,extend1=0,extend2=0,angle=90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,43.3224,3,-16.4037)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=flange,gap1=0,gap2=0,extend1=0,extend2=0,angle=90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,27.2833,177,0.437901)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=height,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,34.8835,178.076,-31.534)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=flange,gap1=0,gap2=0,extend1=0,extend2=0,angle=90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,-23.6026,177,0.681632)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=height,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90,mpara=mpara)
        face='Face%d'%(tbox.pointinfaceindex(bobj,-24.6837,178.236,-31.534)+1)
        bobj=tbox.bend(baseobj=bobj,face=face,length=flange,gap1=0,gap2=0,extend1=0,extend2=0,angle=90,mpara=mpara)
    #    bobj=bend(baseobj=bobj,face='Face120',length=height,radius=bendingradius,gap1=0,gap2=0,extend1=0,extend2=0,angle=90)
    #    bobj=bend(baseobj=bobj,face='Face128',length=height,radius=bendingradius,gap1=0,gap2=0,extend1=0,extend2=0,angle=90)
        pass
    if 0:
        pass
#    bobj=bend(baseobj=bobj,face='Face41',length=30,radius=bendingradius,gap1=0,gap2=0,extend1=0,extend2=0,angle=90)
#    bend(baseobj,face,length,gap1,gap2,extend1,extend2,angle=90,invert=False,radius=1)
    return bobj

dirname='../part/'
filepath=dirname+'powercage.FCStd'
doc=powercage(filepath,xycuts=cut.powercagexycuts,xzcuts=cut.powercagexzcuts,material=["5052 H32 Aluminum","0.040"])
