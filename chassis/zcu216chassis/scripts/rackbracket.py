import tbox
import os
import FreeCAD
import numpy
import cut 
def rackbracket(x,y,z,
                rackmountparts,chassismountparts,filepath,material):
    mpara=tbox.materialinfo[material[0]][material[1]]#["5052 H32 Aluminum"]["0.040"]
    thickness=mpara['thickness']
    basename=os.path.splitext(os.path.basename(filepath))[0]
    dirname=os.path.dirname(filepath)

    doc=FreeCAD.newDocument(basename)
    body=doc.addObject('PartDesign::Body','metalbend')
    #FreeCADGui.ActiveDocument.ActiveView.setActiveObject('pdbody',FreeCAD.getDocument(basename).getObject('metalbend'))

    zoff=15
    outlinept=[(-y/2,zoff),(-y/2,3.5*25.4-zoff),(-y/2+95,3.5*25.4-zoff),(-y/2+95,zoff),(-y/2,zoff)]
    bobj=tbox.outlinepad(doc,body,x=0,y=0,outlinept=outlinept,thickness=thickness)
    #basesketch=body.newObject('Sketcher::SketchObject','basesketch')
    #if FreeCAD.Version()[0]==1:
    #    basesketch.AttachmentSupport = (doc.getObject('XY_Plane'),[''])
    #else:
    #    basesketch.Support = (doc.getObject('XY_Plane'),[''])
    #basesketch.MapMode = 'FlatFace'
    #basesketch.Visibility= 0
    #geoList=rect(y0=zoff,x0=-y/2,y1=3.5*25.4-zoff,x1=-y/2+95)
    #basesketch.addGeometry(geoList,False)
    #pad=body.newObject('PartDesign::Pad','Pad')
    #pad.Profile=basesketch
    #pad.Length=thickness
#    FreeCADGui.Selection.addSelection(basename,'metalbend','Pad.Face1',0,0,0)
    bobj=tbox.bend(baseobj=bobj,face='Face1',length=x,gap1=0,gap2=0,extend1=zoff-1.2,extend2=zoff-1.2,mpara=mpara)
    bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=2)


#    filletout=body.newObject("PartDesign::Fillet","Fillet")
#    if FreeCAD.Version()[0]==1:
#        outeredge =[2,8,21,32,33,34]
#    else:
#        outeredge =[4,18,23,21,26,34]
#    filletout.Base=(bobj,['Edge%d'%edgeindex for edgeindex in outeredge])
#    filletout.Radius=2
#    bobj.Visibility=False
#    print('chassismountparts',chassismountparts)
    chassismounthole=tbox.panelparts(baseobj=bobj,parts=chassismountparts,plane='XY_Plane')
    bobj=tbox.pockethole(baseobj=bobj,name='chassismountcut',pocketprofile=chassismounthole,pocketmidplane=1)
#    yholemove=-y/2-thickness-bendingradius+yhole
#    rackbkhole=rackbrackethole(doc=doc,body=body,y=yholemove
#               #                [-y/2+14.22-thickness-bendingradius,-y/2+2*25.4-thickness-bendingradius,-y/2+14.22+77.6-thickness-bendingradius]
#                               ,z=zhole,r=0.189*25.4/2,plane='XY_Plane')
#    rackbkcut=body.newObject('PartDesign::Pocket','rackbkcut')
#    rackbkcut.Profile = rackbkhole
#    rackbkcut.Type = 1
#    rackbkcut.Midplane = 1
    
    rackmounthole=tbox.panelparts(baseobj=bobj,parts=rackmountparts,plane='YZ_Plane')
    bobj=tbox.pockethole(baseobj=bobj,name='rackmountcut',pocketprofile=rackmounthole,pocketmidplane=0)
#    partcut=body.newObject('PartDesign::Pocket','partcut')
#    partcut.Profile = parthole
#    partcut.Type = 1
#    partcut.Reversed = 0
    doc.recompute()
    bobj.Shape.exportStep(os.path.join(dirname,basename+'.step'))
    doc.saveAs(os.path.join(dirname,basename+'.FCStd')) #u"/home/ghuang/tmp/zcu216chassis/rackbracket.FCStd")
    #FreeCADGui.SendMsgToActiveView("ViewFit")
    #gui ImportGui.export([body],os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/rackbracket.step")
    return doc


dirname='../part/'
doc=rackbracket(x=26.05,y=cut.ych,z=45,rackmountparts=cut.brkparts,chassismountparts=cut.chassismountparts,filepath=dirname+'rackbracket.FCStd',material=["304 Stainless Steel","0.060"])

