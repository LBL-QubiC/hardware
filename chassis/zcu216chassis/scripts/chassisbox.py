import tbox
import os
import FreeCAD
import cut 
def chassisbox(x,z,y,edgewidth,topthickness,fprpmounthole,topmounthole,fprpbrackethole,basegridhole,sidehole,filepath,material):
    mpara=tbox.materialinfo[material[0]][material[1]]#["5052 H32 Aluminum"]["0.040"]
    thickness=mpara['thickness']
    bendingradius=mpara['Effective bend radius @ 90']

    basename=os.path.splitext(os.path.basename(filepath))[0]
    dirname=os.path.dirname(filepath)
    doc=FreeCAD.newDocument(basename)
    body=doc.addObject('PartDesign::Body',basename)
    outlinept=[
            (-x/2,-y/2),(-x/2,y/2),(x/2,y/2),(x/2,-y/2),(-x/2,-y/2)
            ]
    if 1:
        bobj=tbox.outlinepad(doc,body,x=0,y=0,outlinept=outlinept,thickness=thickness)
        bobj=chassisboxbend(bobj,z,topthickness,edgewidth,mpara=mpara)
        bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=2)
        bobj=tbox.edgefillet(baseobj=bobj,length=thickness,r=0.5)
        fprphole=tbox.panelparts(baseobj=bobj,parts=fprpmounthole,plane='XZ_Plane')
        bobj=tbox.pockethole(baseobj=bobj,name='fprpcut',pocketprofile=fprphole,pocketmidplane=1)
        doc.recompute()
        
        tbox.ztopplane(baseobj=bobj,zoffset=z-topthickness)
        tphole=tbox.panelparts(baseobj=bobj,parts=topmounthole,plane='Ztopplane') 
        bobj=tbox.pockethole(baseobj=bobj,name='tpcut',pocketprofile=tphole,length=thickness,pocketmidplane=0)
       
        fprpbkhole=tbox.panelparts(baseobj=bobj,parts=fprpbrackethole,plane='XY_Plane')
        bobj=tbox.pockethole(baseobj=bobj,name='fprpbracketcut',pocketprofile=fprpbkhole,pocketmidplane=1)
       
        basegridholes=tbox.panelparts(baseobj=bobj,parts=basegridhole,plane='XY_Plane') 
        bobj=tbox.pockethole(baseobj=bobj,name='basegridcut',pocketprofile=basegridholes,pocketmidplane=1)

        sideholes=tbox.panelparts(baseobj=bobj,parts=sidehole,plane='YZ_Plane')
        bobj=tbox.pockethole(baseobj=bobj,name='rackbkcut',pocketprofile=sideholes,pocketmidplane=1)
    if 0:
        pass
    doc.recompute()
    bobj.Shape.exportStep(os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/frontpanel.step")
    #FreeCADGui.SendMsgToActiveView("ViewFit")
    #gui ImportGui.export([body],os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/chassisbox.step")
    bobj.Visibility=True
    bobj._Body.Visibility=True
    doc.saveAs(os.path.join(dirname,basename+'.FCStd'))
    return doc,(x,y,z)
#    for x in (-52,52):
def chassisboxbend(baseobj,z,topthickness,edgewidth,mpara):
    bendingradius=mpara['Effective bend radius @ 90']
    thickness=mpara['thickness']
    #bend(baseobj,face,length,gap1,gap2,extend1,extend2,angle=90,invert=False,radius=1):
    if 1:
        bobj=    tbox.bend(baseobj=baseobj,face='Face%d'%(tbox.pointinfaceindex(baseobj,-207.26033,0,0.8)+1),     length=z-2*bendingradius-2*thickness-topthickness,gap1=bendingradius+thickness,gap2=bendingradius+thickness,extend1=0,extend2=0,mpara=mpara)
        bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,-208.94943, -276.911, 41.53)+1),   length=edgewidth,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius,mpara=mpara)
        bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,-208.94943, 276.911, 41.53)+1),length=edgewidth,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius,mpara=mpara)
        bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,-208.94943, 0, 80.3148)+1),length=edgewidth,gap1=0,gap2=0,extend1=0.9*bendingradius+thickness,extend2=0.9*bendingradius+thickness,mpara=mpara)
        bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,207.26033,0,0.8)+1),length=z-2*bendingradius-2*thickness-topthickness,gap1=bendingradius+thickness,gap2=bendingradius+thickness,extend1=0,extend2=0,mpara=mpara)
        bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,208.94943,-276.911,41.53)+1),length=edgewidth,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius,mpara=mpara)
        bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,208.94943,276.911,41.53)+1),length=edgewidth,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius,mpara=mpara)
        bobj=tbox.bend(baseobj=bobj,face='Face%d'%(tbox.pointinfaceindex(bobj,208.94943,0,80.3148)+1),length=edgewidth,gap1=0,gap2=0,extend1=0.9*bendingradius+thickness,extend2=0.9*bendingradius+thickness,mpara=mpara)
    if 0:
        pass
    return bobj



dirname='../part/'
minholeedge=0.3*25.4
x=max(cut.xhole)-min(cut.xhole)+2*(cut.rhole*1.1+minholeedge)
doc,(x,y,z)=chassisbox(x=x,z=cut.zch,y=cut.ych,edgewidth=(0.189+0.25+0.2)*25.4,topthickness=cut.topthickness,fprpmounthole=cut.fprpmounthole,topmounthole=cut.topmounthole,fprpbrackethole=cut.fprpbrackethole,basegridhole=cut.basegridhole,sidehole=cut.sidehole,filepath=dirname+'chassisbox.FCStd',material=["5052 H32 Aluminum","0.063"])
#doc.recompute()

