import os
import numpy
import FreeCAD
#import FreeCADGui
import Part
import PartDesign
import importDXF
#gui import ImportGui
import platform
import SheetMetalCmd
import FreeCADGui
FreeCADGui.showMainWindow()
mw=FreeCADGui.getMainWindow()
mw.hide()

inch=25.4
materialinfo={
        "5052 H32 Aluminum":{
             "0.040":{"thickness":0.040*inch,"K Factor":0.45,"Effective bend radius @ 90":0.024*inch,"Bend relief depth":0.084*inch}
            ,"0.063":{"thickness":0.063*inch,"K Factor":0.42,"Effective bend radius @ 90":0.035*inch,"Bend relief depth":0.118*inch}
            ,"0.080":{"thickness":0.080*inch,"K Factor":0.45,"Effective bend radius @ 90":0.045*inch,"Bend relief depth":0.084*inch}
            ,"0.090":{"thickness":0.090*inch,"K Factor":0.37,"Effective bend radius @ 90":0.032*inch,"Bend relief depth":0.142*inch}
            ,"0.100":{"thickness":0.100*inch,"K Factor":0.40,"Effective bend radius @ 90":0.125*inch,"Bend relief depth":0.245*inch}
            ,"0.125":{"thickness":0.125*inch,"K Factor":0.44,"Effective bend radius @ 90":0.125*inch,"Bend relief depth":0.245*inch}
            ,"0.187":{"thickness":0.187*inch,"K Factor":0.43,"Effective bend radius @ 90":0.250*inch,"Bend relief depth":0.245*inch}
            ,"0.250":{"thickness":0.250*inch,"K Factor":0.42,"Effective bend radius @ 90":0.250*inch,"Bend relief depth":0.245*inch}
            ,"3mm":{"thickness":3}
            }
        ,"304 Stainless Steel":{
             "0.030":{"thickness":0.030*inch,"K Factor":0.36,"Effective bend radius @ 90":0.080*inch}
             ,"0.048":{"thickness":0.048*inch,"K Factor":0.36,"Effective bend radius @ 90":0.080*inch}
             ,"0.060":{"thickness":0.060*inch,"K Factor":0.34,"Effective bend radius @ 90":0.070*inch}
             ,"0.074":{"thickness":0.074*inch,"K Factor":0.36,"Effective bend radius @ 90":0.075*inch}
             ,"0.100":{"thickness":0.100*inch,"K Factor":0.36,"Effective bend radius @ 90":0.187*inch}
             ,"0.125":{"thickness":0.125*inch,"K Factor":0.38,"Effective bend radius @ 90":0.150*inch}
             ,"0.187":{"thickness":0.187*inch,"K Factor":0.35,"Effective bend radius @ 90":0.130*inch}
             ,"0.250":{"thickness":0.250*inch,"K Factor":0.34,"Effective bend radius @ 90":0.225*inch}
            }

        }
def edge_direction(edge):
    start = edge.Vertexes[0].Point
    end = edge.Vertexes[1].Point
    direction = end.sub(start)
    return direction.normalize()
def bend(baseobj,face,length,gap1,gap2,extend1,extend2,mpara,angle=90,invert=False,kfactor=0.45):
    osname=platform.platform()
    doc=baseobj.Document
    bobj=doc.addObject("PartDesign::FeaturePython","Bend")
    #SheetMetalCmd.SMBendWall(bobj,doc.getObject(base),face)
    SheetMetalCmd.SMBendWall(bobj,baseobj,face)
    if hasattr(SheetMetalCmd,'SMViewProviderFlat'):
        SheetMetalCmd.SMViewProviderFlat(bobj.ViewObject)
    baseobj._Body.addObject(bobj)
    bobj.length=length
    bobj.gap1=gap1
    bobj.gap2=gap2
    bobj.extend1=extend1
    bobj.extend2=extend2
    bobj.angle=angle
    
    bobj.radius=mpara["Effective bend radius @ 90"]
    bobj.kfactor=mpara["K Factor"]
    bobj.reliefType=u"Round"
    baseobj.Visibility=False
    doc.recompute()
    return bobj


def cordxyz(P0,Pz,Px,P):
    Pt=P-P0
    vz=Pz-P0
    vx=Px-P0
    vz=vz/numpy.linalg.norm(vz)
    vx=vx/numpy.linalg.norm(vx)
    vy=numpy.cross(vz,vx)
    vy=vy/numpy.linalg.norm(vy)
    R=numpy.array([vx,vy,vz])
    Pnew=numpy.dot(R,Pt)
    return Pnew
def line_intersection(P1, P2, P3, P4):
  x1, y1 = P1
  x2, y2 = P2
  x3, y3 = P3
  x4, y4 = P4

  # Check if the lines are parallel
  if (x2 - x1) * (y4 - y3) == (x4 - x3) * (y2 - y1):
    return None  # Lines are parallel

  # Calculate the intersection point
  t = (x3 * (y4 - y3) - y3 * (x4 - x3) + y1 * (x4 - x3) - x1 * (y4 - y3)) / ((x2 - x1) * (y4 - y3) - (x4 - x3) * (y2 - y1))
  intersection_x = x1 + t * (x2 - x1)
  intersection_y = y1 + t * (y2 - y1)

  return numpy.array([intersection_x, intersection_y])

def convexedge(obj,edge,threshold=numpy.pi*0.99):
    P0=numpy.array([edge.Vertexes[0].X,edge.Vertexes[0].Y,edge.Vertexes[0].Z])
    Pz=numpy.array([edge.Vertexes[1].X,edge.Vertexes[1].Y,edge.Vertexes[1].Z])
    fedge=obj.Shape.ancestorsOfType(edge,Part.Face)
    convex=False
    if len(fedge)==2:
        f0,f1=fedge
        if isinstance(f0.Surface,Part.Plane) and isinstance(f1.Surface,Part.Plane):
            Px=P0+f1.normalAt(P0[0],P0[1])

            cm00=cordxyz(P0,Pz,Px,numpy.array(f0.CenterOfMass))
            cm01=cordxyz(P0,Pz,Px,numpy.array(f0.CenterOfMass+f0.normalAt(0,0)))
            cm10=cordxyz(P0,Pz,Px,numpy.array(f1.CenterOfMass))
            cm11=cordxyz(P0,Pz,Px,numpy.array(f1.CenterOfMass+f1.normalAt(0,0)))
            a00=numpy.arctan2(cm00[1],cm00[0])%(2*numpy.pi)
            a01=numpy.arctan2(cm01[1],cm01[0])%(2*numpy.pi)
            a10=numpy.arctan2(cm10[1],cm10[0])%(2*numpy.pi)
            a11=numpy.arctan2(cm11[1],cm11[0])%(2*numpy.pi)
            if a10>a00:
                twopim0=a01>a00 if (abs(a01-a00)<=numpy.pi) else ((a01+numpy.pi)%(2*numpy.pi))>((a00+numpy.pi)%(2*numpy.pi))
                twopim1=a11<a10 if (abs(a11-a10)<=numpy.pi) else ((a11+numpy.pi)%(2*numpy.pi))<((a10+numpy.pi)%(2*numpy.pi))
            else:
                twopim0=a01<a00 if (abs(a01-a00)<=numpy.pi) else ((a01+numpy.pi)%(2*numpy.pi))<((a00+numpy.pi)%(2*numpy.pi))
                twopim1=a11>a10 if (abs(a11-a10)<=numpy.pi) else ((a11+numpy.pi)%(2*numpy.pi))>((a10+numpy.pi)%(2*numpy.pi))

            bodyang=(2*numpy.pi-abs(a10-a00)) if (twopim0 and twopim1) else abs(a10-a00)
            convex=bodyang<threshold
    return convex

def convexedgewrong(obj,edge,threshold=0.5):
    P0=numpy.array([edge.Vertexes[0].X,edge.Vertexes[0].Y,edge.Vertexes[0].Z])
    P1=numpy.array([edge.Vertexes[1].X,edge.Vertexes[1].Y,edge.Vertexes[1].Z])
    Pm=(P0+P1)/2
    e0=obj.Shape.ancestorsOfType(edge.Vertexes[0],Part.Edge)
    e1=obj.Shape.ancestorsOfType(edge.Vertexes[1],Part.Edge)
    l0=[e.Length for e in e0]
    l1=[e.Length for e in e1]
    minl=min(min(l0),min(l1))
    fedge=obj.Shape.ancestorsOfType(edge,Part.Face)
    if len(fedge)==2:
        f0,f1=fedge
        n0=f0.normalAt(0,0)
        n1=f1.normalAt(0,0)
        a01=numpy.arccos(n0.dot(n1))
        a2=max(min(a01/10,numpy.pi/10),numpy.pi/60)
        angs=numpy.arange(0,2*numpy.pi,a2)
        n2=numpy.cross(n0,P0-P1)
        v=(P0-P1)/numpy.linalg.norm(P0-P1)
        n0=numpy.array(n0/numpy.linalg.norm(n0))
        n2=numpy.array(n2/numpy.linalg.norm(n2))
        r=minl/2
        #print(r,a2)
        Pslist=[]
        for ang in angs:
            Pslist.append(Pm+r*numpy.cos(ang)*n0+r*numpy.sin(ang)*n2)
        convex=sum([obj.Shape.isInside(FreeCAD.Vector(p),0.0001,True) for p in Pslist])/len(Pslist)<threshold
    else:
        convex=False
    return convex

def edgefilletindex(obj,length,r,err=1e-9,debug=False,alledge=False):
    redge=[]
    egs=obj.Shape.Edges
    if debug:
        print(obj)
        print('length of egs',len(egs))
    for iedge,edge in enumerate(egs):
        #print(iedge,len(egs))

        egl=edge.Length #numpy.round(edge.Length,10)
        if debug:
            print(egl)
        if abs(egl-length)<=err:
            if debug:
                print('length pass %d'%iedge)
            if convexedge(obj,edge):
                e0=[e for e in obj.Shape.ancestorsOfType(edge.Vertexes[0],Part.Edge) if not e.isSame(edge)]
                e1=[e for e in obj.Shape.ancestorsOfType(edge.Vertexes[1],Part.Edge) if not e.isSame(edge)]
                l0=[e.Length for e in e0]
                l1=[e.Length for e in e1]
                minl=min(min(l0),min(l1))
                if minl>=r:
                    redge.append(edge)
                else:
                    if alledge:
                        redge.append(edge)
                    if debug:
                        print('convex pass %d but length %f,r=%f'%(iedge,minl,r),minl>=r)
                    pass
#    fillet1=body.newObject("PartDesign::Fillet","Fillet")
    rindex=[i for i,g in enumerate(egs) if g.hashCode() in [e.hashCode() for e in redge]]
    #for i in r1index:
    #    FreeCADGui.Selection.addSelection('chassisbox','metalbend','rackbkcut.Edge%d'%(i+1),0,0,0)
    return rindex

def ztopplane(baseobj,zoffset):
    body=baseobj._Body
    ztopplane=body.newObject('PartDesign::Plane','Ztopplane')
    ztopplane.AttachmentOffset = FreeCAD.Placement(FreeCAD.Vector(0.0000000000, 0.0000000000, zoffset),  FreeCAD.Rotation(0.0000000000, 0.0000000000, 0.0000000000))
    ztopplane.MapReversed = False
    if FreeCAD.Version()[0]=='1':
        ztopplane.AttachmentSupport = [(FreeCAD.getDocument('chassisbox').getObject('XY_Plane'),'')]
    else:
        ztopplane.Support = [(baseobj.Document.getObject('XY_Plane'),'')]
    ztopplane.MapPathParameter = 0.000000
    ztopplane.MapMode = 'ObjectXY'
    ztopplane.Visibility= 0
    return ztopplane


def linearclinearc(dx,dy,centerx,centery,r1,r2):
    geoList=[]
    sgx=numpy.sign(dx)
    sgy=numpy.sign(dy)
    dyr1=numpy.sqrt(r1**2-dx**2)
    yr1=centery+sgy*dyr1
    ayr1=numpy.arctan2(sgy*dyr1,dx)
    dyr2=numpy.sqrt(r2**2-dx**2)
    yr2=centery+sgy*dyr2
    ayr2=numpy.arctan2(sgy*dyr2,dx)
    dxr1=numpy.sqrt(r1**2-dy**2)
    xr1=centerx+sgx*dxr1
    axr1=numpy.arctan2(dy,sgx*dxr1)
    dxr2=numpy.sqrt(r2**2-dy**2)
    xr2=centerx+sgx*dxr2
    axr2=numpy.arctan2(dy,sgx*dxr2)
    x=centerx+dx
    y=centery+dy

    ar10=axr1 if (axr1-ayr1)%(2*numpy.pi)>numpy.pi else ayr1
    ar11=ayr1 if (axr1-ayr1)%(2*numpy.pi)>numpy.pi else axr1
    ar20=axr2 if (axr2-ayr2)%(2*numpy.pi)>numpy.pi else ayr2
    ar21=ayr2 if (axr2-ayr2)%(2*numpy.pi)>numpy.pi else axr2

    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(centerx,centery,0),FreeCAD.Vector(0,0,1),r1),ar10,ar11))
    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(centerx,centery,0),FreeCAD.Vector(0,0,1),r2),ar20,ar21))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x,yr1,0),FreeCAD.Vector(x,yr2,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(xr1,y,0),FreeCAD.Vector(xr2,y,0)))
    return geoList

def arc4(dx,dy,centerx,centery,r1,r2):
    geoList = []
    for x in [-dx,dx]:
        for y in [-dy,dy]:
            geoList.extend(linearclinearc(dx=x,dy=y,centerx=centerx,centery=centery,r1=r1,r2=r2))
    return geoList
def minarcpp(ccenter,cp1,cp2,r):
    angle1=numpy.angle(cp1-ccenter)
    angle2=numpy.angle(cp2-ccenter)
    return minarcaa(ccenter.real,ccenter.imag,angle1,angle2,r)

def minarcaa(centerx,centery,angle1,angle2,r):
    ar0=angle1 if (angle1 - angle2)%(2*numpy.pi)>numpy.pi else angle2
    ar1=angle2 if (angle1 - angle2)%(2*numpy.pi)>numpy.pi else angle1
    return Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(centerx,centery,0),FreeCAD.Vector(0,0,1),r),ar0,ar1)

def fanoutcut(centerx,centery,hx,hy,dx,dy,r1,r2,r3,xmax,ymax):
    sgx=numpy.sign(dx)
    sgy=numpy.sign(dy)
    ccenter=centerx+1j*centery
    ch=hx+1j*hy
    ah=numpy.angle(ch)
    d=abs(ch)
    ad=numpy.arccos((d**2+r3**2-r2**2)/(2*r3*d))
    cp1=ccenter+ch-r3*numpy.exp(1j*(numpy.angle(ch)+ad))
    cp2=ccenter+ch-r3*numpy.exp(1j*(numpy.angle(ch)-ad))
    dxr1=sgx*numpy.sqrt(r1**2-dy**2)
    xr1=centerx+dxr1
    dyr1=sgy*numpy.sqrt(r1**2-dx**2)
    yr1=centery+dyr1
    dxr2=sgx*numpy.sqrt(r2**2-ymax**2)
    xr2=centerx+dxr2
    dyr2=sgy*numpy.sqrt(r2**2-xmax**2)
    yr2=centery+dyr2
    cx=ccenter+dx+1j*dyr2
    cy=ccenter+dxr2+1j*dy
    cxmax=ccenter+sgx*xmax+1j*dy
    cymax=ccenter+dx+1j*sgy*ymax
    x=centerx+dx
    y=centery+dy

    geoList=[]
    geoList.append(Part.LineSegment(FreeCAD.Vector(xr1,y,0),FreeCAD.Vector(cxmax.real,cxmax.imag,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(cxmax.real,cxmax.imag,0),FreeCAD.Vector(cxmax.real,yr2,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x,yr1,0),FreeCAD.Vector(cymax.real,cymax.imag,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(cymax.real,cymax.imag,0),FreeCAD.Vector(xr2,cymax.imag,0)))
    geoList.append(minarcpp(ccenter=ccenter,cp1=x+1j*yr1,cp2=xr1+1j*y,r=r1))
    #geoList.append(minarcpp(ccenter=ccenter,cp1=cx,cp2=cp1,r=r2))
    geoList.append(minarcpp(ccenter=ccenter,cp1=xr2+1j*cymax.imag,cp2=cp2 if abs(numpy.angle((cp1-ccenter)/(cy-ccenter)))<abs(numpy.angle((cp2-ccenter)/(cy-ccenter))) else cp1,r=r2))
    geoList.append(minarcpp(ccenter=ccenter,cp1=cxmax.real+1j*yr2,cp2=cp2 if abs(numpy.angle((cp1-ccenter)/(cx-ccenter)))<abs(numpy.angle((cp2-ccenter)/(cx-ccenter))) else cp1,r=r2))
    geoList.append(minarcpp(ccenter=ccenter+ch,cp1=cp1,cp2=cp2 ,r=r3))
    return geoList

def rslide22(x,y):
    geoList = []
    for x0 in [26,26+39+89,26+39+89+288]:
        geoList.extend(hole(x0=x+x0,y0=y,r=4/2))
    return geoList
def fan60(centerx,centery):
    geoList = []
    for r1,r2 in [(12,17),(19,24)]:
        geoList.extend(arc4(dx=2,dy=2,centerx=centerx,centery=centery,r1=r1,r2=r2))
    for hx in [-25,25]:
        for hy in [-25,25]:
#            geoList.append(Part.Circle(FreeCAD.Vector(centerx+hx,centery+hy,0),FreeCAD.Vector(0,0,1),2.25))
            geoList.append(Part.Circle(FreeCAD.Vector(centerx+hx,centery+hy,0),FreeCAD.Vector(0,0,1),0.166*25.4/2)) # 4-40 self clinch
            geoList.extend(fanoutcut(centerx=centerx,centery=centery,hx=hx,hy=hy,dx=numpy.sign(hx)*2,dy=numpy.sign(hy)*2,r1=26,r2=33,r3=5.3,xmax=29,ymax=29))
    return geoList

def ftphole(doc,body,x,y,r,refplane):
    tpholesketch=body.newObject('Sketcher::SketchObject','tpholesketch')
    if FreeCAD.Version()[0]=='1':
        tpholesketch.AttachmentSupport = (doc.getObject('Ztopplane'),[''])
    else:
        tpholesketch.Support = (doc.getObject('Ztopplane'),[''])
    tpholesketch.MapMode = 'FlatFace'
    for xp in x:
        for yp in y:
            tpholesketch.addGeometry(Part.Circle(FreeCAD.Vector(xp,yp,0),FreeCAD.Vector(0,0,1),r),False)
    tpholesketch.Visibility= 0
    return tpholesketch
def ffprphole(doc,body,x,z,r):
    fprpholesketch=body.newObject('Sketcher::SketchObject','fprpholesketch')
    if FreeCAD.Version()[0]=='1':
        fprpholesketch.AttachmentSupport = (doc.getObject('XZ_Plane'),[''])
    else:
        fprpholesketch.Support = (doc.getObject('XZ_Plane'),[''])
    fprpholesketch.MapMode = 'FlatFace'
    for xp in x:
        for yp in z:
            fprpholesketch.addGeometry(Part.Circle(FreeCAD.Vector(xp,yp,0),FreeCAD.Vector(0,0,1),r),False)
    fprpholesketch.Visibility= 0
    return fprpholesketch
def rackbrackethole(doc,body,y,z,r,plane='YZ_Plane'):
    rackbracketsketch=body.newObject('Sketcher::SketchObject','rackbracketsketch')
    if FreeCAD.Version()[0]=='1':
        rackbracketsketch.AttachmentSupport = (doc.getObject(plane),[''])
    else:
        rackbracketsketch.Support = (doc.getObject(plane),[''])
    rackbracketsketch.MapMode = 'FlatFace'
    for yp in y:
        for zp in z:
            rackbracketsketch.addGeometry(Part.Circle(FreeCAD.Vector(yp,zp,0),FreeCAD.Vector(0,0,1),r),False)
    rackbracketsketch.Visibility= 0
    return rackbracketsketch
def fprpbracket(doc,body,x,y,r):
    fprpbracketsketch=body.newObject('Sketcher::SketchObject','fprpbracketsketch')
    if FreeCAD.Version()[0]=='1':
        fprpbracketsketch.AttachmentSupport = (doc.getObject('XY_Plane'),[''])
    else:
        fprpbracketsketch.Support = (doc.getObject('XY_Plane'),[''])
    fprpbracketsketch.MapMode = 'FlatFace'
    for xp in x:
        for yp in y:
            fprpbracketsketch.addGeometry(Part.Circle(FreeCAD.Vector(xp,yp,0),FreeCAD.Vector(0,0,1),r),False)
    fprpbracketsketch.Visibility= 0
    return fprpbracketsketch

#def baseplategrid(doc,body,x,y,r):
#    baseplategridsketch=body.newObject('Sketcher::SketchObject','baseplategridsketch')
#    if FreeCAD.Version()[0]=='1':
#        baseplategridsketch.AttachmentSupport = (doc.getObject('XY_Plane'),[''])
#    else:
#        baseplategridsketch.Support = (doc.getObject('XY_Plane'),[''])
#    baseplategridsketch.MapMode = 'FlatFace'
#    for xp in x:
#        for yp in y:
#            baseplategridsketch.addGeometry(Part.Circle(FreeCAD.Vector(xp,yp,0),FreeCAD.Vector(0,0,1),r),False)
#    baseplategridsketch.Visibility= 0
#    return baseplategridsketch
def panelparts(baseobj,parts,plane='XY_Plane'):
    doc=baseobj.Document
    body=baseobj._Body
    partcut=body.newObject('Sketcher::SketchObject','partcut')
    if FreeCAD.Version()[0]=='1':
        partcut.AttachmentSupport = (doc.getObject(plane),[''])
    else:
        partcut.Support = (doc.getObject(plane),[''])
    partcut.MapMode = 'FlatFace'
    geoList=[]
    for part,x,y,paradict in parts:
        if  part=='SMA':
            geoList.extend(smapanel(x0=x,y0=y))
        elif part=='BRACKET4334':
            geoList.extend(brackethole(x0=x,y0=y,r=1.8))
        elif part=='BRACKET617':
            geoList.extend(brackethole(x0=x,y0=y,r=1.8))
        elif part=='BRACKET621':
            geoList.extend(brackethole(x0=x,y0=y,r=1.4))
        elif part=='VENTHOLE30':
            geoList.extend(venthole(x0=x,y0=y,r=1.5))
        elif part=='roundhole':
            geoList.extend(hole(x0=x,y0=y,**paradict))
        elif part=='holeslotx':
            geoList.extend(holeslotx(x=x,y=y,**paradict))
        elif part=='holesloty':
            geoList.extend(holesloty(x=x,y=y,**paradict))
        elif part=='rectcut':
            geoList.extend(rectcut(x=x,y=y,**paradict))
#        elif part=='powerbutton':
#            geoList.extend(hole(x0=x,y0=y,r=8))
        elif part=='rj45holder_y':
            geoList.extend(rj45holder_y(x=x,y=y))
        elif part=='rj45holder_x':
            geoList.extend(rj45holder_x(x=x,y=y))
        elif part=='powerholder':
            geoList.extend(powerholder(x=x,y=y))
        elif part=='rackmounthole':
            geoList.extend(rackmounthole(x=x,y=y))
        elif part=='handlehole':
            geoList.extend(handlehole(x=x,y=y))
        elif part=='keyhole632':
            geoList.extend(keyhole632(x=x,y=y))
        elif part=='multilinecut':
            geoList.extend(multilinecut(x=x,y=y,**paradict))
#        elif part=='power6p_1':
#            geoList.extend(power6p_1(x=x,y=y))
#        elif part=='zcu216standoff':
#            geoList.extend(zcu216standoff(x=x,y=y))
#        elif part=='diffcarrier':
#            geoList.extend(diffcarrier(x=x,y=y))
#        elif part=='gridhole':
#            geoList.extend(gridhole(x=x,y=y,Nx=14,Ny=3,dx=25.4,dy=4*25.4,r=2.2))
        elif part=='screwM2x0.4tap':
            geoList.extend(hole(x0=x,y0=y,r=1.66/2))
        elif part=='screwM2x0.4closefit':
            geoList.extend(hole(x0=x,y0=y,r=2.2/2))
        elif part=='screwM2x0.4freefit':
            geoList.extend(hole(x0=x,y0=y,r=2.4/2))
        elif part=='screw2-56tap':
            geoList.extend(hole(x0=x,y0=y,r=0.07*25.4/2))
        elif part=='screw2-56closefit':
            geoList.extend(hole(x0=x,y0=y,r=0.089*25.4/2))
        elif part=='screw2-56freefit':
            geoList.extend(hole(x0=x,y0=y,r=0.096*25.4/2))
        elif part=='screw2-56selfclinchingnut':
            geoList.extend(hole(x0=x,y0=y,r=0.166*25.4/2))
        elif part=='screw4-40tap':
            geoList.extend(hole(x0=x,y0=y,r=0.089*25.4/2))
        elif part=='screw4-40closefit':
            geoList.extend(hole(x0=x,y0=y,r=0.116*25.4/2))
        elif part=='screw4-40freefit':
            geoList.extend(hole(x0=x,y0=y,r=0.1285*25.4/2))
        elif part=='screw4-40selfclinchingnut':
            geoList.extend(hole(x0=x,y0=y,r=0.166*25.4/2))
        elif part=='screw6-32tap':
            geoList.extend(hole(x0=x,y0=y,r=0.1065*25.4/2))
        elif part=='screw6-32closefit':
            geoList.extend(hole(x0=x,y0=y,r=0.1440*25.4/2))
        elif part=='screw6-32freefit':
            geoList.extend(hole(x0=x,y0=y,r=0.1495*25.4/2))
        elif part=='screw6-32selfclinchingnut':
            geoList.extend(hole(x0=x,y0=y,r=0.1875*25.4/2))
        elif part=='screw8-32tap':
            geoList.extend(hole(x0=x,y0=y,r=0.1360*25.4/2))
        elif part=='screw8-32closefit':
            geoList.extend(hole(x0=x,y0=y,r=0.1695*25.4/2))
        elif part=='screw8-32freefit':
            geoList.extend(hole(x0=x,y0=y,r=0.1770*25.4/2))
        elif part=='screw8-32selfclinchingnut':
            geoList.extend(hole(x0=x,y0=y,r=0.2130*25.4/2))
        elif part=='fan60':
            geoList.extend(fan60(centerx=x,centery=y))
        elif part=='rslide22':
            geoList.extend(rslide22(x=x,y=y))
        else:
            print('????')
    partcut.addGeometry(geoList,False)
    partcut.Visibility= 0
    return partcut
#def gridhole(x,y,Nx,Ny,dx,dy,r=2.2):
#    geoList=[]
#    for ixh in range(Nx):
#        for iyh in range(Ny):
#            geoList.extend(hole(x+ixh*dx,y+iyh*dy,r=r))
#    return geoList

def handlehole(x,y):
    geoList=[]
    dx=48.41
    r=2.1
    geoList.append(Part.Circle(FreeCAD.Vector(x-dx/2,y,0),FreeCAD.Vector(0,0,1),r))
    geoList.append(Part.Circle(FreeCAD.Vector(x+dx/2,y,0),FreeCAD.Vector(0,0,1),r))
    return geoList
def rackmounthole(x,y):
    dy=4.77
    r=3.56
    return holesloty(x,y,dy,r)
def holesloty(x,y,dy,r):
    geoList=[]
    geoList.append(Part.LineSegment(FreeCAD.Vector(x+r,y-dy/2,0),FreeCAD.Vector(x+r,y+dy/2,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x-r,y-dy/2,0),FreeCAD.Vector(x-r,y+dy/2,0)))
    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(x,y-dy/2,0),FreeCAD.Vector(0,0,1),r),numpy.pi,0))
    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(x,y+dy/2,0),FreeCAD.Vector(0,0,1),r),0,-numpy.pi))
    return geoList
def holeslotx(x,y,dx,r):
    geoList=[]
    geoList.append(Part.LineSegment(FreeCAD.Vector(x-dx/2,y-r,0),FreeCAD.Vector(x+dx/2,y-r,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x-dx/2,y+r,0),FreeCAD.Vector(x+dx/2,y+r,0)))
    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(x+dx/2,y,0),FreeCAD.Vector(0,0,1),r),numpy.pi*3/2,numpy.pi/2))
    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(x-dx/2,y,0),FreeCAD.Vector(0,0,1),r),numpy.pi/2,-numpy.pi/2))
    return geoList
def rectcut(x,y,dx,dy):
    geoList=[]
    geoList.extend(rect(x-dx/2,y-dy/2,x+dx/2,y+dy/2))
    return geoList

def rj45holder_y(x,y):
    return holder2p(x=x,y=y,yrect=22.86,xrect=17.15,dhole=29.0,rhole=1.75,holedir='y')
def rj45holder_x(x,y):
    return holder2p(x=x,y=y,xrect=22.86,yrect=17.15,dhole=29.0,rhole=1.75,holedir='x')
def rj45holder(x,y,direction='x'):  # digikey:4654-CP30625S-ND
    geoList=[]
    if direction=='x':
        geoList.append(('rectcut',x,y,dict(dx=22.86,dy=17.15))) 
        geoList.append(('screw4-40tap',x+29/2,y,{})) 
        geoList.append(('screw4-40tap',x-29/2,y,{})) 
    elif direction=='y':
        geoList.append(('rectcut',x,y,dict(dy=22.86,dx=17.15))) 
        geoList.append(('screw4-40tap',x,y+29/2,{})) 
        geoList.append(('screw4-40tap',x,y-29/2,{})) 
    else:
        print('direction should be x or y')
    return  geoList
def powerholder(x,y):
    geoList=[]
    xs=[-60,0,60]
    r=1.9
    for xi in xs:
        geoList.append(Part.Circle(FreeCAD.Vector(x+xi,y,0),FreeCAD.Vector(0,0,1),r))
    geoList.append(Part.Circle(FreeCAD.Vector(x+30,y+16,0),FreeCAD.Vector(0,0,1),r))
    return geoList
def zcu216rp(x,y,tight=False,FMC=False):
    geoList=[]
    #geoList.extend(rect(x0=x-1,y0=y-12,x1=x+1,y1=y+40))
#    geoList.extend(rect(x0=x+52-7,y0=y-20,x1=x+81+6,y1=y+27))  # QSFP
#    geoList.extend(rect(x0=x+119.3-10,y0=y-12,x1=x+182.3+10,y1=y+25)) #FMC+
#    geoList.extend(rect(x0=x+215,y0=y-12,x1=x+287,y1=y+14)) #PWR SW,USB
    if tight:
        geoList.append(('rectcut',x+66.5,y+6,dict(dx=32,dy=28)))  # QSFP tight
    else:
        geoList.append(('rectcut',x+66.5,y+6,dict(dx=42,dy=47)))  # QSFP loose
    if FMC:
        geoList.append(('rectcut',x+151,y+6.5,dict(dx=73.3,dy=37)))  # FMC+
    geoList.append(('rectcut',x+251,y-1,dict(dx=72,dy=22)))    # PWR SW USB
    return geoList
#def zcu216standoff(x,y,r=2.12):
#    geoList=[]
#    holes=[(0,0),(0,297.94),(258.42,0),(258.42,297.94),(246.06,98.81),(246.06,268.71)]
#    for (xh,yh) in holes:
#        geoList.extend(hole(x+xh,y+yh,r=r))
##        geoList.append(Part.Circle(FreeCAD.Vector(x+xh,y+yh,0),FreeCAD.Vector(0,0,1),r=3))
#    return geoList
#def diffcarrier(x,y,r=2.12):
#    geoList=[]
#    for xh in [0,114.3,152.4]:
#        for yh in [0,241.3]:
#            geoList.extend(hole(x+xh,y+yh,r=r))
#    geoList.extend(hole(x+152.4,y+120.728,r=r))
#    return geoList
def holder2p(x,y,xrect,yrect,dhole,rhole,holedir='x'):
    geoList = []
    x0=x-xrect/2
    x1=x+xrect/2
    y0=y-yrect/2
    y1=y+yrect/2
    geoList.extend(rect(x0=x0,y0=y0,x1=x1,y1=y1))
    if holedir=='x':
        geoList.extend(hole(x-dhole/2,y,rhole))
        geoList.extend(hole(x+dhole/2,y,rhole))
    elif holedir=='y':
        geoList.extend(hole(x,y-dhole/2,rhole))
        geoList.extend(hole(x,y+dhole/2,rhole))
    else:
        print('other direction, not yet')

    return geoList


def outlineboard(doc,body,x,y,outlinept):
    basesketch=body.newObject('Sketcher::SketchObject','basesketch')
    if FreeCAD.Version()[0]=='1':
        basesketch.AttachmentSupport = (doc.getObject('XY_Plane'),[''])
    else:
        basesketch.Support = (doc.getObject('XY_Plane'),[''])
    basesketch.MapMode = 'FlatFace'
    geoList = []
    for i,(x1,y1) in enumerate(outlinept[:-1]):
        x2,y2=outlinept[i+1]
        geoList.append(Part.LineSegment(FreeCAD.Vector(x+x1,y+y1,0),FreeCAD.Vector(x+x2,y+y2,0)))
    basesketch.addGeometry(geoList,False)
    basesketch.Visibility= 0
    return basesketch
def outlinepad(doc,body,x,y,outlinept,thickness):
    basesketch=outlineboard(doc,body,x,y,outlinept)
    pad=body.newObject('PartDesign::Pad','Pad')
    pad.Profile=basesketch
    pad.Length=thickness
    doc.recompute()
    return pad
def ffpboard(doc,body,x,z,mid=True):
    x2=x/2
    z2=z/2
    z0=-z2 if mid else 0
    z1=z2 if mid else z
    outlinept=[
            (-x2,z0),(-x2,z1),(x2,z1),(x2,z0),(-x2,z0)
            ]
    return outlineboard(doc=doc,body=body,x=0,y=0,outlinept=outlinept)
def rect(x0,y0,x1,y1):
    geoList = []
    geoList.append(Part.LineSegment(FreeCAD.Vector(x0,y0,0),FreeCAD.Vector(x0,y1,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x0,y1,0),FreeCAD.Vector(x1,y1,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x1,y1,0),FreeCAD.Vector(x1,y0,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x1,y0,0),FreeCAD.Vector(x0,y0,0)))
    return geoList
def hole(x0,y0,r):
    geoList = []
    geoList.append(Part.Circle(FreeCAD.Vector(x0,y0,0),FreeCAD.Vector(0,0,1),r))
    return geoList
def keyhole632(x,y):
    rhole=0.189*25.4/2
    return keyhole(x0=x,y0=y,r0=rhole,dx=0,dy=10,r1=2*rhole)
def keyhole(x0,y0,r0,dx,dy,r1):
    x1=x0+dx
    y1=y0+dy
    geoList = []
    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(x0,y0,0),FreeCAD.Vector(0,0,1),r0),numpy.pi,0))
    y=numpy.sqrt(r1**2-r0**2)
    arc0=numpy.arctan2(-y,-r0)
    arc1=numpy.arctan2(-y,r0)
    geoList.append(Part.ArcOfCircle(Part.Circle(FreeCAD.Vector(x1,y1,0),FreeCAD.Vector(0,0,1),r1),arc1,arc0))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x0-r0,y0,0),FreeCAD.Vector(x0-r0,y1-y,0)))
    geoList.append(Part.LineSegment(FreeCAD.Vector(x0+r0,y0,0),FreeCAD.Vector(x0+r0,y1-y,0)))
    return geoList
def venthole(x0,y0,r):
    return hole(x0,y0,r)
def brackethole(x0,y0,r):
    return hole(x0,y0,r)
def multilinecut(x,y,xys): #part Molex 2070170006
    geoList = []
    for ixy in range(len(xys)-1):
        x1,y1=xys[ixy]
        x2,y2=xys[ixy+1]
        geoList.append(Part.LineSegment(FreeCAD.Vector(x+x1,y+y1,0),FreeCAD.Vector(x+x2,y+y2,0)))
    return geoList

def power6p_2(x,y): #part Molex 0039012061
    xy=((0,-5.0)
,(7.1,-5.0)
,(7.1,-2.8)
,(9.6,-2.8)
,(9.6,2.8)
,(7.1,2.8)
,(7.1,5.0)
,(2,5.0)
,(2,6.6)
,(-2,6.6)
,(-2,5)
,(-7.1,5)
,(-7.1,2.8)
,(-9.6,2.8)
,(-9.6,-2.8)
,(-7.1,-2.8)
,(-7.1,-5)
,(0,-5))
    geoList = []
#    for ixy in range(len(xy)-1):
#        x1,y1=xy[ixy]
#        x2,y2=xy[ixy+1]
#        geoList.append(Part.LineSegment(FreeCAD.Vector(x+x1,y+y1,0),FreeCAD.Vector(x+x2,y+y2,0)))
    geoList.append(('multilinecut',x,y,dict(xys=xy)))
    return geoList
def power6p_1(x,y): #part Molex 2070170006
    xy=((0,-5),(7.1,-5),(7.1,-2.75),(10.45,-2.75),(10.45,2.75),(7.1,2.75),(7.1,5),(2,5),(2,6.6),(-2,6.6),(-2,5),(-7.1,5),(-7.1,2.75),(-10.45,2.75),(-10.45,-2.75),(-7.1,-2.75),(-7.1,-5),(0,-5))
    geoList = []
#    for ixy in range(len(xy)-1):
#        x1,y1=xy[ixy]
#        x2,y2=xy[ixy+1]
#        geoList.append(Part.LineSegment(FreeCAD.Vector(x+x1,y+y1,0),FreeCAD.Vector(x+x2,y+y2,0)))
    geoList.append(('multilinecut',x,y,dict(xys=xy)))
    return geoList

def smapanelmount(x,y,screw='2-56tap'):
    geoList = []
    geoList.append(('roundhole',x,y,dict(r=3.25))) #Part.Circle(FreeCAD.Vector(x0,y0,0),FreeCAD.Vector(0,0,1),3.25))
    for xp in [-4.32,4.32]:
        for yp in [-4.32,4.32]:
            geoList.append(('screw%s'%screw,x+xp,y+yp,{})) #Part.Circle(FreeCAD.Vector(x0+xp,y0+yp,0),FreeCAD.Vector(0,0,1),0.07*25.4/2))
    return geoList
    
def smapanel(x0,y0):
    geoList = []
    geoList.append(Part.Circle(FreeCAD.Vector(x0,y0,0),FreeCAD.Vector(0,0,1),3.25))
    for xp in [-4.32,4.32]:
        for yp in [-4.32,4.32]:
            geoList.append(Part.Circle(FreeCAD.Vector(x0+xp,y0+yp,0),FreeCAD.Vector(0,0,1),0.07*25.4/2))
    return geoList
    
def rectpanel(x,z,filepath,material,parts=[],rcorner=0,mid=0,outeredge=None):
    mpara=materialinfo[material[0]][material[1]]#["5052 H32 Aluminum"]["0.040"]
    thickness=mpara['thickness']
    print(thickness)
    x2=x/2
    z2=z/2
    z0=-z2 if mid else 0
    z1=z2 if mid else z
    outlinept=[
            (-x2,z0),(-x2,z1),(x2,z1),(x2,z0),(-x2,z0)
            ]
    return outlinepanel(0,0,outlinept,filepath,thickness,parts,rcorner,outeredge)
def outlinepanel(x,z,outlinept,filepath,thickness=3,parts=[],rcorner=0,outeredge=None):
    basename=os.path.splitext(os.path.basename(filepath))[0]
    dirname=os.path.dirname(filepath)
    
    doc=FreeCAD.newDocument(basename)
    body=doc.addObject('PartDesign::Body',basename)
    body.Visibility=True
    #FreeCADGui.ActiveDocument.ActiveView.setActiveObject('pdbody',FreeCAD.getDocument(basename).getObject(basename))
#    fpboard=ffpboard(doc=doc,body=body,x=x,z=z,mid=False) 
#    fpboard=outlineboard(doc,body,x,z,outlinept)
#    bobj=body.newObject('PartDesign::Pad','Pad')
#    bobj.Profile=fpboard
#    bobj.Length=thickness
#    doc.recompute()
    bobj=outlinepad(doc,body,x=x,y=z,outlinept=outlinept,thickness=thickness)
    if rcorner>0:
        bobj=edgefillet(baseobj=bobj,length=thickness,r=rcorner)
#        bobj=body.newObject("PartDesign::Fillet","Fillet")
#        #outeredge =[1,2,5,8]
#        #filletout.Base=(pad,['Edge%d'%edgeindex for edgeindex in outeredge])
#        alledges=pad.Shape.Edges
#        zedge=['Edge%d'%(i+1) for i in range(len(alledges)) if edge_direction(alledges[i])==FreeCAD.Vector(0,0,1)]
#        bobj.Base=(pad,zedge)
#        bobj.Radius=rcorner
#        pad.Visibility=False
#    print('parts',parts)
    if parts:
        if isinstance(parts[0][0],list) or isinstance(parts[0][0],tuple):
            partlist=parts
        else:
            partlist=[parts]
        for p in partlist:
#            print('1',p)
#        print(parts)
            parthole=panelparts(baseobj=bobj,parts=p)
            bobj=pockethole(baseobj=bobj,name='partcut',pocketprofile=parthole,pocketreversed=1) #,length=None,pockettype=1,pocketmidplane=1):
        importDXF.export([bobj],os.path.join(dirname,basename+'.dxf')) #"/home/ghuang/tmp/zcu216chassis/frontpanel.dxf")
    bobj.Visibility=True
    bobj.Shape.exportStep(os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/frontpanel.step")
    doc.recompute()
    print(bobj.Label,bobj.Visibility)    
    print(body.Label,body.Visibility)    
    #FreeCADGui.SendMsgToActiveView("ViewFit")
    #gui ImportGui.export([body],os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/frontpanel.step")
#    if outeredge: 
#        filletout=body.newObject("PartDesign::Fillet","Fillet")
#    #    outeredge =[274,273,279,278,283,282,185,186,152,153,25,28]
#        filletout.Base=(filletin,['Edge%d'%edgeindex for edgeindex in outeredge])
#        filletout.Radius=2
#        filletin.Visibility=False
    doc.saveAs(os.path.join(dirname,basename+'.FCStd'))
    return doc
def pockethole(baseobj,name,pocketprofile,length=None,pockettype=1,pocketmidplane=1,pocketreversed=0):
    body=baseobj._Body
    cut=body.newObject('PartDesign::Pocket',name)
    cut.Profile = pocketprofile
    if length is not None:
        cut.Length=length
        cut.Type = 0
    else:
        cut.Type = pockettype
    cut.Midplane = pocketmidplane
    cut.Reversed = pocketreversed
    baseobj.Visibility=False
    cut.Visibility=True
    baseobj.Document.recompute()
    return cut
#def apecover2(x,z,cutlength,filepath,thickness=3,parts=[],rcorner=0,outeredge=None,partsyz=[],bendradius=2.52):
#    outlinept=[
#            (0,0),(-12.7+bendradius,0),(-12.7+bendradius,9.5),(-12.7,9.5),(-12.7,74-9.5),(-12.7+bendradius,74-9.5),(-12.7+bendradius,74),(12.7,74.0),(12.7,0)
#            ,(0,0)
#            ]
#    doc=outlinepanel(x=x,z=z,outlinept=outlinept,filepath=filepath,thickness=thickness,parts=parts,rcorner=0)
#    body=doc.Objects[0]
##    sidecutsketch=body.newObject('Sketcher::SketchObject','siddecutSketch')
##    sidecutsketch.Support = (doc.getObject('partcut001'),'Face6')
##    sidecutsketch.MapMode = 'FlatFace'
##    geoList=rectcut(x=-5,y=thickness/2,dx=10,dy=thickness)
##    geoList.extend(rectcut(x=-70,y=thickness/2,dx=8,dy=thickness))
##    sidecutsketch.addGeometry(geoList,False)
##    sidecutsketch.Visibility= 0
##    sidecut=body.newObject('PartDesign::Pocket','sidecut')
##    sidecut.Profile = sidecutsketch
##    sidecut.Type = 0
##    sidecut.Midplane = 0
##    sidecut.Length=cutlength
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'partcut001.Face10',0,0,0)
#    bend1=bend(doc=doc,length=8.5,radius=1,gap1=0.2,gap2=0.0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend.Face17',0,0,0)
#    bend1=bend(doc=doc,length=8.5,radius=1,gap1=0,gap2=0.2,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend001.Face49',0,0,0)
#    bend1=bend(doc=doc,length=8.5-1.015-1.61,radius=1,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend002.Face57',0,0,0)
#    bend1=bend(doc=doc,length=8.5-1.015-1.61,radius=1,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend003.Face65',0,0,0)
#    bend1=bend(doc=doc,length=9.0,radius=1,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend004.Face73',0,0,0)
#    bend1=bend(doc=doc,length=9.0,radius=1,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#
#
#    parthole=panelparts(doc=doc,body=body,parts=partsyz,plane='YZ_Plane')
#    partcut=pockethole(body=body,name='partcutyz',pocketprofile=parthole,pocketreversed=1,length=8
#                       ) #,length=None,pockettype=1,pocketmidplane=1):
## Gui.Selection.addSelection('ape4ampcover','ape4ampcover','sidecut.Face8',-4.94569,73,0.49812)
## Gui.runCommand('SheetMetal_AddWall',0)
## Gui.Selection.clearSelection()
## Gui.Selection.addSelection('ape4ampcover','ape4ampcover','Bend.Face9',-3,74.4821,-5.80766)
## Gui.runCommand('SheetMetal_AddWall',0)
## Gui.Selection.clearSelection()
#### Begin command Std_ViewFront
## Gui.activeDocument().activeView().viewFront()
#### End command Std_ViewFront
#### Begin command Std_ViewTop
## Gui.activeDocument().activeView().viewTop()
#### End command Std_ViewTop
#
##    =body.newObject('Sketcher::SketchObject','tpholesketch')
##    tpholesketch.Support = (doc.getObject('Ztopplane'),[''])
##    tpholesketch.MapMode = 'FlatFace'
#    basename=os.path.splitext(os.path.basename(filepath))[0]
#    dirname=os.path.dirname(filepath)
#    ImportGui.export([doc.getObject('partcutyz')],os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/chassisbox.step")
##    
#    return doc
#
##    doc=rectpanel(x=25.4,z=zch-1,filepath='/home/ghuang/tmp/zcu216chassis/ape4ampcover.FCStd',thickness=3,parts=ape4ampcut,rcorner=2)
def edgefillet(baseobj,length,r,alledge=False):
    doc=baseobj.Document
    edgeindex=edgefilletindex(obj=baseobj,length=length,r=r,alledge=alledge)
    #print(edgeindex,len(edgeindex))
    if len(edgeindex)>0:
        bobj=baseobj._Body.newObject("PartDesign::Fillet","Fillet")
        bobj.Base=(baseobj,['Edge%d'%(ei+1) for ei in edgeindex])
        bobj.Radius=r
        #bobj.Visibility=False
        bobj.Visibility=True
        doc.recompute()
    else:
        bobj=baseobj
    print('exit edgefillet')
    return bobj
#def apecover(x,z,cutlength,filepath,thickness=3,parts=[],rcorner=0,outeredge=None,partsyz=[]):
#    outbend=2.52
#    outlinept=[
#            (0,0)
#,(-3,0)
#,(-3,outbend)
#,(-12.7,outbend)
#,(-12.7,74-outbend)
#,(-3,74-outbend)
#,(-3,74.0)
#,(12.7,74.0)
#,(12.7,0)
#            ,(0,0)
#            ]
#    doc=outlinepanel(x=x,z=z,outlinept=outlinept,filepath=filepath,thickness=thickness,parts=parts,rcorner=0)
#    body=doc.Objects[0]
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'partcut001.Face11',0,0,0)
#    bend1=bend(doc=doc,length=10,radius=1,gap1=0.0,gap2=0.2,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend.Face13',0,0,0)
#    bend1=bend(doc=doc,length=10,radius=1,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend001.Face46',0,0,0)
#    bend1=bend(doc=doc,length=8.5,radius=1,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#    FreeCADGui.Selection.addSelection(doc.Label,doc.Label,'Bend002.Face52',0,0,0)
#    bend1=bend(doc=doc,length=8.5,radius=1,gap1=0,gap2=0,extend1=0,extend2=0,angle=-90)
#    FreeCADGui.Selection.clearSelection()
#
#    parthole=panelparts(doc=doc,body=body,parts=partsyz,plane='YZ_Plane')
#    partcut=pockethole(body=body,name='partcutyz',pocketprofile=parthole,pocketreversed=1) #,length=None,pockettype=1,pocketmidplane=1):
#    
#    return doc
#
##    doc=rectpanel(x=25.4,z=zch-1,filepath='/home/ghuang/tmp/zcu216chassis/ape4ampcover.FCStd',thickness=3,parts=ape4ampcut,rcorner=2)
def pointinfaceindex(obj,x,y,z,torlance=0.001):
    p=FreeCAD.Vector(x,y,z)
    index=[i for i,f in enumerate(obj.Shape.Faces) if f.isInside(p,torlance,True)]
    if len(index)>0:
        return index[0]
    else:
        return None
    
#def chassisbox(x,filepath,y=None,z=None,thickness=3,bendingradius=0,minholeedge=0,edgewidth=20,topthickness=0,fprpmounthole=[],topmounthole=[],fprpbrackethole=[],basegridhole=[],sidehole=[]):
#    basename=os.path.splitext(os.path.basename(filepath))[0]
#    dirname=os.path.dirname(filepath)
#
#    doc=FreeCAD.newDocument(basename)
#    doc.saveAs(os.path.join(dirname,basename+'.FCStd') )#u"/home/ghuang/tmp/zcu216chassis/chassisbox.FCStd")
#    body=doc.addObject('PartDesign::Body','metalbend')
#    FreeCADGui.ActiveDocument.ActiveView.setActiveObject('pdbody',FreeCAD.getDocument(basename).getObject('metalbend'))
#    #basesketch=fbasesketch(doc=doc,body=body,x=x,y=y) 
#    basesketch=ffpboard(doc=doc,body=body,x=x,z=y) 
#    #doc.recompute()
#    pad=body.newObject('PartDesign::Pad','Pad')
#    pad.Profile=basesketch
#    pad.Length=thickness
#    doc.recompute()
#    print(z-2*bendingradius-2*thickness-topthickness)
##    p=FreeCAD.Vector(-209.07643, -276.7838, 41.53)
#    FreeCAD.Vector(-207.26033,0,0.8)
#    bobj=    bend(doc=doc,body=body,base='Pad',face='Face%d'%(pointinfaceindex(pad,-207.26033,0,0.8)+1),     length=z-2*bendingradius-2*thickness-topthickness,radius=bendingradius,gap1=bendingradius+thickness,gap2=bendingradius+thickness,extend1=0,extend2=0)
#    bobj=bend(doc=doc,body=body,base=bobj.Label,face='Face%d'%(pointinfaceindex(bobj,-209.07643, -276.7838, 41.53)+1),   length=edgewidth,radius=bendingradius,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius)
#    bobj=bend(doc=doc,body=body,base=bobj.Label,face='Face%d'%(pointinfaceindex(bobj,-209.07643, 276.7838, 41.53)+1),length=edgewidth,radius=bendingradius,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius)
#    bobj=bend(doc=doc,body=body,base=bobj.Label,face='Face%d'%(pointinfaceindex(bobj,-209.07643, 0, 80.4438)+1),length=edgewidth,radius=bendingradius,gap1=0,gap2=0,extend1=0.9*bendingradius+thickness,extend2=0.9*bendingradius+thickness)
#    bobj=bend(doc=doc,body=body,base=bobj.Label,face='Face%d'%(pointinfaceindex(bobj,207.26033,0,0.8)+1),length=z-2*bendingradius-2*thickness-topthickness,radius=bendingradius,gap1=bendingradius+thickness,gap2=bendingradius+thickness,extend1=0,extend2=0)
#    bobj=bend(doc=doc,body=body,base=bobj.Label,face='Face%d'%(pointinfaceindex(bobj,209.07643,-276.7838,41.53)+1),length=edgewidth,radius=bendingradius,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius)
#    bobj=bend(doc=doc,body=body,base=bobj.Label,face='Face%d'%(pointinfaceindex(bobj,209.07643,276.7838,41.53)+1),length=edgewidth,radius=bendingradius,gap1=0,gap2=0,extend1=0.9*bendingradius,extend2=0.9*bendingradius)
#    bobj=bend(doc=doc,body=body,base=bobj.Label,face='Face%d'%(pointinfaceindex(bobj,209.07643,0,80.4438)+1),length=edgewidth,radius=bendingradius,gap1=0,gap2=0,extend1=0.9*bendingradius+thickness,extend2=0.9*bendingradius+thickness)
#    if 1:
#        outeredge=edgefilletindex(bobj,thickness,r=2)
#        filletout=body.newObject("PartDesign::Fillet","Fillet")
#        filletout.Base=(bobj,['Edge%d'%(edgeindex+1) for edgeindex in outeredge])
#        filletout.Radius=2
#        bobj.Visibility=False
#        doc.recompute()
#        inneredge=edgefilletindex(obj=doc.getObject(filletout.Label),length=thickness,r=0.5,err=1e-6)
#        print(inneredge)
#        filletin=body.newObject("PartDesign::Fillet","Fillet")
#        filletin.Base=(filletout,["Edge%d"%(edgeindex+1) for edgeindex in inneredge])
#        filletin.Radius=0.5
#        filletout.Visibility = False
#        doc.recompute()
#
#    #
#    if 1:
#        fprphole=panelparts(doc=doc,body=body,parts=fprpmounthole,plane='XZ_Plane')
#        fprpcut=pockethole(body=body,name='fprpcut',pocketprofile=fprphole,pocketmidplane=1)
#        
#        ztopplane(doc=doc,body=body,zoffset=z-topthickness)
#        tphole=panelparts(doc=doc,body=body,parts=topmounthole,plane='Ztopplane') 
#        tpcut=pockethole(body=body,name='tpcut',pocketprofile=tphole,length=thickness,pocketmidplane=0)
#       
#        fprpbkhole=panelparts(doc=doc,body=body,parts=fprpbrackethole,plane='XY_Plane')
#        fprpbracketcut=pockethole(body=body,name='fprpbracketcut',pocketprofile=fprpbkhole,pocketmidplane=1)
#       
#        basegridholes=panelparts(doc=doc,body=body,parts=basegridhole,plane='XY_Plane') 
#        basegridcut=pockethole(body=body,name='basegridcut',pocketprofile=basegridholes,pocketmidplane=1)
#
#        sideholes=panelparts(doc=doc,body=body,parts=sidehole,plane='YZ_Plane')
#        rackbkcut=pockethole(body=body,name='rackbkcut',pocketprofile=sideholes,pocketmidplane=1)
#        
#    doc.recompute()
#    FreeCADGui.SendMsgToActiveView("ViewFit")
#    #doc.recompute()
#    ImportGui.export([body],os.path.join(dirname,basename+'.step')) #u"/home/ghuang/tmp/zcu216chassis/chassisbox.step")
#
#    return doc,(x,y,z)
def pws3501h(x,y):
    geoList=[]
    geoList.append(('rectcut',x-50+40.5,y,dict(dx=65,dy=34)))  # Power supply place holder
    geoList.append(('screw6-32freefit',x-50+5,y,{}))  # Power supply place holder
    geoList.append(('screw6-32freefit',x+50-5,y,{}))  # Power supply place holder
    geoList.append(('screw6-32freefit',x+50-21,y,{}))  # Power supply place holder
    return geoList
#
#reload(tbox);tbox.tbox()
#
if __name__=="__main__":
    doc=tbox()#(300,400,80)

    nouse='''
#from freecad python console
import sys
#sys.path.append('/home/ghuang/tmp/zcu216chassis')
sys.path.append('/home/ghuang/tmp/qubic_hardware/chassis/zcu216chassis/scripts/')
import tbox
from importlib import reload
#FreeCADGui.activateWorkbench("SMWorkbench")
reload(tbox);tbox.tbox();
'''
