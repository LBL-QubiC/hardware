import numpy
import struct
import serial
import time
import sys
s=serial.Serial(sys.argv[1],9600,timeout=0)#,serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE,timeout=0,)
s.flushOutput()
s.flushInput()
time.sleep(0.1)
c1=s.read(s.in_waiting)
print('after open port',c1)
if 1:
    s.write(str.encode('c3'));
    s.write(str.encode('c3'));
    while s.in_waiting==0:
        s.write(str.encode('c3'));
        time.sleep(0.1)
    p2=s.read(s.in_waiting)
#    time.sleep(0.1)
    print('after c3',p2)
if 0:
    s.write(str.encode('r'));
#    time.sleep(1.5)
    p3=s.read(s.in_waiting)
    print('p3',len(p3),p3,type(len(p3)/3))
    d=[int.from_bytes(p3[3*i:3*i+3],'big') for i in range(int(len(p3)/3))]

#    d=[i for i in struct.unpack('>%dL'%(len(p3)/4),p3)]
    print('d',[hex(i) for i in d])
    print('len d',len(d))
#p3=s.readline()
if 1:
    import lmx2595
    c1=lmx2595.lmx2595()
    f1=c1.freqcalc(oscin=100e6,rfouta=float(sys.argv[2]))#,rfoutb=20.142857e6)
    manualupdate=dict(fcal_en=1)
    c1.regmapgen(**manualupdate)
    adcalc=c1.addrdata(79)
    regf=numpy.array(adcalc)
    #print('adcalc',[hex(i) for i in adcalc])

    v=regf
    #print('len v', len(v))

    s.write(str.encode('d'));
#    time.sleep(0.5)
    #w=struct.pack('>%dL'%113,*113*[772])
#    w=struct.pack('>%dB'%113*3,*v)
    w=b''.join([int(k).to_bytes(3,'big') for k in v])
    #print([hex(iv) for iv in v])
#    w=c3
    #print('w len',len(w),w)
    #ww=[w[i:i+10] for i in range(0,len(w),10)]
    for iw,wd in enumerate(w):
        s.write([wd])
        #print(hex(wd))
        if (iw%10==0):
            time.sleep(0.1)
    time.sleep(0.5)
    #print(s.readline())
    c4=s.read(s.in_waiting)
    d4=[int.from_bytes(c4[3*i:3*i+3],'big') for i in range(int(len(c4)/3))]
    print('c4',c4)
    print('len c4',len(c4))
    print('d4',[hex(i) for i in d4])
#    time.sleep(0.5)
    c5=s.read(s.in_waiting)
    print('c5',c5)
    if 1:
        #input('cont')
        s.write(str.encode('b'));
#    print(crc(w))


def crc(data8):
    length=len(data8)
    crc=0xffff
    sum1=crc
    sum2=crc>>8
    for index in range(length):
        sum1=(sum1+data8[index])&0xff
        sum2=(sum2+sum1)&0xff
    return (sum2<<8)|sum1

