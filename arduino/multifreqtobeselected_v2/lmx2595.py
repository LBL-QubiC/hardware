import fractions
import numpy
class lmx2595():
    def __init__(self,regwidth=16,**kwargs):
        self.vardict={}
        self.vardict.update(dict(dblr_ibias_ctrl1=3115))   # datasheet p55, better output power
        self.vardict.update(dict(fsysref=None))   # datasheet p55, better output power
        self.regwidth=regwidth
        self.regstatic={}
        self.regstatic.update({0:dict(static=0x2410,mask=0x3c10)})
        self.regstatic.update({1:dict(static=0x0808,mask=0xfff8)})
        self.regstatic.update({2:dict(static=0x0500,mask=0xffff)})
        self.regstatic.update({3:dict(static=0x0642,mask=0xffff)})
        self.regstatic.update({4:dict(static=0x0043,mask=0x00ff)})
        self.regstatic.update({5:dict(static=0x00c8,mask=0xffff)})
        self.regstatic.update({6:dict(static=0xc802,mask=0xffff)})
        self.regstatic.update({7:dict(static=0x00b2,mask=0xbfff)})
        self.regstatic.update({8:dict(static=0x2000,mask=0xb7ff)})
        self.regstatic.update({9:dict(static=0x0604,mask=0xefff)})
        self.regstatic.update({10:dict(static=0x1058,mask=0x107f)})
        self.regstatic.update({11:dict(static=0x0008,mask=0xf00f)})
        self.regstatic.update({12:dict(static=0x5000,mask=0xf000)})
        self.regstatic.update({13:dict(static=0x4000,mask=0xffff)})
        self.regstatic.update({14:dict(static=0x1e00,mask=0xff8f)})
        self.regstatic.update({15:dict(static=0x064f,mask=0xffff)})
        self.regstatic.update({16:dict(static=0x0000,mask=0xfe00)})
        self.regstatic.update({17:dict(static=0x0000,mask=0xfe00)})
        self.regstatic.update({18:dict(static=0x0064,mask=0xffff)})
        self.regstatic.update({19:dict(static=0x2700,mask=0xff00)})
        self.regstatic.update({20:dict(static=0xc048,mask=0xc3ff)})
        self.regstatic.update({21:dict(static=0x0401,mask=0xffff)})
        self.regstatic.update({22:dict(static=0x0001,mask=0xffff)})
        self.regstatic.update({23:dict(static=0x007c,mask=0xffff)})
        self.regstatic.update({24:dict(static=0x071a,mask=0xffff)})
        self.regstatic.update({25:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({26:dict(static=0x0db0,mask=0xffff)})
        self.regstatic.update({27:dict(static=0x0002,mask=0xfffe)})
        self.regstatic.update({28:dict(static=0x0488,mask=0xffff)})
        self.regstatic.update({29:dict(static=0x318c,mask=0xffff)})
        self.regstatic.update({30:dict(static=0x318c,mask=0xffff)})
        self.regstatic.update({31:dict(static=0x03ec,mask=0xbfff)})
        self.regstatic.update({32:dict(static=0x0393,mask=0xffff)})
        self.regstatic.update({33:dict(static=0x1e21,mask=0xffff)})
        self.regstatic.update({34:dict(static=0x0000,mask=0xfff8)})
        self.regstatic.update({35:dict(static=0x0004,mask=0xffff)})
        self.regstatic.update({36:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({37:dict(static=0x0004,mask=0x4004)})
        self.regstatic.update({38:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({39:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({40:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({41:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({42:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({43:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({44:dict(static=0x0000,mask=0xc018)})
        self.regstatic.update({45:dict(static=0xc0c0,mask=0xe1c0)})
        self.regstatic.update({46:dict(static=0x07fc,mask=0xfffc)})
        self.regstatic.update({47:dict(static=0x0300,mask=0xffff)})
        self.regstatic.update({48:dict(static=0x0300,mask=0xffff)})
        self.regstatic.update({49:dict(static=0x4180,mask=0xffff)})
        self.regstatic.update({50:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({51:dict(static=0x0080,mask=0xffff)})
        self.regstatic.update({52:dict(static=0x0820,mask=0xffff)})
        self.regstatic.update({53:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({54:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({55:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({56:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({57:dict(static=0x0020,mask=0xffff)})
        self.regstatic.update({58:dict(static=0x0001,mask=0x01ff)})
        self.regstatic.update({59:dict(static=0x0000,mask=0xfffe)})
        self.regstatic.update({60:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({61:dict(static=0x00a8,mask=0xffff)})
        self.regstatic.update({62:dict(static=0x0322,mask=0xffff)})
        self.regstatic.update({63:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({64:dict(static=0x1388,mask=0xffff)})
        self.regstatic.update({65:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({66:dict(static=0x01f4,mask=0xffff)})
        self.regstatic.update({67:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({68:dict(static=0x03e8,mask=0xffff)})
        self.regstatic.update({69:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({70:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({71:dict(static=0x0001,mask=0xff03)})
        self.regstatic.update({72:dict(static=0x0000,mask=0xf800)})
        self.regstatic.update({73:dict(static=0x0000,mask=0xf000)})
        self.regstatic.update({74:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({75:dict(static=0x0800,mask=0xf83f)})
        self.regstatic.update({76:dict(static=0x000c,mask=0xffff)})
        self.regstatic.update({77:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({78:dict(static=0x0001,mask=0xf401)})
        self.regstatic.update({79:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({80:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({81:dict(static=0x0000,mask=0xfffe)})
        self.regstatic.update({82:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({83:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({84:dict(static=0x0000,mask=0xfffe)})
        self.regstatic.update({85:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({86:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({87:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({88:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({89:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({90:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({91:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({92:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({93:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({94:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({95:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({96:dict(static=0x0000,mask=0x0003)})
        self.regstatic.update({97:dict(static=0x0800,mask=0x7804)})
        self.regstatic.update({98:dict(static=0x0000,mask=0x0002)})
        self.regstatic.update({99:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({100:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({101:dict(static=0x0000,mask=0xff8c)})
        self.regstatic.update({102:dict(static=0x0000,mask=0xc000)})
        self.regstatic.update({103:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({104:dict(static=0x0000,mask=0x0000)})
        self.regstatic.update({105:dict(static=0x0000,mask=0x000c)})
        self.regstatic.update({106:dict(static=0x0000,mask=0xffe8)})
        self.regstatic.update({107:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({108:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({109:dict(static=0x0000,mask=0xffff)})
        self.regstatic.update({110:dict(static=0x0000,mask=0xf91f)})
        self.regstatic.update({111:dict(static=0x0000,mask=0xff00)})
        self.regstatic.update({112:dict(static=0x0000,mask=0xfe00)})
        self.paramap={}
        self.paramap.update(dict(ramp_en=[self.regparadict(regaddr=0,regmsb=15,reglsb=15,default=0)]))
        self.paramap.update(dict(vco_phase_sync=[self.regparadict(regaddr=0,regmsb=14,reglsb=14,default=0)]))
        self.paramap.update(dict(out_mute=[self.regparadict(regaddr=0,regmsb=9,reglsb=9,default=0)]))
        self.paramap.update(dict(fcal_hpfd_adj=[self.regparadict(regaddr=0,regmsb=8,reglsb=7,default=2)]))
        self.paramap.update(dict(fcal_lpfd_adj=[self.regparadict(regaddr=0,regmsb=6,reglsb=5,default=0)]))
        self.paramap.update(dict(fcal_en=[self.regparadict(regaddr=0,regmsb=3,reglsb=3,default=0)]))
        self.paramap.update(dict(muxout_ld_sel=[self.regparadict(regaddr=0,regmsb=2,reglsb=2,default=0)]))
        self.paramap.update(dict(reset=[self.regparadict(regaddr=0,regmsb=1,reglsb=1,default=0)]))
        self.paramap.update(dict(powerdown=[self.regparadict(regaddr=0,regmsb=0,reglsb=0,default=0)]))

        self.paramap.update(dict(cal_clkdiv=[self.regparadict(regaddr=1,regmsb=2,reglsb=0,default=0)]))
        self.paramap.update(dict(out_force=[self.regparadict(regaddr=7,regmsb=14,reglsb=14,default=0)]))
        self.paramap.update(dict(osc_2x=[self.regparadict(regaddr=9,regmsb=12,reglsb=12,default=0)]))
        self.paramap.update(dict(mult=[self.regparadict(regaddr=10,regmsb=11,reglsb=7,default=1)]))
        self.paramap.update(dict(pll_r=[self.regparadict(regaddr=11,regmsb=11,reglsb=4,default=1)]))
        self.paramap.update(dict(pll_r_pre=[self.regparadict(regaddr=12,regmsb=11,reglsb=0,default=1)]))
        self.paramap.update(dict(cpg=[self.regparadict(regaddr=14,regmsb=6,reglsb=4,default=7)]))
        self.paramap.update(dict(acal_cmp_delay=[self.regparadict(regaddr=4,regmsb=15,reglsb=8,default=10)]))
        self.paramap.update(dict(vco_daciset_force=[self.regparadict(regaddr=8,regmsb=14,reglsb=14,default=0)]))
        self.paramap.update(dict(vco_capctrl_force=[self.regparadict(regaddr=8,regmsb=11,reglsb=11,default=0)]))
        self.paramap.update(dict(vco_daciset=[self.regparadict(regaddr=16,regmsb=8,reglsb=0,default=128)]))
        self.paramap.update(dict(vco_daciset_strt=[self.regparadict(regaddr=17,regmsb=8,reglsb=0,default=250)]))
        self.paramap.update(dict(vco_capctrl=[self.regparadict(regaddr=19,regmsb=7,reglsb=0,default=183)]))
        self.paramap.update(dict(vco_sel=[self.regparadict(regaddr=20,regmsb=13,reglsb=11,default=7)]))
        self.paramap.update(dict(vco_sel_force=[self.regparadict(regaddr=20,regmsb=10,reglsb=10,default=0)]))
        self.paramap.update(dict(pll_n=[self.regparadict(regaddr=34,paramsb=18,paralsb=16,regmsb=2,reglsb=0,default=100),
                                   self.regparadict(regaddr=36,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=100)]))
        self.paramap.update(dict(mash_seed_en=[self.regparadict(regaddr=37,regmsb=15,reglsb=15,default=0)]))
        self.paramap.update(dict(pfd_dly_sel=[self.regparadict(regaddr=37,regmsb=13,reglsb=8,default=2)]))
        self.paramap.update(dict(pll_den=[self.regparadict(regaddr=38,paramsb=31,paralsb=16,regmsb=15,reglsb=0,default=4294967295),
                                   self.regparadict(regaddr=39,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=4294967295)]))
        self.paramap.update(dict(mash_seed=[self.regparadict(regaddr=40,paramsb=31,paralsb=16,regmsb=15,reglsb=0,default=0),
                                   self.regparadict(regaddr=41,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=0)]))
        self.paramap.update(dict(pll_num=[self.regparadict(regaddr=42,paramsb=31,paralsb=16,regmsb=15,reglsb=0,default=0),
                                   self.regparadict(regaddr=43,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=0)]))
        self.paramap.update(dict(outa_pwr=[self.regparadict(regaddr=44,regmsb=13,reglsb=8,default=31)]))
        self.paramap.update(dict(outb_pd=[self.regparadict(regaddr=44,regmsb=7,reglsb=7,default=1)]))
        self.paramap.update(dict(outa_pd=[self.regparadict(regaddr=44,regmsb=6,reglsb=6,default=0)]))
        self.paramap.update(dict(mash_reset_n=[self.regparadict(regaddr=44,regmsb=5,reglsb=5,default=1)]))
        self.paramap.update(dict(mash_order=[self.regparadict(regaddr=44,regmsb=2,reglsb=0,default=0)]))
        self.paramap.update(dict(outa_mux=[self.regparadict(regaddr=45,regmsb=12,reglsb=11,default=1)]))
        self.paramap.update(dict(out_iset=[self.regparadict(regaddr=45,regmsb=10,reglsb=9,default=0)]))
        self.paramap.update(dict(outb_pwr=[self.regparadict(regaddr=45,regmsb=5,reglsb=0,default=31)]))
        self.paramap.update(dict(outb_mux=[self.regparadict(regaddr=46,regmsb=1,reglsb=0,default=1)]))
        self.paramap.update(dict(inpin_ignore=[self.regparadict(regaddr=58,regmsb=15,reglsb=15,default=1)]))
        self.paramap.update(dict(inpin_hyst=[self.regparadict(regaddr=58,regmsb=14,reglsb=14,default=0)]))
        self.paramap.update(dict(inpin_lvl=[self.regparadict(regaddr=58,regmsb=13,reglsb=12,default=0)]))
        self.paramap.update(dict(inpin_fmt=[self.regparadict(regaddr=58,regmsb=11,reglsb=9,default=0)]))
        self.paramap.update(dict(ld_type=[self.regparadict(regaddr=59,regmsb=0,reglsb=0,default=1)]))
        self.paramap.update(dict(ld_dly=[self.regparadict(regaddr=60,regmsb=15,reglsb=0,default=1000)]))
        self.paramap.update(dict(mash_rst_count=[self.regparadict(regaddr=69,paramsb=31,paralsb=16,regmsb=15,reglsb=0,default=50000),
                                   self.regparadict(regaddr=70,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=50000)]))
        self.paramap.update(dict(sysref_div_pre=[self.regparadict(regaddr=71,regmsb=7,reglsb=5,default=4)]))
        self.paramap.update(dict(sysref_pulse=[self.regparadict(regaddr=71,regmsb=4,reglsb=4,default=0)]))
        self.paramap.update(dict(sysref_en=[self.regparadict(regaddr=71,regmsb=3,reglsb=3,default=0)]))
        self.paramap.update(dict(sysref_repeat=[self.regparadict(regaddr=71,regmsb=2,reglsb=2,default=0)]))
        self.paramap.update(dict(sysref_div=[self.regparadict(regaddr=72,regmsb=10,reglsb=0,default=0)]))
        self.paramap.update(dict(jesd_dac1_ctrl=[self.regparadict(regaddr=73,regmsb=5,reglsb=0,default=63)]))
        self.paramap.update(dict(jesd_dac2_ctrl=[self.regparadict(regaddr=73,regmsb=11,reglsb=6,default=0)]))
        self.paramap.update(dict(jesd_dac3_ctrl=[self.regparadict(regaddr=74,regmsb=5,reglsb=0,default=0)]))
        self.paramap.update(dict(jesd_dac4_ctrl=[self.regparadict(regaddr=74,regmsb=11,reglsb=6,default=0)]))
        self.paramap.update(dict(sysref_pulse_cnt=[self.regparadict(regaddr=74,regmsb=15,reglsb=12,default=0)]))
        self.paramap.update(dict(dblr_ibias_ctrl1=[self.regparadict(regaddr=25,regmsb=15,reglsb=0,default=0x0624)]))
        self.paramap.update(dict(vco2x_en=[self.regparadict(regaddr=27,regmsb=0,reglsb=0,default=0)]))
        self.paramap.update(dict(seg1_en=[self.regparadict(regaddr=31,regmsb=14,reglsb=14,default=0)]))
        self.paramap.update(dict(chdiv=[self.regparadict(regaddr=75,regmsb=10,reglsb=6,default=0)]))
        self.paramap.update(dict(ramp_thresh=[self.regparadict(regaddr=79,paramsb=31,paralsb=16,regmsb=15,reglsb=0,default=0),self.regparadict(regaddr=80,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=0),self.regparadict(regaddr=78,paramsb=32,paralsb=32,regmsb=11,reglsb=11,default=0)]))
        self.paramap.update(dict(quick_recal_en=[self.regparadict(regaddr=78,regmsb=9,reglsb=9,default=0)]))
        self.paramap.update(dict(vco_capctrl_strt=[self.regparadict(regaddr=78,regmsb=8,reglsb=1,default=0)]))

        self.paramap.update(dict(ramp_limit_high=[self.regparadict(regaddr=82,paramsb=31,paralsb=16,regmsb=15,reglsb=0,default=0),
                                   self.regparadict(regaddr=83,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=0),
                                   self.regparadict(regaddr=81,paramsb=32,paralsb=32,regmsb=11,reglsb=11,default=0)
                                   ]))
        self.paramap.update(dict(ramp_limit_low=[self.regparadict(regaddr=85,paramsb=31,paralsb=16,regmsb=15,reglsb=0,default=0),
                                   self.regparadict(regaddr=86,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=0),
                                   self.regparadict(regaddr=84,paramsb=32,paralsb=32,regmsb=11,reglsb=11,default=0)
                                   ]))
        self.paramap.update(dict(ramp_burst_en=[self.regparadict(regaddr=96,regmsb=15,reglsb=15,default=0)]))
        self.paramap.update(dict(ramp_burst_count=[self.regparadict(regaddr=96,regmsb=14,reglsb=2,default=0)]))
        self.paramap.update(dict(ramp0_rst=[self.regparadict(regaddr=97,regmsb=15,reglsb=15,default=0)]))
        self.paramap.update(dict(ramp_triga=[self.regparadict(regaddr=97,regmsb=6,reglsb=3,default=0)]))
        self.paramap.update(dict(ramp_trigb=[self.regparadict(regaddr=97,regmsb=10,reglsb=7,default=0)]))
        self.paramap.update(dict(ramp_burst_trig=[self.regparadict(regaddr=97,regmsb=1,reglsb=0,default=0)]))
        self.paramap.update(dict(ramp0_rst=[self.regparadict(regaddr=97,regmsb=15,reglsb=15,default=0)]))
        self.paramap.update(dict(ramp0_inc=[self.regparadict(regaddr=98,paramsb=29,paralsb=16,regmsb=15,reglsb=2,default=0),
                                   self.regparadict(regaddr=99,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=0)
                                   ]))
        self.paramap.update(dict(ramp0_dly=[self.regparadict(regaddr=98,regmsb=0,reglsb=0,default=0)]))
        self.paramap.update(dict(ramp0_len=[self.regparadict(regaddr=100,regmsb=15,reglsb=0,default=0)]))
        self.paramap.update(dict(ramp0_next_trig=[self.regparadict(regaddr=101,regmsb=0,reglsb=0,default=0)]))
        self.paramap.update(dict(ramp1_dly=[self.regparadict(regaddr=101,regmsb=6,reglsb=6,default=0)]))
        self.paramap.update(dict(ramp1_rst=[self.regparadict(regaddr=101,regmsb=5,reglsb=5,default=0)]))
        self.paramap.update(dict(ramp0_next=[self.regparadict(regaddr=101,regmsb=4,reglsb=4,default=0)]))
        self.paramap.update(dict(ramp1_inc=[self.regparadict(regaddr=102,paramsb=29,paralsb=16,regmsb=13,reglsb=0,default=0),
                                   self.regparadict(regaddr=103,paramsb=15,paralsb=0,regmsb=15,reglsb=0,default=0)
                                   ]))
        self.paramap.update(dict(ramp1_len=[self.regparadict(regaddr=104,regmsb=15,reglsb=0,default=0)]))
        self.paramap.update(dict(ramp_dly_cnt=[self.regparadict(regaddr=105,regmsb=15,reglsb=6,default=0)]))
        self.paramap.update(dict(ramp_manual=[self.regparadict(regaddr=105,regmsb=5,reglsb=5,default=0)]))
        self.paramap.update(dict(ramp1_next=[self.regparadict(regaddr=105,regmsb=4,reglsb=4,default=0)]))
        self.paramap.update(dict(ramp1_next_trig=[self.regparadict(regaddr=105,regmsb=0,reglsb=0,default=0)]))
        self.paramap.update(dict(ramp_trig_cal=[self.regparadict(regaddr=106,regmsb=4,reglsb=4,default=0)]))
        self.paramap.update(dict(ramp_scale_count=[self.regparadict(regaddr=106,regmsb=0,reglsb=0,default=7)]))
        self.paramap.update(dict(rb_ld_vtune=[self.regparadict(regaddr=110,regmsb=10,reglsb=9,default=0)]))
        self.paramap.update(dict(rb_vco_sel=[self.regparadict(regaddr=110,regmsb=7,reglsb=5,default=0)]))
        self.paramap.update(dict(rb_vco_capctrl=[self.regparadict(regaddr=111,regmsb=7,reglsb=0,default=183)]))
        self.paramap.update(dict(rb_vco_daciset=[self.regparadict(regaddr=112,regmsb=8,reglsb=0,default=170)]))

        self.regs=numpy.zeros(113,dtype=int)

        #return ((var['ramp_en']&0x1)<<15)+((var['vco_phase_sync']&0x1)<<14)+((0x1)<<13)+((0x1)<<10)+((var['out_mute']&0x3)<<9)+((var['fcal_hpfd_adj']&0x3)<<7)+((var['fcal_lpfd_adj']&0x1)<<5)+((0x1)<<4)+((var['fcal_en']&0x1)<<3)+((var['muxout_ld_sel']&0x1)<<2)+((var['reset']&0x1)<<1)+((var['powerdown']&0x1)<<0)
    def regparadict(self,**kwargs):
        opt=dict(regaddr=None,paramsb=None,paralsb=None,regmsb=None,reglsb=None,default=None,val=None)
        opt.update(kwargs)
        return opt


    def regmapgen(self,**kwargs):
        #        self.manualupdate(kwargs)
        self.vardict.update(kwargs)
        varval={k:v for k,v in self.vardict.items() if k in self.paramap}
#        print(varval)
        for k,v in varval.items():
            for d in self.paramap[k]:
                d.update(dict(val=v))
        for a,s in self.regstatic.items():
            self.regs[a]=s['static']
        for k,l in self.paramap.items():
            for d in l:
                #print(l)
                self.updateval(d)#self.regs[v['regaddr']]
#                print(k,self.regs[d['regaddr']])

        return self.regs
    def addrdata(self,endaddr=79):
        ad=numpy.zeros(endaddr,dtype=int)
        for i,v in enumerate(self.regs[0:endaddr]):
            ad[i]=v+(i<<16)
        return ad

    def updateval(self,rd):
        #        print(rd)
        #print(rd['val'])
        if rd['val'] is None:
           val=rd['default']
        else:
           val=rd['val']
        if rd['paramsb'] is None and rd['paralsb'] is None:
           val=val
        elif rd['paramsb'] is not None and rd['paralsb'] is not None:
           val=(val&((1<<(rd['paramsb']+1))-1))>>rd['paralsb']
        else:
           print('paramsb and paralsb should be both None or both not None')
        mask1=((1<<(rd['regmsb']+1))-1)^((1<<rd['reglsb'])-1)
        mask0=((1<<(self.regwidth+1))-1)^mask1
        #print(bin(mask1),bin(mask0))
        #print(self.regs[rd['regaddr']])
        self.regs[rd['regaddr']]=((self.regs[rd['regaddr']]&mask0)|((val<<rd['reglsb'])&mask1))
#    def regdmf(self,oscin,rfouta=None,rfoutb=None):
#        self.fpd(oscin)
#        if rfouta is not None:
#            self.outamux(rfouta)
#
#        elif rfoutb is not None:
#            if rfoutb>10e6 and rfoutb<7.5e9:
#                self.vardict.update(dict(outa_mux=0))
#                self.vardict.update(self.table8(rfoutb))
#            elif rfoutb>=7.5e9 and rfoutb<=15e9:
#                self.vardict.update(dict(outa_mux=1,fvco=rfoutb))
#            else:
#                self.vardict.update(dict(outa_mux=2,fvco=rfoutb/2))
#        return self.vardict

        #            oscindoubler=0,prerdivider=1,multiplier=3,postrdivider=1,ndividier=28,fractionnumerator=1,fractiondenominator=1,fractionorder=0,channeldivider=0):
        pass
#    def fpd(self,oscin):
#    def freqcalc(self,oscin,rfouta):
    def freqcalc(self,oscin,rfouta=None,rfoutb=None):
        if rfouta is None and rfoutb is None:
            self.vardict.update(dict(outa_pd=1,outb_pd=1))
        elif rfouta is None and rfoutb is not None:
            if rfoutb>10e6 and rfoutb<7.5e9:
                self.vardict.update(dict(outb_mux=0,vco2x_en=0))
                self.vardict.update(self.table8(rfoutb))
                self.vardict.update(dict(outa_pd=1,outb_pd=0))
            elif rfoutb>=7.5e9 and rfoutb<=15e9:
                self.vardict.update(dict(outb_mux=1,fvco=rfoutb,vco2x_en=0))
                self.vardict.update(dict(outa_pd=1,outb_pd=0))
            else:
                self.vardict.update(dict(outb_mux=3))

        elif rfouta is not None and rfoutb is None:
            self.outamux(rfouta)
            self.vardict.update(dict(outa_pd=0,outb_pd=1))
        elif rfouta is not None and rfoutb is not None:
            self.outamux(rfouta)
            self.vardict.update(dict(outa_pd=0,outb_pd=0))
            if rfoutb==rfouta:
                self.vardict.update(dict(outb_mux=0))
            elif rfoutb==self.vardict['fvco']:
                self.vardict.update(dict(outb_mux=1))
            else:
                self.vardict.update(dict(fsysref=rfoutb))
                self.vardict.update(dict(outb_mux=2))

        self.outamux(rfouta)
        #self.fpd(oscin)
        #return self.vardict
        fvco=self.vardict['fvco']
        if fvco<10e6 or fvco>20e9:
            exit('fvco should in [10e6,20e9]')
        osc_2x=numpy.array([0,1])
        osc2xdict={0:1,1:2}
        pll_r_pre=range(1,128)
        mult=[1,3,4,5,6,7]
        pll_r=range(1,256)
        mash_order=range(5)
        sysref_div_pre=[1,2,4]
        sysref_div={i:2*(i+1)+2 for i in numpy.arange(2048)}
        fsysref=self.vardict['fsysref']
        fpds=[]
        for osc_2x_i in osc_2x:
            for pll_r_pre_i in pll_r_pre:
                for mult_i in [mult[0]] if osc_2x_i==1 else mult:
                    for pll_r_i in pll_r:
                        fpd_i=round(oscin*osc2xdict[osc_2x_i]*mult_i/(pll_r_pre_i*pll_r_i),10)
                        ratio=round(fvco/fpd_i,10)
                        for sysref_div_pre_i in ([1] if fsysref is None else sysref_div_pre):
                            finterpolator=fvco/(self.vardict['includeddivide']*sysref_div_pre_i)
                            if fsysref is not None:
                                sysref_div_est=(finterpolator/(2*fsysref)-2)/2-1
                                sysref_div_sel=[i for i in range(int(sysref_div_est-2),int(sysref_div_est+2)) if i in sysref_div]
                            else:
                                sysref_div_sel=[1]
                            for sysref_div_i in sysref_div_sel:
                                fsysref_calc=finterpolator/(2*sysref_div[sysref_div_i])
                                if fsysref is None or (abs(fsysref_calc-fsysref)<(0.01*fsysref) and finterpolator>0.8e9 and finterpolator<1.5e9):
                                    for mash_order_i in mash_order:
                                        valid=False
                                        if mash_order_i==0:
                                            if fpd_i>0.125e6 and fpd_i<400e6:
                                                if fvco%fpd_i==0:
                                                    if fvco<=12500e6:
                                                        if int(ratio)>=28:
                                                            pfd_dly_sel_i=1
                                                            valid=True
                                                    elif fvco>12500e6:
                                                        if int(ratio)>=32:
                                                            pfd_dly_sel_i=1
                                                            valid=True
                                        elif mash_order_i==1:
                                            if fpd_i>5e6 and fpd_i<300e6:
                                                if fvco<=10000e6:
                                                    if int(ratio)>=28:
                                                        pfd_dly_sel_i=1
                                                        valid=True
                                                elif fvco<=12500e6:
                                                    if int(ratio)>=32:
                                                        pfd_dly_sel_i=2
                                                        valid=True
                                                elif fvco>12500e6:
                                                    if int(ratio)>=32:
                                                        pfd_dly_sel_i=3
                                                        valid=True
                                        elif mash_order_i==2:
                                            if fpd_i>5e6 and fpd_i<300e6:
                                                if fvco<=10000e6:
                                                    if int(ratio)>=32:
                                                        pfd_dly_sel_i=2
                                                        valid=True
                                                elif fvco>10000e6:
                                                    if int(ratio)>=36:
                                                        pfd_dly_sel_i=3
                                                        valid=True
                                        elif mash_order_i==3:
                                            if fpd_i>5e6 and fpd_i<300e6:
                                                if fvco<=10000e6:
                                                    if int(ratio)>=36:
                                                        pfd_dly_sel_i=3
                                                        valid=True
                                                elif fvco>10000e6:
                                                    if int(ratio)>=40:
                                                        pfd_dly_sel_i=4
                                                        valid=True
                                        elif mash_order_i==4:
                                            if fpd_i>5e6 and fpd_i<240e6:
                                                if fvco<=10000e6:
                                                    if int(ratio)>=44:
                                                        pfd_dly_sel_i=5
                                                        valid=True
                                                elif fvco>10000e6:
                                                    if int(ratio)>=48:
                                                        pfd_dly_sel_i=6
                                                        valid=True
                                        else:
                                            print('should not happen')

                                        if valid:
                                            frac=fractions.Fraction(ratio-int(ratio)).limit_denominator(2**32)
                                            pll_num=frac.numerator
                                            pll_den=frac.denominator
                                            pll_n=int(ratio)
                                            fvcocal=fpd_i*(pll_n+(1.0*pll_num/pll_den))

                                            vardict=dict(osc_2x=osc_2x_i,mash_order=mash_order_i,fpd=fpd_i,pll_r_pre=pll_r_pre_i,mult=mult_i,pll_r=pll_r_i,pfd_dly_sel=pfd_dly_sel_i,pll_n=pll_n,pll_num=pll_num,pll_den=pll_den,fsysref_calc=fsysref_calc,fsysref=fsysref)
                                            if fsysref is not None:
                                                vardict.update(dict(sysref_div_pre=sysref_div_pre_i,sysref_div=sysref_div_i))
                                            fpds.append(vardict)

        #                        print(osc_2x_i,pll_r_pre_i,mult_i,pll_r_i,mash_order_i,pfd_dly_sel_i,pll_n,pll_num,pll_den,ratio,fvco,fvcocal-fvco,fpd_i,mash_order_i)
        print('possible combinations:',len(fpds))
        allfpds=sorted(fpds,key=lambda d:(d['osc_2x'],d['mash_order'],d['fpd']),reverse=True)
        self.vardict.update(allfpds[0])
        return allfpds







    def outamux(self,rfouta):
        if rfouta>10e6 and rfouta<7.5e9:
            self.vardict.update(dict(outa_mux=0,vco2x_en=0))
            self.vardict.update(self.table8(rfouta))
        elif rfouta>=7.5e9 and rfouta<=15e9:
            self.vardict.update(dict(outa_mux=1,fvco=rfouta,vco2x_en=0))
        elif rfouta>15e9:
            self.vardict.update(dict(outa_mux=2,fvco=rfouta/2,vco2x_en=1))
        else:
            self.vardict.update(dict(outa_mux=3))
    def table8(self,rfout):
        rdt={}
        if rfout>3750e6 and rfout<=7500e6:
            rdt.update(dict(equivaldivval=2,seg1_en=1,includeddivide=4))
        elif rfout>1875e6 and rfout <=3750e6:
            rdt.update(dict(equivaldivval=4,seg1_en=0,includeddivide=4))
        elif rfout>1250e6 and rfout<=2500e6:
            rdt.update(dict(equivaldivval=6,seg1_en=0,includeddivide=6))
        elif rfout>937.5e6 and rfout<=1437.5e6:
            rdt.update(dict(equivaldivval=8,seg1_en=0,includeddivide=4))
        elif rfout>625e6 and rfout<=958.333e6:
            rdt.update(dict(equivaldivval=12,seg1_en=0,includeddivide=6))
        elif rfout>468.75e6 and rfout<=718.75e6:
            rdt.update(dict(equivaldivval=16,seg1_en=0,includeddivide=4))
        elif rfout>312.5e6 and rfout<=479.167e6:
            rdt.update(dict(equivaldivval=24,seg1_en=0,includeddivide=4))
        elif rfout>234.375e6 and rfout<=359.375e6:
            rdt.update(dict(equivaldivval=32,seg1_en=0,includeddivide=4))
        elif rfout>156.25e6 and rfout<=239.583e6:
            rdt.update(dict(equivaldivval=48,seg1_en=0,includeddivide=6))
        elif rfout>117.1875e6 and rfout<=179.6875e6:
            rdt.update(dict(equivaldivval=64,seg1_en=0,includeddivide=4))
        elif rfout>104.167e6 and rfout<=159.722e6:
            rdt.update(dict(equivaldivval=72,seg1_en=0,includeddivide=6))
        elif rfout>78.125e6 and rfout<=119.792e6:
            rdt.update(dict(equivaldivval=96,seg1_en=0,includeddivide=6))
        elif rfout>58.594e6 and rfout<=89.844e6:
            rdt.update(dict(equivaldivval=128,seg1_en=0,includeddivide=4))
        elif rfout>39.0625e6 and rfout<=59.896e6:
            rdt.update(dict(equivaldivval=192,seg1_en=0,includeddivide=4))
        elif rfout>29.297e6 and rfout<=44.922e6:
            rdt.update(dict(equivaldivval=256,seg1_en=0,includeddivide=4))
        elif rfout>19.531e6 and rfout<=29.948e6:
            rdt.update(dict(equivaldivval=384,seg1_en=0,includeddivide=6))
        elif rfout>14.648e6 and rfout<=22.461e6:
            rdt.update(dict(equivaldivval=512,seg1_en=0,includeddivide=4))
        elif rfout>9.766e6 and rfout<=14.974e6:
            rdt.update(dict(equivaldivval=768,seg1_en=0,includeddivide=6))
        else:
            print('table8 rfout outside range')
        divdict={2:0,4:1,6:2,8:3,12:4,16:5,24:6,32:7,48:8,64:9,72:10,96:11,128:12,192:13,256:14,384:15,512:16,768:17}
        rdt.update(fvco=rfout*rdt['equivaldivval'],chdiv=divdict[rdt['equivaldivval']])
        return rdt
    def varfromreg(self,reg):
        vardict={}
        for k,l in self.paramap.items():
            vark=0
            for d in l:
                if d['paramsb'] is None and d['paralsb'] is None:
                    vark=(reg[d['regaddr']]&((1<<(d['regmsb']+1))-1))>>d['reglsb']
                elif d['paramsb'] is not None and d['paralsb'] is not  None:
                    vark=vark&(((1<<(d['paramsb']-d['paralsb']+1))-1)<<d['paralsb'])
                    vark|=((reg[d['regaddr']]>>d['reglsb'])&((1<<(d['regmsb']-d['reglsb']+1))-1))<<d['paralsb']
            if vark!=self.paramap[k][0]['default']:
                vardict[k]=vark
        return vardict
#    def manualupdate(self,dictin):
#        self.vardict.update(dictin)



if __name__=="__main__":
    c1=lmx2595()
    #c1.freqcalc(oscin=100e6,rfouta=6.52e9,)
    f1=c1.freqcalc(oscin=100e6,rfouta=2256e6,rfoutb=20.142857e6)
    manualupdate=dict(fcal_en=1)
#,muxout_ld_sel=1
#,out_force=1
#,vco_daciset_strt=300
#,vco_sel=4
#,outb_mux=0
#,ld_dly=0
#,sysref_div=1
#,vco_capctrl_strt=1
#,ramp_triga=1
#,ramp_trigb=1
#,ramp0_next_trig=1
#,ramp_manual=1
#,ramp1_next_trig=1
#,ramp_scale_count=0
#,rb_vco_capctrl=0
#,rb_vco_daciset=0
#,ramp0_next=1
    #c1.manualupdate(manualupdate)
    regf=numpy.array(c1.regmapgen(**manualupdate))
    print([hex(i) for i in regf])
    print(c1.vardict)
#    c1.outamux(6.2342345e9)
#    print(c1.fpd(100e6)[0])
#    print(c1.vardict)
#    print([(n,hex(i)) for n,i in enumerate(c1.regmapgen(ramp_en=0))])
#    print(c1.regdmf(oscin=100e6,rfouta=6520e6))
#    print(c1.regdmf(oscin=100e6,rfouta=4520e6))
#    print(c1.regdmf(oscin=100e6,rfouta=12520e6))
#    print(c1.regdmf(oscin=100e6,rfouta=15220e6))
#    reg6520=numpy.array([0, 0, 0, 0, 0, 0, 0, 33, 0, 0, 0, 17, 0, 0, 0, 2184, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 12, 2048, 0, 63, 1, 129, 50000, 0, 1000, 0, 500, 0, 5000, 0, 802, 168, 0, 1, 32769, 32, 0, 0, 0, 0, 2080, 128, 0, 16768, 768, 768, 2044, 49375, 8099, 200, 0, 0, 0, 1000, 0, 1028, 65, 4, 0, 7713, 915, 17388, 12684, 12684, 1160, 2, 3504, 3115, 1818, 124, 1, 1025, 57416, 10167, 100, 300, 128, 1615, 7792, 16384, 20481, 24, 4312, 5636, 8192, 16562, 51202, 200, 2627, 1602, 1280, 2056, 9500])#} //LMX2595_6520M.txt
#    print('a','reg6520','calcval')
#    for i,v in enumerate(reg6520):
#        if v!=regf[i]:
#            print(112-i,v,regf[i])
#
#    if 1:
#        reg6520=numpy.flip(reg6520)
#       #print(reg6520[10])
#
#        for k,v in c1.varfromreg(reg=reg6520).items():
#            print(k,v)


