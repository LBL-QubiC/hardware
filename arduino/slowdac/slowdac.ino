const int SSPin = 17;
const int SCKPin = 13;
const int MOSIPin = 11;
const int MISOPin = 12;
const int SSA0 = 4;
const int SSA1 = 15;
const int SSA2 = 16;
char VERSION[] = "Multi LTC2704 Programmer V1.1";

struct words {
  uint32_t zero : 4;
  uint32_t ver  : 1;
  uint32_t chip : 3;
  uint32_t ctrl : 4;
  uint32_t addr : 4;
  uint32_t data : 16;
};

words regval;
words val2serial;

void setup() {
  Serial.begin(9600);
  pinMode(SSPin, OUTPUT);
  pinMode(SCKPin, OUTPUT);
  pinMode(MOSIPin, OUTPUT);
  pinMode(MISOPin, INPUT);
  pinMode(SSA0, OUTPUT);
  pinMode(SSA1, OUTPUT);
  pinMode(SSA2, OUTPUT);
  digitalWrite(SSPin, HIGH);
  digitalWrite(SSA0, LOW);
  digitalWrite(SSA1, LOW);
  digitalWrite(SSA2, LOW);
  delay(500);
}


void loop() {
  static enum {S_IDLE, S_SERIALREAD, S_WRONGCODE, S_VERSION, S_SPIWRITE, S_SPIREAD, S_SERIALWRITE} state = S_IDLE;
  int incoming = 0;
  byte fromserial [4];
  byte toserial [4];
  switch (state) {
    case S_IDLE:
      delay(500);
      if (Serial.available() > 0) {
        state=S_SERIALREAD;
      }
      else {
        state = S_IDLE;
      }
      break;
    case S_SERIALREAD: {
      int bytesavail = Serial.available();
      if (bytesavail < 4) {
        for (int k = 0; k < bytesavail; k++) {
          Serial.read();
        }
        state = S_IDLE;
      }
      else {
        for (int k = 0; k < bytesavail-4; k++) {
          Serial.read();
        }
        int i = 0;
        while (i < 4) {
          incoming = Serial.read();
          if (incoming != -1) {
            fromserial[i] = incoming;
            i++;
          }
        }
        regval=bytes2words(fromserial);
        if ((regval.zero == 0b0000) && (regval.ver == 0b0) && (((regval.ctrl >= 0b0010) && (regval.ctrl <= 0b1001)) || ((regval.ctrl >= 0b1110) && (regval.ctrl <= 0b1111)))) {
          state = S_SPIWRITE;
        }
        else if ((regval.zero == 0b0000) && (regval.ver == 0b0) && (regval.ctrl >= 0b1010) && (regval.ctrl <= 0b1101)) {
          state = S_SPIREAD;
        }
        else if ((regval.zero == 0b0000) && (regval.ver == 0b1)) {
          state = S_VERSION;
        }
        else {
          state = S_WRONGCODE;
        }
      }
                       }
      break;
    case S_WRONGCODE: 
      Serial.println("Wrong Code");
      state = S_IDLE;
      break;
    case S_VERSION:
      Serial.println(VERSION);
      state = S_IDLE;
      break;
    case S_SPIWRITE:
      val2serial = spireadwrite2704(regval, false);
      state = S_SERIALWRITE;
      break;
    case S_SPIREAD:
      val2serial = spireadwrite2704(regval, true);
      state = S_SERIALWRITE;
      break;
    case S_SERIALWRITE:
      words2bytes(val2serial, toserial);
      for (int i = 0; i < 4; i++) {
        Serial.write(toserial[i]);
        delay(10);
      }
      state = S_IDLE;
      break;
    default: state = S_IDLE;
    break;
  }
}

words bytes2words(byte fromserial[]) {
  regval.data = (uint16_t)fromserial[3];
  regval.data += (uint16_t)fromserial[2] << 8;
  regval.addr = fromserial[1] & 0x0f;
  regval.ctrl = (fromserial[1] & 0xf0) >> 4;
  regval.chip = fromserial[0] & 0x07;
  regval.ver = (fromserial[0] & 0x08) >> 3;
  regval.zero = (fromserial[0] & 0xf0) >> 4;
  return regval;
}

void words2bytes(words regval, byte toserial[]) {
  toserial[0] = ((regval.zero << 4) + (regval.ver << 3) + regval.chip) & 0xff;
  toserial[1] = ((regval.ctrl << 4) + regval.addr) & 0xff;
  toserial[2] = (regval.data >> 8) & 0xff;
  toserial[3] = (regval.data) & 0xff;
}

void chipsel(int chip) {
  digitalWrite(SSA0, bitRead(chip, 0));
  digitalWrite(SSA1, bitRead(chip, 1));
  digitalWrite(SSA2, bitRead(chip, 2));
  delay(200);
}

words spireadwrite2704(words regval, bool readflag) {
  words wordsval;
  chipsel(regval.chip);
  wordsval=readwriteone(regval, readflag);
  return wordsval;
}

words readwriteone(words regval, bool readflag) {
  words wordsval;
  wordsval = regval;
  int regctrl;
  int regaddr;
  uint16_t regdata;
  uint16_t readdata;
  int bitval;
  digitalWrite(SSPin, LOW);
  regctrl = regval.ctrl;
  regaddr = regval.addr;
  regdata = regval.data;
  for (int ctrlbit = 3; ctrlbit >= 0; ctrlbit--) {
    spi1bitwrite(SCKPin, MOSIPin, bitRead(regctrl, ctrlbit));
  }
  for (int addrbit = 3; addrbit >= 0; addrbit--) {
    spi1bitwrite(SCKPin, MOSIPin, bitRead(regaddr, addrbit));
  }
  if (readflag) {
    for (int databit = 15; databit >= 0; databit--) {
      bitval=spi1bitread(SCKPin, MISOPin);
      bitWrite(readdata,databit,bitval);
    }
    wordsval.data = readdata;
  }
  else {
    for (int databit = 15; databit >= 0; databit--) {
      spi1bitwrite(SCKPin, MOSIPin, bitRead(regdata, databit));
    }
  }
  digitalWrite(SSPin, HIGH);
  return wordsval;
}

void spi1bitwrite(int sckpin, int mosipin, bool bitval) {
  digitalWrite(mosipin, bitval ? HIGH : LOW);
  digitalWrite(sckpin, HIGH);
  digitalWrite(mosipin, bitval ? HIGH : LOW);
  digitalWrite(sckpin, LOW);
}

int spi1bitread(int sckpin, int misopin) {
  int bitval;
  bitval=digitalRead(misopin);
  digitalWrite(sckpin, HIGH);
  bitval=digitalRead(misopin);
  digitalWrite(sckpin, LOW);
  return bitval;
}
