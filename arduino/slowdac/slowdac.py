import json
import serial
import devcom
import time
import struct

class slowdac():
    def __init__(self,slowdaccfg='slowdaccfg.json',**kwargs):
        self.addrcfg=dict(
                A=0b0000,  # DAC A
                B=0b0010,  # DAC B
                C=0b0100,  # DAC C
                D=0b0110,  # DAC D
                All=0b1111 # All DACs
                )
        with open(slowdaccfg) as jfile:
            self.chancfg=json.load(jfile)
        '''
        Span Codes:
        https://www.analog.com/media/en/technical-documentation/data-sheets/2704fd.pdf
        In LTC2704 datasheet, Table 3 shows the span codes for 5V reference and ±15V analog supply.
        Here we use 2.048V reference and ±5V analog supply. 
        Therefore the span voltage should be divided by ~2.5 for calculation.
        0b0000: Unipolar 0V to 2V
        0b0001: Unipolar 0V to 4V
        0b0010: Bipolar -2V to 2V
        0b0011: Bipolar -4V to 4V
        0b0100: Bipolar -1V to 1V
        0b0101: Bipolar -1V to 3V
        '''

    def words32(self,ver,chip,ctrl,addr,data):
        words=((ver&0x1)<<27)+((chip&0x7)<<24)+((ctrl&0xf)<<20)+((addr&0xf)<<16)+(data&0xffff)
        return words

    def write(self,ser,device,chan,data,span=None,ctrl=0b0111):
        retwords=self.readwrite(ser=ser,device=device,chip=self.chancfg[chan]['chip'],ctrl=ctrl,addr=self.chancfg[chan]['addr'],data=data,span=span,ver=False)
        return retwords

    def read(self,ser,device,chan,ctrl=0b1101):
        retwords=self.readwrite(ser=ser,device=device,chip=self.chancfg[chan]['chip'],ctrl=ctrl,addr=self.chancfg[chan]['addr'],data=0,span=None,ver=False)
        return retwords

    def ver(self,ser,device):
        retwords=self.readwrite(ser=ser,device=device,chip=None,ctrl=None,addr=None,data=None,span=None,ver=True)
        return retwords

    def readwrite(self,ser,device,chip,ctrl,addr,data,span=None,ver=False):
        #print('Device',device)
        ser.flushOutput()
        ser.flushInput()
        time.sleep(0.1)
        if ver:
            words=self.words32(ver=ver,chip=0,ctrl=0,addr=0,data=0)
        else:
            addr=self.addrcfg[addr]
            data=span if span is not None else data
            words=self.words32(ver=ver,chip=chip,ctrl=ctrl,addr=addr,data=data)
        #print('Words',words)
        words2ser=int(words).to_bytes(4,'big')
        print('Words to serial: ',words2ser)
        ser.write(words2ser)
        if ver:
            retwords=ser.readline()
            print('Version: ',retwords.decode('utf-8'))
        else:
            for i in range(4):
                time.sleep(0.1)
                retwords=self.printhexbytes(ser.read(),bytesperword=1)
                print('Return words: ',retwords)
        return retwords

    def main(self,chan,data,ctrl,span=None,deviceid='slowdac_01',sndev=devcom.sndev):
        dev=devcom.devcom(sndev)
        print('devcom',dev)
        retwords=self.slowdacwrite(chan=chan,data=data,span=span,ctrl=ctrl,serialport=dev[deviceid],device=deviceid)
        #retwords=self.slowdacread(chan=chan,ctrl=ctrl,serialport=dev[deviceid],device=deviceid)
        #retwords=self.slowdacver(serialport=dev[deviceid],device=deviceid)
        return retwords

    def slowdacwrite(self,chan,data,span,ctrl,serialport,device):
        ser=serial.Serial(serialport,9600)
        retwords=self.write(chan=chan,data=data,span=span,ctrl=ctrl,ser=ser,device=device)
        return retwords

    def slowdacread(self,chan,ctrl,serialport,device):
        ser=serial.Serial(serialport,9600)
        retwords=self.read(chan=chan,ctrl=ctrl,ser=ser,device=device)
        return retwords

    def slowdacver(self,serialport,device):
        ser=serial.Serial(serialport,9600)
        retwords=self.ver(ser=ser,device=device)
        return retwords

    def hexbytestolong(self,b,bytesperword=3):
        dl=[]
        #print('b in hexbytestolong',b)
        if len(b)%bytesperword==0 and bytesperword<=4:
            for index in range(0,len(b),bytesperword):
                longbytes=(4-bytesperword)*b'\x00'+b[index:index+bytesperword]
                dl.append(struct.unpack('>L',longbytes)[0])
        else:
            print('length error')
            dl=[i for i in struct.unpack('>%dB'%(len(b)),b)]
        return dl

    def printhexbytes(self,b,bytesperword=1):
        d=self.hexbytestolong(b,bytesperword=bytesperword)
        return ([hex(i) for i in d])


if __name__=="__main__":
    slowdac=slowdac()
    dev=devcom.devcom(devcom.sndev)
    print('devcom',dev)
    deviceid='slowdac_03'
    slowdac.slowdacver(serialport=dev[deviceid],device=deviceid) # version
    slowdac.slowdacwrite(chan='0',data=None,span=0b0010,ctrl=0b0110,serialport=dev[deviceid],device=deviceid) # write span
    slowdac.slowdacwrite(chan='0',data=0xbeaf,span=None,ctrl=0b0111,serialport=dev[deviceid],device=deviceid) # write data
    slowdac.slowdacread(chan='0',ctrl=0b1100,serialport=dev[deviceid],device=deviceid) # read span
    slowdac.slowdacread(chan='0',ctrl=0b1101,serialport=dev[deviceid],device=deviceid) # read data
    '''
    if 1:
        slowdac.main(chan='23',data=None,span=0b0010,ctrl=0b0110,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='23',data=0x1120,span=None,ctrl=0b0111,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='18',data=None,span=0b0010,ctrl=0b0110,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='18',data=0xca15,span=None,ctrl=0b0111,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='13',data=None,span=0b0010,ctrl=0b0110,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='13',data=0xacc9,span=None,ctrl=0b0111,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='8',data=None,span=0b0010,ctrl=0b0110,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='8',data=0xa3b5,span=None,ctrl=0b0111,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='1',data=None,span=0b0010,ctrl=0b0110,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='1',data=0xc7d9,span=None,ctrl=0b0111,deviceid='slowdac_03',sndev=devcom.sndev)
    if 0:
        slowdac.main(chan='0',data=0x0000,span=None,ctrl=0b1101,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='6',data=0x0000,span=None,ctrl=0b1101,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='23',data=0x0000,span=None,ctrl=0b1101,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='18',data=0x0000,span=None,ctrl=0b1101,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='13',data=0x0000,span=None,ctrl=0b1101,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='8',data=0x0000,span=None,ctrl=0b1101,deviceid='slowdac_03',sndev=devcom.sndev)
        slowdac.main(chan='1',data=0x0000,span=None,ctrl=0b1101,deviceid='slowdac_03',sndev=devcom.sndev)
   '''
