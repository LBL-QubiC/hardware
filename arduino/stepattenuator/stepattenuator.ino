const int V0_5=A4;
const int V1=A3;
const int V2=A5;
const int V4=A0;
const int V8=A2;
const int V16=A1;

int val=0;
void setup() {
  // put your setup code here, to run once:
pinMode(V16, OUTPUT);  
pinMode(V8, OUTPUT);  
pinMode(V4, OUTPUT);  
pinMode(V2, OUTPUT);  
pinMode(V1, OUTPUT);  
pinMode(V0_5, OUTPUT);  
digitalWrite(V0_5,0);
digitalWrite(V1,0);
digitalWrite(V2,0);
digitalWrite(V4,0);
digitalWrite(V8,0);
digitalWrite(V16,0);
digitalWrite(A6,0);
pinMode(LED_BUILTIN, OUTPUT);
Serial.begin(9600);
}

void loop() {
// put your main code here, to run repeatedly:
  int inByte;
  if (Serial.available()>0){
    inByte=Serial.read();
    switch (inByte){
      //case '\n':      
      case 192 ... 255:{
        val=inByte;
        digitalWrite(V0_5,bitRead(val,0));
        digitalWrite(V1,bitRead(val,1));
        digitalWrite(V2,bitRead(val,2));
        digitalWrite(V4,bitRead(val,3));
        digitalWrite(V8,bitRead(val,4));
        digitalWrite(V16,bitRead(val,5));
        Serial.println(val,HEX);        
        break;              
      }
      case 'v':
        Serial.println(val);
        break;
      case 'i':
        Serial.println("Step attenuator\n");
        break;
      default:
        Serial.println(inByte);
        break;
      }
  }
}
