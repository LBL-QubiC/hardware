const int bit0=3;
const int bit1=2;
const int bit2=4;
int val=0;
void setup() {
  // put your setup code here, to run once:
pinMode(bit0, OUTPUT);  
pinMode(bit1, OUTPUT);  
pinMode(bit2, OUTPUT);  
Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int inByte;
  if (Serial.available()>0){
    inByte=Serial.read();
    switch (inByte){
      //case '\n':      
      case '0' ... '7':{
        val=inByte-'0';
        digitalWrite(bit0,bitRead(val,0));
        digitalWrite(bit1,bitRead(val,1));
        digitalWrite(bit2,bitRead(val,2));
        Serial.println(val);        
        break;              
      }
      default:
        Serial.println(inByte);
        break;
      }
  }
}
