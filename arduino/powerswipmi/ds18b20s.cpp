#include "ds18b20s.h"
c_ds18b20s::c_ds18b20s() {
  Serial.println("init ds18b20 with no parameter");

}
c_ds18b20s::c_ds18b20s(uint8_t wire ) {
  _debug = false;
  init(wire);
}
c_ds18b20s::c_ds18b20s(uint8_t wire, bool debug) {
  _debug = debug;
  init(wire);
}
void c_ds18b20s::init(uint8_t wire) {
  if (_debug) {
    Serial.print("init ds18b20 with parameter ");
    Serial.println(wire);
  }
  onewire = new OneWire(wire);
  ds18b20s = new DallasTemperature(onewire);
  ds18b20s->begin();
  devicecount = ds18b20s->getDeviceCount();

  _temperature = new float[devicecount];

}
void c_ds18b20s::reuestTemperatures(int8_t chan) {
  if (_debug) {
    Serial.print("devicecount");
    Serial.println(devicecount);
  }
  if (chan < 0) {
    ds18b20s->requestTemperatures();
  }
  else {
    ds18b20s->requestTemperaturesByIndex(chan);
  }
}
void c_ds18b20s::getTemperatures(uint8_t chan) {
    _temperature[chan] = ds18b20s->getTempCByIndex(chan);
}
float c_ds18b20s::temperature(uint8_t chan) {
  float ret = -1;
  if (chan < devicecount) {
    ret = _temperature[chan];
  }
  return ret;
}
