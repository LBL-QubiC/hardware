#include <stdint.h>
#include "ntp.h"
void c_ntp::init(char * rxbuf, char *txbuf, unsigned int maxbufsize) {
  _rxbuf = rxbuf;
  _txbuf = txbuf;
  _maxbufsize = maxbufsize;

  _udp = new EthernetUDP();
  _udp->begin(NTPLOCALPORT);

}
c_ntp::c_ntp(char * rxbuf, char *txbuf, unsigned int maxbufsize) {

  _debug = false;
  init(rxbuf, txbuf, maxbufsize);
}
c_ntp::c_ntp(char * rxbuf, char *txbuf, unsigned int maxbufsize, bool debug) {
  _debug = debug;
  init(rxbuf, txbuf, maxbufsize);
}
void c_ntp::set_timeserver(char *timeserver) {
  //  _timeserver = String(timeserver);
}
void c_ntp::request() {
  // set all bytes in the buffer to 0
  memset(_txbuf, 0, NTP_PACKET_SIZE);
  _txbuf[0] = 0b11100011;   // LI, Version, Mode
  _txbuf[1] = 0;     // Stratum, or type of clock
  _txbuf[2] = 6;     // Polling Interval
  _txbuf[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  _txbuf[12]  = 49;
  _txbuf[13]  = 0x4E;
  _txbuf[14]  = 49;
  _txbuf[15]  = 52;


  time64 tm = abstime();
  _txbuf[40] = tm.bytes[7];//(tm.tsec >> 24) & 0xff;
  _txbuf[41] = tm.bytes[6];//(tm.tsec >> 16) & 0xff;
  _txbuf[42] = tm.bytes[5];//(tm.tsec >> 8) & 0xff;
  _txbuf[43] = tm.bytes[4];//(tm.tsec) & 0xff;
  _txbuf[44] = tm.bytes[3];//(tm.nfsec >> 24) & 0xff;
  _txbuf[45] = tm.bytes[2];//(tm.nfsec >> 16) & 0xff;
  _txbuf[46] = tm.bytes[1];//(tm.nfsec >> 8) & 0xff;
  _txbuf[47] = tm.bytes[0];//(tm.nfsec) & 0xff;

  _udp->beginPacket(_timeserver, NTPPORT); // NTP requests are to port 123
  _udp->write(_txbuf, NTP_PACKET_SIZE);
  _udp->endPacket();

}

int c_ntp::parsePacket() {
  int ret = 0;
  unsigned long rxtime;
  if (ret = _udp->parsePacket()) {
    time64 localrxtime = abstime();//(millistotime64(rxtime)).t64+_toffset.t64;
    _udp->read(_rxbuf, NTP_PACKET_SIZE);

    time64 reference_timestamp = byte8totime(_rxbuf, 16);
    time64 originate_timestamp = byte8totime(_rxbuf, 24);
    time64 receive_timestamp = byte8totime(_rxbuf, 32);
    time64 transmit_timestamp = byte8totime(_rxbuf, 40);
//    
//    Serial.print("reference time:");
//    Serial.print(reference_timestamp.tl[1], HEX); Serial.print(".");
//    Serial.println(reference_timestamp.tl[0], HEX);
//    Serial.print("originate time:");
//    Serial.print(originate_timestamp.tl[1], HEX); Serial.print(".");
//    Serial.println(originate_timestamp.tl[0], HEX);
//    Serial.print("receive   time:");
//    Serial.print(receive_timestamp.tl[1], HEX); Serial.print(".");
//    Serial.println(receive_timestamp.tl[0], HEX);
//    Serial.print("transmit  time:");
//    Serial.print(transmit_timestamp.tl[1], HEX); Serial.print(".");
//    Serial.println(transmit_timestamp.tl[0], HEX);
//    Serial.print("localrx   time:");
//    Serial.print(localrxtime.tl[1], HEX); Serial.print(".");
//    Serial.println(localrxtime.tl[0], HEX);
//    Serial.print("offset    time:");
//    Serial.print(_toffset.tl[1], HEX); Serial.print(".");
//    Serial.println(_toffset.tl[0], HEX);


    unsigned long long server_diff = transmit_timestamp.t64 - receive_timestamp.t64;
    unsigned long long local_diff = localrxtime.t64 - originate_timestamp.t64;
    unsigned long long tx_diff = receive_timestamp.t64 - originate_timestamp.t64;
    unsigned long long rx_diff = localrxtime.t64 - transmit_timestamp.t64;
    if (_toffset.tl[1] == 0L) {
      _toffset.t64 = transmit_timestamp.t64 - originate_timestamp.t64;
    }
    else {
      unsigned long long cabledelay = (local_diff - server_diff) >> 1;
      unsigned long long corr = tx_diff - cabledelay;
      _toffset.t64 = _toffset.t64 + corr;

    }



  }
}

time64 c_ntp::byte8totime(byte bytes[], int startaddr) {
  time64 t;
  for (int i = 0; i < 8; i++) {
    t.bytes[i] = bytes[startaddr + 7 - i];
  }
  return t;
}

time64 c_ntp::millistotime64(unsigned long ms) {
  double tms = ms / 1000.0;
  unsigned long tmsi = tms / 1;
  unsigned long tmsf = (tms - tmsi) * 0x100000000L;
  time64 out;
  out.tl[1] = tmsi;
  out.tl[0] = tmsf;
  return out;
}
#define SEVENTYYEARS  2208988800UL
unsigned long c_ntp::unixtimestamp(time64 t64) {
  return unixtimestamp(t64.tl[1]);
}
unsigned long c_ntp::unixtimestamp(unsigned long tsec) {
  return tsec-SEVENTYYEARS;  
}

time64 c_ntp::abstime() {
  unsigned long timenow = millis();
  if (timenow < _lasttime) {
    _toffset.t64 += 0x4189374bc6a7f0L; //hex(int(0x100000000/1000.0*0x100000000))
  }
  _lasttime = timenow;
  time64 tm;
  tm.t64 = (millistotime64(timenow)).t64 + _toffset.t64;
  return tm;
}
