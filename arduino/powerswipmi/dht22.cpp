#include "dht22.h"
c_dht22::c_dht22(uint8_t pin) {
  _debug = false;
  init(pin);
}
c_dht22::c_dht22(uint8_t pin, bool debug) {
  _debug = debug;
  init(pin);
}
void c_dht22::init(uint8_t pin){
  dht=new DHT(pin,DHTTYPE);
  dht->begin();
}
void c_dht22::readhardware(){
  _temperature=dht->readTemperature();
  _humidity=dht->readHumidity();
}
float c_dht22::get_temperature(){
  return _temperature;
}
float c_dht22::get_humidity(){
  return _humidity;
}
