
class c_ipmi {
  private:
  
    uint8_t _rssa;
    uint8_t _netfn_rslun;
    uint8_t _rchecksum;
    uint8_t _rqsa;
    uint8_t _rqseq_rqlun;
    uint8_t _cmd;
    unsigned char * _databuf;
    uint8_t _qchecksum;


//    uint8_t _authtype;
    uint8_t _sessionsequencenumber[4];
//    uint8_t _sessionid[4];

    uint8_t _payloadlength;
    

    uint8_t _channel;       // set by set_channel
    uint8_t _authtype=0x10; // straight password key
    uint8_t _anonymous=0x20;
    uint8_t _supportversion=0x00; // 
    uint8_t _username[16]={0x41,0x44,0x4d,0x49,0x4e,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    uint8_t _authcode[16]={0x41,0x44,0x4d,0x49,0x4e,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};    
    bool _authgood=true;
    uint8_t _sessionid[4];
    uint8_t _challengestring[16];
    uint8_t _maxprivilege;
    uint32_t _outboundseq;
    uint32_t _inboundseq;

    unsigned int seqno=0;

  uint8_t _deviceid=0xab; 
  uint8_t _devicerevision=0x10;
  uint8_t _firmwarerevision1=0x01;
  uint8_t _firmwarerevision2=0x01;
  uint8_t _ipmiversion=0x02;
  uint8_t _additionaldevicesupport=0xbf; // too much
  uint8_t _manufacturerid[3]={0x09,0x08,0x06};
  uint8_t _productid[2]={0x01,0x02};

  uint8_t _acpisystempowerstate;
  uint8_t _acpidevicepowerstate;
  uint8_t _poweraction;
    
    
  protected:
  public:
    c_ipmi();
    int set_channel(unsigned int);
    int c_ipmi::set_acpi_device_power_state(unsigned int acpidevicepowerstate);
    int c_ipmi::set_acpi_system_power_state(unsigned int acpisystempowerstate);
    int c_ipmi::get_poweraction();
    void c_ipmi::set_powernoaction();
    int process15wrap(unsigned char *, unsigned char*);
    int process(unsigned char *, unsigned int, unsigned char*);
    int c_ipmi::cmd38(unsigned char * rxbuf,unsigned char * txbuf);
    int c_ipmi::cmd39(unsigned char * rxbuf,unsigned char * txbuf);
    int c_ipmi::cmd3a(unsigned char * rxbuf,unsigned char * txbuf);
    int c_ipmi::cmd3b(unsigned char * rxbuf,unsigned char * txbuf);
    int c_ipmi::cmd01(unsigned char * rxbuf,unsigned char * txbuf);
    int c_ipmi::cmd02(unsigned char * rxbuf,unsigned char * txbuf);
    int c_ipmi::cmd07(unsigned char * rxbuf,unsigned char * txbuf);
    uint8_t c_ipmi::checksum(unsigned char * txbuf, unsigned int startaddr, unsigned stopaddr);
};
