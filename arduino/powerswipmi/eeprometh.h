#include <EEPROM.h>
#include <Ethernet.h>

class c_eeprometh {
  private:
  bool _debug;
    unsigned char mac[6] = {0xDd, 0xdD, 0xdd, 0xdd, 0xdd, 0xdd};
    unsigned char localip[4] = {10, 0, 0, 1};
    void init(uint8_t ethcspin);
    bool ethstatus;
  public:
  c_eeprometh(uint8_t ethcspin);
  c_eeprometh(uint8_t ethcspin,bool debug);
  bool getstatus(){return ethstatus;};


};
