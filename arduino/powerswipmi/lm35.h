#include <Arduino.h>

class c_lm35 {
  private:
    bool _debug;
    void init(uint8_t pin);
    float temperatureC;
  uint8_t _pin;
  protected:
  public:
    c_lm35(uint8_t);
    c_lm35(uint8_t, bool debug);
    void readtemperature();
    float get_temperatureC();

};
