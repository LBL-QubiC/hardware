#include "eeprometh.h"
c_eeprometh::c_eeprometh(uint8_t ethcspin){
  _debug=false;
  init(ethcspin);
}
c_eeprometh::c_eeprometh(uint8_t ethcspin,bool debug){
    _debug=debug;
  init(ethcspin);
;
}

void c_eeprometh::init(uint8_t ethcspin) {
  for (int i = 0; i < 6; i++) {
    mac[i] = EEPROM.read(i);
  }
  for (int i = 6; i < 10; i++) {
    localip[i] = EEPROM.read(i);
  }
  Ethernet.init(ethcspin);
  ethstatus=Ethernet.begin(mac);
  if (ethstatus==0) {
    Serial.println(F("Failed to initialize Ethernet library"));    
  }
  else{
    Serial.println(Ethernet.localIP());
  }
}
// ntp, http, mqtt, nmcpipmi,
