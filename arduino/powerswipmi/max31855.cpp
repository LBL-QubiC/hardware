#include "max31855.h"
c_max31855::c_max31855(uint8_t cspin) {
  _debug = false;
  init(cspin);
}
c_max31855::c_max31855(uint8_t cspin, bool debug) {
  _debug = debug;
  init(cspin);
}
void c_max31855::init(uint8_t cspin) {
  max31855= new Adafruit_MAX31855(cspin);
  max31855->begin();
}
void c_max31855::readmax31855() {
  _internal_temperature = max31855->readInternal();
  _temperature = max31855->readCelsius();
//  if (isnan(_temperature)) {
//    _temperature = -999;
//  }

}

float c_max31855::get_internal_temperature() {
  return _internal_temperature;

}
float c_max31855::get_temperature() {
  return _temperature;
}
