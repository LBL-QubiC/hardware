#define NTPLOCALPORT 8888
#define NTPPORT 123
#define NTP_PACKET_SIZE 48
#include <EthernetUdp.h>

//
//typedef struct time64 {
//  uint32_t tsec; // time in second
//  uint32_t nfsec; // int for fraction of a second
//  double fsec;   // fraction of a second
//};
#ifndef NTP
#define NTP
union time64 {
  unsigned long long t64;
  byte bytes[8];
  unsigned long tl[2];
};

class c_ntp {
  private:
    EthernetUDP* _udp;
    time64 _toffset;
    const char * _timeserver="time.nist.gov";    
    bool _debug;
    char * _rxbuf;
    char * _txbuf;
    unsigned int _maxbufsize;
    unsigned int _rxlen;
    unsigned int _txlen;
    void init(char * rxbuf, char *txbuf, unsigned int maxbufsize);
    unsigned long _lasttime;
  protected:
  public:
    c_ntp(char * rxbuf, char *txbuf, unsigned int maxbufsize);
    c_ntp(char * rxbuf, char *txbuf, unsigned int maxbufsize, bool debug);
    c_ntp(EthernetUDP* udp);

    void request();
    int parsePacket() ;
    time64 byte8totime(unsigned char bytes[], int startaddr) ;
    time64 millistotime64(unsigned long ms) ;
//    void time64tobyte8(time64 t64, char * bytes) ;
//    void printsecsSince1900(unsigned long secsSince1900) ;
//    double t64diff(time64 t1, time64 t2) ;
//    void hexprint(char *, int addr0);
    void set_timeserver(char *timeserver);
    time64 abstime();
    unsigned long unixtimestamp(time64 t64);
    unsigned long unixtimestamp(unsigned long tsec) ;
};
#endif
