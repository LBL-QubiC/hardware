#include <Arduino.h>
#include <stdint.h>
#include <Wire.h>
#include "max6615.h"
c_max6615::c_max6615(uint8_t i2caddr) {
  _i2caddr = i2caddr;
}
void c_max6615::set_tempch2sourcelocal(bool tempch2sourcelocal) {
  _tempch2sourcelocal = tempch2sourcelocal;
  i2cwrite(0x2, reg02(false));
}

void c_max6615::softreset() {
  i2cwrite(2, reg02(true));
  delay(1000);
  i2cwrite(2, reg02(false));
};
void c_max6615::i2cinit(int clkfreq){
    Wire.begin();
  Wire.setClock(clkfreq);
}
void c_max6615::i2cwrite(uint8_t regaddr, uint8_t regvalue) {
  Wire.beginTransmission(_i2caddr); // transmit to device #112
  Wire.write(byte(regaddr));      // sets register pointer to echo #1 register (0x02)
  Wire.write(byte(regvalue));      // sets register pointer to echo #1 register (0x02)
  Wire.endTransmission();      // stop transmitting
//  Serial.print("write register ");
//  Serial.print(regaddr, HEX);
//  Serial.print(":");
//  Serial.println(regvalue, HEX);
}

void c_max6615::fanmanual(uint8_t chan, float ratiofullspeed) {
  uint8_t target = (uint8_t)(ratiofullspeed * 0xf0);
  if (chan == 0) {
    _pwm1[PWM_TARGET_DUTY_CYCLE] = target;
    _pwm1[DUTY_CYCLE_RATE_OF_CHANGE] = 0;
    _fan1control1 = 0;
    _fan1control2 = 0;
  }
  else if (chan == 1) {
    _pwm2[PWM_TARGET_DUTY_CYCLE] = target;
    _pwm2[DUTY_CYCLE_RATE_OF_CHANGE] = 0;
    _fan2control1 = 0;
    _fan2control2 = 0;
  }

  i2cwrite(0x11, reg11());
  if (chan == 0) {
    i2cwrite(0x07, reg07());
    i2cwrite(0x0b, reg0b());
  }
  else if (chan == 1) {
    i2cwrite(0x08, reg08());
    i2cwrite(0x0c, reg0c());
  }
}

uint8_t c_max6615::i2cread(uint8_t regaddr) {
  uint8_t reading = 0;
  Wire.beginTransmission(_i2caddr);
  Wire.write(regaddr);
  Wire.endTransmission();
  Wire.requestFrom(_i2caddr, (size_t)1);    // request 2 bytes from peripheral device #112
  if (1 <= Wire.available()) { // if two bytes were received
    reading = Wire.read();  // receive high byte (overwrites previous reading)
    //    Serial.print("reading addr  ");
    //    Serial.print(regaddr, HEX);
    //    Serial.print(":");
    //    Serial.print(reading, HEX);
    //    Serial.println("");
  }
  return reading;
}

void c_max6615::readtemperature(uint8_t chan) {
  //  float temp;
  uint8_t r_int;
  uint8_t r_frac;
  if (chan == 0) {
    r_int = i2cread(0);
    r_frac = i2cread(0x1e);
  }
  else if (chan == 1) {
    r_int = i2cread(1);
    r_frac = i2cread(0x1f);
  }
  _temperature[chan] = r_int + (float)r_int / 512.0;
}
float c_max6615::get_temperature(uint8_t chan) {
  return _temperature[chan];
}
void c_max6615::readotstatus(uint8_t chan) {
  uint8_t ot8 = i2cread(5);
  //  bool ot1bit;
  if (chan == 0) {
    _otstatus[0] = (ot8 >> 7) & 0x1;
  }
  else if (chan == 1) {
    _otstatus[1] = (ot8 >> 6) & 0x1;
  }
}
bool c_max6615::get_otstatus(uint8_t chan) {
  return _otstatus[chan];
}
void c_max6615::readinstantaneousdutycycle(uint8_t chan) {
  uint8_t dutycycle;
  if (chan == 0) {
    _dutycycle[0] = i2cread(0x0d);
  }
  else if (chan == 1) {
    _dutycycle[1] = i2cread(0x0e);
  }
}
uint8_t c_max6615::get_dutycycle(uint8_t chan) {
  return _dutycycle[chan];
}
void c_max6615::readtach(uint8_t chan) {
  //  uint8_t tach;
  if (chan == 0) {
    _tach[0] = i2cread(0x18);
  }
  else if (chan == 1) {
    _tach[1] = i2cread(0x19);
  }
//  Serial.print("read tach");
//  Serial.println(_tach[chan]);
}
uint8_t c_max6615::get_tach(uint8_t chan) {
  return _tach[chan];
}
void c_max6615::set_freqselect(uint8_t freqselect) {
  _freqselect = freqselect;
  i2cwrite(0x14, reg14());
};

uint8_t c_max6615::i2creadreg(uint8_t addr){
  return i2cread(addr);
}
