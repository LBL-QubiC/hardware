#include "httpsrv.h"
void c_httpsrv::init(char * rxbuf, char * txbuf,unsigned int maxbufsize){
  _rxbuf=rxbuf;
  _txbuf=txbuf;
  _maxbufsize=maxbufsize;
  http = new EthernetServer(HTTPPORT);
  http->begin();
}
c_httpsrv::c_httpsrv(char * rxbuf, char * txbuf,unsigned int maxbufsize){
  _debug=false;
  init(rxbuf,txbuf,maxbufsize);  
}
c_httpsrv::c_httpsrv(char * rxbuf, char * txbuf,unsigned int maxbufsize,bool debug){
  _debug=debug;
  init(rxbuf,txbuf,maxbufsize);  
}
EthernetClient c_httpsrv::available(){
  return http->available();
}

void c_httpsrv::response(EthernetClient client,String  strin){//,DynamicJsonDocument jsondoc){
  
    Serial.println(F("New client"));
    _rxlen=0;
    // Read the request (we ignore the content in this example)
    while (client.available()) {
      _rxbuf[_rxlen]=client.read();
      _rxlen++;      
    }
    String str=String((char*)_rxbuf);
//    Serial.print("client:");
//    Serial.println(_rxlen);
//    Serial.write(_rxbuf,_rxlen);
//    Serial.println();
//    if (str.indexOf("?buttonToggle") >0 ){
//      Serial.println("receive http request button toggle");
//    }
//    Serial.println("in httpsrv");
//    Serial.println(strin);

//    Serial.print(F("Sending: "));
//    serializeJson(jsondoc, Serial);
//    Serial.println();
//    jsonresponse(client,jsondoc);
//
//const char* html=R"=====(
//<!DOCTYPE html>
//<html>
//<body>
//<a href=\"/?buttonToggle\"\">Toggle LED</a>
//</body>
//</html>
//
//)=====";
//client.print(html);
//
//}
//void c_httpsrv::jsonresponse(EthernetClient client,DynamicJsonDocument jsondoc){
    // Write response headers
    client.println(F("HTTP/1.0 200 OK"));
    client.println(F("Content-Type: application/json"));
    client.println(F("Connection: close"));
    client.print(F("Content-Length: "));
//    client.println(measureJsonPretty(jsondoc));
    client.println(strin.length());
    client.println();
    client.println(strin);
    // Write JSON document
//    serializeJsonPretty(jsondoc, client);
    // Disconnect
    client.stop();
}
