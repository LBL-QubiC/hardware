#include <Ethernet.h>
#include "ipmi.h"
class c_asf{
  private:
  uint8_t _ianaenterprisenumber[4];
  uint8_t _messagetype;
  uint8_t _messagetag;
  uint8_t _reserved;
  uint8_t _datalength;
  
  protected:
  public:
  c_asf();
  int process(unsigned char *,unsigned char*);
  int pingpong(unsigned char *,unsigned char *);

};



class c_rmcp {
private:
  const static int _port=623;
  uint8_t _version;
  uint8_t _reserved;
  uint8_t _sequencenumber;
  uint8_t _classofmessage;
  

protected:
  uint16_t _remaining;
public:
  c_asf asf;
  c_ipmi ipmi;
  c_rmcp();
  int port();
  int process(unsigned char* rxbuf, int rxlen, unsigned char* txbuf, int txlen);
  int ack(unsigned char *,unsigned char *);  
//  bufpl readrmcp();  
};
