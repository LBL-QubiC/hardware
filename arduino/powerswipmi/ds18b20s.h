
#include <OneWire.h>

#include <DallasTemperature.h>


class c_ds18b20s {
  private:
    bool _debug;
    DallasTemperature *ds18b20s;
    float *_temperature;
    uint8_t devicecount;
    void init(uint8_t wire);
    OneWire *onewire;
  protected:
  public:
    c_ds18b20s(uint8_t wire);
    c_ds18b20s(uint8_t wire, bool debug);
    c_ds18b20s();

    float temperature(uint8_t chan);  // from this object member
    void reuestTemperatures(int8_t chan); // -1 for all
    void getTemperatures(uint8_t chan);  // from hardware

};
