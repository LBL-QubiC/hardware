
typedef struct {
  uint8_t addr;
  uint8_t value;
} addrval;

#define TEMP_OT_LIMIT 0
#define OT_MASK 1
#define PWM_START_DUTY_CYCLE 2
#define PWM_MAX_DUTY_CYCLE 3
#define PWM_TARGET_DUTY_CYCLE 4
#define FAN_START_TEMP 5
#define DUTY_CYCLE_RATE_OF_CHANGE 6
#define DUTY_CYCLE_STEP_SIZE 7
#define THERMISTEROFFSET 8
#define TACHLIMIT 9

class c_max6615 {
  private:
    uint8_t _i2caddr;
    bool _standby = 0;

    bool _timeout = 0;
    bool _fan1pwminvert = 1;
    bool _fan2pwminvert = 1;
    bool _mindutycycle = 0;
    bool _tempch2sourcelocal = 0;
    bool _spinupdisable = 0;
    uint8_t _freqselect = 0x20;
    uint8_t _pwm1[10] = {0x6e, 0x0, 0x60, 0xf0, 0x0, 0x0, 0x5, 0x5, 0x0, 0xff};
    uint8_t _pwm2[10] = {0x6e, 0x0, 0x60, 0xf0, 0x0, 0x0, 0x5, 0x5, 0x0, 0xff};
    bool _fan1tachdisable = 0;
    bool _fan2tachdisable = 0;
    bool _measurefan1whenfullspeed = 0;
    bool _measurefan2whenfullspeed = 0;
    bool _maskfanfailpin = 0;
    bool _fanfailcrossfull = 0;
    bool _hysteresis = 0;
    bool _tempstep2c = 0;
    bool _fan1control1;
    bool _fan1control2;
    bool _fan2control1;
    bool _fan2control2;

    uint8_t reg02(bool POR) {
      return (_standby << 7) + (POR << 6) + (_timeout << 5) + (_fan1pwminvert << 4) + (_fan2pwminvert << 3) + (_mindutycycle << 2) + (_tempch2sourcelocal << 1) + (_spinupdisable);
    }
    uint8_t reg03() {
      return (uint8_t)_pwm1[TEMP_OT_LIMIT];
    }
    uint8_t reg04() {
      return (uint8_t)_pwm2[TEMP_OT_LIMIT];
    }
    uint8_t reg06() {
      return (uint8_t)(((_pwm1[OT_MASK] & 0x1) << 7) + ((_pwm2[OT_MASK] & 1) << 6));
    }
    uint8_t reg07() {
      return (uint8_t)_pwm1[PWM_START_DUTY_CYCLE];
    }
    uint8_t reg08() {
      return (uint8_t)_pwm2[PWM_START_DUTY_CYCLE];
    }
    uint8_t reg09() {
      return (uint8_t)_pwm1[PWM_MAX_DUTY_CYCLE];
    }
    uint8_t reg0a() {
      return (uint8_t)_pwm2[PWM_MAX_DUTY_CYCLE];
    }
    uint8_t reg0b() {
      return (uint8_t)_pwm1[PWM_TARGET_DUTY_CYCLE];
    }
    uint8_t reg0c() {
      return (uint8_t)_pwm2[PWM_TARGET_DUTY_CYCLE];
    }
    uint8_t reg0f() {
      return (uint8_t)_pwm1[FAN_START_TEMP];
    }
    uint8_t reg10() {
      return (uint8_t)_pwm2[FAN_START_TEMP];
    }
    uint8_t reg11() {
      return (uint8_t)((_hysteresis << 7) + (_tempstep2c << 6) + (_fan1control1 << 5) + (_fan1control2 << 4) + (_fan2control1 << 3) + (_fan2control2 << 2));
    }
    uint8_t reg12() {
      return (uint8_t)(((_pwm1[DUTY_CYCLE_RATE_OF_CHANGE] & 0x7) << 5) + ((_pwm2[DUTY_CYCLE_RATE_OF_CHANGE] & 0x7) << 2));
    }
    uint8_t reg13() {
      return (uint8_t)(((_pwm1[DUTY_CYCLE_STEP_SIZE] & 0xf) << 4) + ((_pwm2[DUTY_CYCLE_STEP_SIZE] & 0xf)));
    }
    uint8_t reg14() {
      return (uint8_t)_freqselect;
    };
    uint8_t reg15() {
      return 0;
    }
    uint8_t reg16() {
      return 0;
    }
    uint8_t reg17() {
      return (uint8_t)(((_pwm1[THERMISTEROFFSET] & 0xf) << 4) + ((_pwm2[THERMISTEROFFSET] & 0xf)));
    }
    uint8_t reg1a() {
      return (uint8_t)_pwm1[TACHLIMIT];
    }
    uint8_t reg1b() {
      return (uint8_t)_pwm2[TACHLIMIT];
    }
    uint8_t reg1c() {
      return (uint8_t)((_fan1tachdisable << 5) + (_fan2tachdisable << 4) + (_measurefan1whenfullspeed << 3) + (_measurefan2whenfullspeed << 2) + (_maskfanfailpin << 1) + (_fanfailcrossfull));
    }
    void i2cwrite(uint8_t regaddr, uint8_t regvalue);
    uint8_t i2cread(uint8_t regaddr);

    float _temperature[2];
    bool _otstatus[2];
    uint8_t  _dutycycle[2];
    uint8_t  _tach[2];

  protected:
  public:
    c_max6615(uint8_t i2caddr);
    void c_max6615::i2cinit(int clkfreq);
    void fanmanual(uint8_t chan, float ratiofullspeed);
    void softreset();
    void set_tempch2sourcelocal(bool tempch2sourcelocal);
    void set_freqselect(uint8_t freqselect);
    void readtemperature(uint8_t chan);
    void readinstantaneousdutycycle(uint8_t chan);
    void readtach(uint8_t chan);
    void readotstatus(uint8_t chan);
    float get_temperature(uint8_t chan);
    bool get_otstatus(uint8_t chan) ;
    uint8_t get_dutycycle(uint8_t chan) ;
    uint8_t get_tach(uint8_t chan);
    uint8_t i2creadreg(uint8_t addr);
};
