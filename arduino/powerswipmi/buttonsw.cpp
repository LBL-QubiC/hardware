#include "buttonsw.h"
void c_buttonsw::checkbutton(time64 tnow) {
  time64 tdiff = {0L};
  bool pbstatus = digitalRead(_pushbutton);
  if (pbstatus) {
    pblow_start = tnow;
  }
  else if (pbstatus == LOW & _pblast == HIGH) {
    _acted = 0;
  }
  else {
    tdiff.t64 = tnow.t64 - pblow_start.t64;
    if (((tdiff.tl[1]) >= 3) & _acted == 0) {
      swstatus = !swstatus;
      powerreq(swstatus, tnow, "PUSHBUTTON");
      _acted = 1;
    }
  }

  //  Serial.print("pushbutton: ");
  //  Serial.print(pbstatus);
  //  Serial.print(" acted ");
  //  Serial.print(_acted);
  //  Serial.print(" time since last low ");
  //  Serial.print(" tnow ");
  //  Serial.print(tnow.tl[1],HEX);Serial.print(".");Serial.print(tnow.tl[0],HEX);
  //  Serial.print(" pbstart ");
  //  Serial.print(pblow_start.tl[1],HEX);Serial.print(".");Serial.print(pblow_start.tl[0],HEX);
  //  Serial.print(" tdiff ");
  //  Serial.println(tdiff.tl[1],HEX);Serial.print(".");Serial.print(tdiff.tl[0],HEX);
  _pblast = pbstatus;
}

void c_buttonsw::power(bool onoff) {
  swstatus = onoff;
  //  jsondoc[String("Chassis_Power_switch")] = swstatus;
  //  Serial.println();
  //  Serial.print(info);
  //  Serial.print(" ");
  //  Serial.print(_powerswitchpin);
  //  Serial.print(" ");
  //  Serial.println(swstatus);
  //  Serial.print(" ");
  digitalWrite(_powerswitchpin, !swstatus);
  //  digitalWrite(16,HIGH);
  //  Serial.println("write high to 16");

}
bool c_buttonsw::powerreset(time64 timenow, int delaysec, String info) {
  bool done = false;
  if (_resetstart.tl[1] == 0) {
    _resetstart = timenow;
    powerreq(false, timenow, info);
  }
  time64 tdiff;
  tdiff.t64 = timenow.t64 - _resetstart.t64;
  if (tdiff.tl[1] >= delaysec) {
    powerreq(true, timenow, info);
    _resetstart.t64 = (unsigned long long)0L;
    done = true;
  }
  return done;
}
c_buttonsw::c_buttonsw(uint8_t pushbutton, uint8_t powerswitchpin) {
  _pushbutton = pushbutton;
  _powerswitchpin = powerswitchpin;
  pinMode(_pushbutton, INPUT);
  pinMode(_powerswitchpin, OUTPUT);
  digitalWrite(_powerswitchpin, HIGH);
}
void c_buttonsw::nextphead() {
  phead = (phead + 1) % FIFOLEN;
}
void c_buttonsw::nextptail() {
  if (! empty()) {
    ptail = (ptail + 1) % FIFOLEN;
  }
}
bool c_buttonsw::full() {
  return ((ptail + 1) % FIFOLEN) == phead;
}
bool c_buttonsw::empty() {
  return (ptail == phead);
}

void c_buttonsw::powerreq(bool sw, time64 treq, String info) {

  reqfifo[phead].sw = sw;
  reqfifo[phead].treq = treq;
  reqfifo[phead].info= info;
  Serial.print(" phead: ");
  Serial.print(phead);
  Serial.print(" ptail ");
  Serial.print(ptail);

  Serial.print(" req sw: ");
  Serial.print(reqfifo[phead].sw);
  Serial.print(" req info: ");
  Serial.println(reqfifo[phead].info);
  nextphead();
}
void c_buttonsw::powerprocess(time64 timenow) {
  if (! empty()) {
    power(reqfifo[ptail].sw);
    reqfifo[ptail].tproc.t64 = timenow.t64;
    Serial.print(" phead: ");
    Serial.print(phead);

    Serial.print(" ptail ");
    Serial.print(ptail);
    Serial.print(" process sw: ");
    Serial.print(reqfifo[ptail].sw);
    Serial.print(" proc info: ");
    Serial.println(reqfifo[ptail].info);
    nextptail();

  }
}
