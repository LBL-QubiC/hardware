#include "lm35.h"
c_lm35::c_lm35(uint8_t pin) {
  _debug=false;
  init(pin);
}
c_lm35::c_lm35(uint8_t pin,bool debug) {
  _debug=debug;
  init(pin);
}
void c_lm35::init(uint8_t pin){
  _pin=pin;
}

void c_lm35::readtemperature() {
  int reading = analogRead(_pin);
  float voltage = reading * (5000 / 1024.0);
  temperatureC = voltage / 10;
}
float c_lm35::get_temperatureC(){
  return temperatureC;
}
