#include "rmcp.h"

//c_ipmi15::c_ipmi15() {
//  ipmi = c_ipmi();
//}
int c_ipmi::process15wrap(unsigned char * rxbuf, unsigned char* txbuf) {
//  Serial.println("process ipmi15wrap");
  int rxcnt = 0;
  int txcnt = 0;
  uint8_t authtype;
  authtype = rxbuf[0]; rxcnt++; txbuf[0] = authtype; txcnt++;
  for (int i = 0; i < 4; i++) {
    _sessionsequencenumber[i] = rxbuf[i + rxcnt];
    txbuf[i + txcnt] = _sessionsequencenumber[i];
  }
  rxcnt += 4; txcnt += 4;
  for (int i = 0; i < 4; i++) {
    _sessionid[i] = rxbuf[i + rxcnt];
    txbuf[i + txcnt] = _sessionid[i];
  }
  rxcnt += 4; txcnt += 4;
  uint8_t authcode[16];
  if (authtype != 0) {
//    Serial.println("authcode");
    for (int i = 0; i < 16; i++) {
      authcode[i] = rxbuf[i + rxcnt], txbuf[i + txcnt] = authcode[i];
      _authgood = _authgood & (authcode[i] == authcode[i]);
//      Serial.print(authcode[i], HEX);
    };
    rxcnt += 16; txcnt += 16;
//    Serial.println();
  }

  else {
    rxcnt += 0; txcnt += 0;
  }
  _payloadlength = rxbuf[rxcnt];
  rxcnt++;



  int ipmitxcnt = process(&rxbuf[rxcnt], rxcnt, &txbuf[txcnt + 1]);
  txbuf[txcnt] = ipmitxcnt; txcnt++;
  txcnt += ipmitxcnt;
  return txcnt;



}

c_ipmi::c_ipmi() {
  set_powernoaction();
}
int c_ipmi::process(unsigned char * rxbuf, unsigned int rxlen, unsigned char* txbuf) {
//  Serial.println("process ipmi");
  int txcnt = 0;
  _rssa = rxbuf[0];
  _netfn_rslun = rxbuf[1];
  _rchecksum = rxbuf[2];
  _rqsa = rxbuf[3];
  _rqseq_rqlun = rxbuf[4];
  _cmd = rxbuf[5];
  _databuf = &rxbuf[6];
  _qchecksum = rxbuf[rxlen - 1];
//  Serial.println(rxlen, HEX);
//  Serial.println(_qchecksum, HEX);
//  Serial.print("rqsa:");
//  Serial.println(_rqsa, HEX);
  txbuf[0] = _rqsa;
  txbuf[1] = _netfn_rslun | 0x4;
  txbuf[2] = checksum(txbuf, 0, 1);
  txbuf[3] = _rssa;
  txbuf[4] = (_rqseq_rqlun & 0x3) + (seqno << 2);
  txbuf[5] = _cmd;
  seqno++;
  txcnt = 6;

  switch (_cmd) {
    case 0x38:  //Get Channel Authentication Capabilities
      txcnt += cmd38(_databuf, &txbuf[txcnt]);
      break;
    case 0x39: //Get Session Challenge
      txcnt += cmd39(_databuf, &txbuf[txcnt]);
      break;
    case 0x3a:
      txcnt += cmd3a(_databuf, &txbuf[txcnt]);
      break;
    case 0x3b:
      txcnt += cmd3b(_databuf, &txbuf[txcnt]);
      break;
    case 0x01:
      txcnt += cmd01(_databuf, &txbuf[txcnt]);
      break;
    case 0x02:
      txcnt += cmd02(_databuf, &txbuf[txcnt]);
      break;
    case 0x07:
      txcnt += cmd07(_databuf, &txbuf[txcnt]);
      break;


    default:
      txcnt = 0;
//      Serial.print("cmd ");
//      Serial.println(_cmd);


      break;
  }
  if (txcnt > 0.5) {
    txbuf[txcnt] = checksum(txbuf, 3, txcnt);
    txcnt++;
  }

  return txcnt;
}
uint8_t c_ipmi::checksum(unsigned char * txbuf, unsigned int startaddr, unsigned stopaddr) {
  uint8_t sum = 0;
  for (int i = startaddr; i <= stopaddr; i++) {
    sum += txbuf[i];
  }
  return (~sum) + 1;
}
int c_ipmi::cmd38(unsigned char * rxbuf, unsigned char * txbuf) {
  int txcnt = 0;
  uint8_t channelnumber = rxbuf[0];
  uint8_t requestmaximumlevel = rxbuf[1];
  uint8_t completed = 0;
  txbuf[0] = completed;
  txcnt++;
  txbuf[1] = _channel;
  txbuf[2] = _authtype;
  txbuf[3] = _anonymous;
  txbuf[4] = _supportversion;
  for (int i = 5; i < 9; i++) {
    txbuf[i] = 0;
  }
  txcnt += 8;
  return txcnt;
}
int c_ipmi::cmd39(unsigned char * rxbuf, unsigned char * txbuf) {
  int txcnt = 0;
  uint8_t reqauthtype = rxbuf[0];
  uint8_t *username = &rxbuf[1];
  bool usernamegood = true;
  uint8_t completed;
  for (int i = 0; i < 16; i++) {
    usernamegood = usernamegood && (username[i] == _username[i]);
  }
  if (usernamegood) {
    completed = 0;
  }
  else {
    completed = 0x81;
  }
  txbuf[0] = completed; txcnt++;
  unsigned int ran;
  for (int i = 0; i < 4; i++) {
    ran = (unsigned int)random(0, 256);
    _sessionid[i] = ran;
    txbuf[i + txcnt] = ran;
  }
  txcnt += 4;
  for (uint8_t i = 0; i < 16; i++) {
    ran = (unsigned int)random(0, 256);
    _challengestring[i] = ran;
    txbuf[i + txcnt] = ran;
  }
  txcnt += 16;
  return txcnt;
}
int c_ipmi::cmd3a(unsigned char * rxbuf, unsigned char * txbuf) {
//  Serial.println("cmd3a.........");
  uint8_t rxptr = 0;
  uint8_t txcnt = 0;
//  Serial.print("authstatus:");
//  Serial.println(_authgood);
  uint8_t completed = 0;
  bool good = true;

  good = good && _authgood;
  good = good && (rxbuf[0] == _authtype);
  rxptr = 2;
  for (int i = 0; i < 16; i++) {
    good = good && (rxbuf[rxptr + i] == _challengestring[i]);
  }


  txbuf[0] = completed; txcnt++;
  _maxprivilege = rxbuf[1];
  txbuf[1] = _authtype; txcnt++;
  for (int i = 0; i < 4; i++) {
    txbuf[txcnt + i] = _sessionid[i];
  }
  txcnt += 4;
  for (int i = 0; i < 4; i++) {
    txbuf[txcnt + i] = (_inboundseq >> (3 - i)) & 0xff;
  }
  _inboundseq++;
  txcnt += 4;
  txbuf[0] = completed;
  txbuf[txcnt] = _maxprivilege;
  txcnt++;
  return txcnt;

}

int c_ipmi::cmd3b(unsigned char * rxbuf, unsigned char * txbuf) {
//  Serial.println("cmd3b.........");
  uint8_t rxptr = 0;
  uint8_t txcnt = 0;
//  Serial.print("authstatus:");
//  Serial.println(_authgood);
  uint8_t completed = 0;
  uint8_t reqprivilege = rxbuf[0];
  bool good = true;
  good = good && (reqprivilege <= _maxprivilege);
  good = good && _authgood;
  if (!good) {
    completed = 0x80;
  }
  txbuf[txcnt] = completed; txcnt++;
  txbuf[txcnt] = reqprivilege;  txcnt++;
  return txcnt;


}
int c_ipmi::cmd01(unsigned char * rxbuf, unsigned char * txbuf) {
//  Serial.println("cmd01.........");
  uint8_t completed = 0;
  bool good = true;
    uint8_t txcnt = 0;
  good = good && _authgood;
  if (!good) {
    completed = 0x80;
  }
  txbuf[0] = completed; txcnt++;
  txbuf[txcnt]=_deviceid; txcnt++;
  txbuf[txcnt]=_devicerevision; txcnt++;
  txbuf[txcnt]=_firmwarerevision1; txcnt++;
  txbuf[txcnt]=_firmwarerevision2; txcnt++;
  txbuf[txcnt]=_ipmiversion; txcnt++;
  txbuf[txcnt]=_additionaldevicesupport; txcnt++;
  for (int i=0;i<3;i++){
    txbuf[txcnt+i]=_manufacturerid[i];
  }
  txcnt+=3;
  for (int i=0;i<3;i++){
  txbuf[txcnt+i]=  _productid[i];
  }
  txcnt+=2;
 

  return txcnt;
  

}

int c_ipmi::cmd02(unsigned char * rxbuf, unsigned char * txbuf) {
//  Serial.println("cmd02.........");
  uint8_t completed = 0;
  bool good = true;
    uint8_t txcnt = 0;
  good = good && _authgood;
  _poweraction=rxbuf[0];
  if (!good) {
    completed = 0x80;
  }
  txbuf[txcnt]=completed;txcnt++;
  return txcnt;

}

int c_ipmi::cmd07(unsigned char * rxbuf, unsigned char * txbuf) {
//  Serial.println("cmd07.........");
  uint8_t completed = 0;
  bool good = true;
    uint8_t txcnt = 0;
  good = good && _authgood;
  if (!good) {
    completed = 0x80;
  }
  txbuf[txcnt]=completed;txcnt++;
  txbuf[txcnt]=_acpisystempowerstate; txcnt++;
  txbuf[txcnt]=_acpidevicepowerstate; txcnt++;
  return txcnt;

}

int c_ipmi::set_acpi_device_power_state(unsigned int acpidevicepowerstate){
  _acpidevicepowerstate=acpidevicepowerstate;
}

int c_ipmi::set_acpi_system_power_state(unsigned int acpisystempowerstate){
  _acpisystempowerstate=acpisystempowerstate;
}

int c_ipmi::set_channel(unsigned int chan) {
  _channel = chan;
}

int c_ipmi::get_poweraction(){
  return _poweraction;
}
void c_ipmi::set_powernoaction(){
  _poweraction=0xff;
}
