#include <DHT.h>
#define DHTTYPE DHT22
class c_dht22 {
  private:
    bool _debug;
    float _temperature;
    float _humidity;
    void init(uint8_t pin);
    DHT * dht;
  public:
    c_dht22(uint8_t pin);
    c_dht22(uint8_t pin, bool debug);
    void readhardware();
    float get_temperature();
    float get_humidity();
};
