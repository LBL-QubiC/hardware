#include "udprmcp.h"
void c_udprmcp::init(char * rxbuf, char *txbuf, unsigned int maxbufsize){
  udp=new EthernetUDP();
  udp->begin(RMCPPORT);
  _rxbuf=rxbuf;
  _txbuf=txbuf;
  _maxbufsize=maxbufsize;
  rmcp=new c_rmcp();
  rmcp->ipmi.set_channel(1);
}
c_udprmcp::c_udprmcp(char * rxbuf, char *txbuf, unsigned int maxbufsize){
  _debug=false;
  init(rxbuf,txbuf,maxbufsize);
}
c_udprmcp::c_udprmcp(char * rxbuf, char *txbuf, unsigned int maxbufsize, bool debug){
  _debug=debug;
  init(rxbuf,txbuf,maxbufsize);  
}
int c_udprmcp::parsePacket(){
  int res=0;
  
  if (res=udp->parsePacket()){
    _remoteip=udp->remoteIP();
    _remoteport=udp->remotePort();
    _rxlen=udp->read(_rxbuf,_maxbufsize);
  }
  return res;
}
void c_udprmcp::response(){
  _txlen=rmcp->process(_rxbuf,_rxlen,_txbuf,_maxbufsize);
  if (_txlen>0.5){
         udp->beginPacket(_remoteip, _remoteport);
        udp->write(_txbuf, _txlen);        
        udp->endPacket();    
  }
  
}
String c_udprmcp::info(){
  return "ipmi from:"+String(_remoteip,HEX);
}
