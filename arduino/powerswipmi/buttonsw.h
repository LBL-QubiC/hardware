#include "ntp.h"
typedef struct powerreq{
  bool sw;
  time64 treq;
  time64 tproc;
  String info;  
};
#define FIFOLEN 8
class c_buttonsw {
  private:
    bool _pblast;
    time64 pblow_start;
    bool _swstatus;
    bool _acted;
    uint8_t _pushbutton;
    bool swstatus;
    uint8_t _powerswitchpin;
    powerreq reqfifo[FIFOLEN];
    uint8_t phead;
    uint8_t ptail;

    void nextphead();
    void nextptail();
    bool empty();
    bool full();
    time64 _resetstart;
  protected:
  public:
    c_buttonsw(uint8_t pushbutton,uint8_t powerswitchpin);
    void checkbutton(time64 tnow) ;
    void power(bool onoff) ;
    bool powerreset(time64, int delayms, String info) ;
    void powerreq(bool,time64,String info);
    void powerprocess(time64);
    void timedpowerreq(time64,bool);

};
