#include <EthernetUdp.h>
#include "rmcp.h"
#define RMCPPORT 623

class c_udprmcp {
  private:
    bool _debug;
    char * _rxbuf;
    char * _txbuf;
    EthernetUDP *udp;
    unsigned int _maxbufsize;
    IPAddress _remoteip;
    unsigned int _remoteport;
    unsigned int _rxlen;
    unsigned int _txlen;

    void init(char * rxbuf, char *txbuf, unsigned int maxbufsize);

  public:
    c_rmcp *rmcp;
    c_udprmcp(char * rxbuf, char *txbuf, unsigned int maxbufsize);
    c_udprmcp(char * rxbuf, char *txbuf, unsigned int maxbufsize, bool debug);
    int parsePacket();
    void c_udprmcp::response();
    String c_udprmcp::info();
};
