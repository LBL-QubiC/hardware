#include "Adafruit_MAX31855.h"


class c_max31855{
  private:
    bool _debug;
    void init(uint8_t cspin);
    float _internal_temperature;
    float _temperature;
    Adafruit_MAX31855 * max31855;
  public:
    c_max31855(uint8_t cspin);
    c_max31855(uint8_t cspin, bool debug);
    void readmax31855();
    float get_internal_temperature();
    float get_temperature();
};
