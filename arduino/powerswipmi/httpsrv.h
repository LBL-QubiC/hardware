#include <Ethernet.h>
//#include <ArduinoJson.h>
#define HTTPPORT 80

class c_httpsrv {
  private:
    bool _debug;
    EthernetServer *http;
    
    void init (char * rxbuf, char * txbuf,unsigned int maxbufsize);
    char *_rxbuf;
    char *_txbuf;
    unsigned int _rxlen;
    unsigned int _txlen;
    unsigned int  _maxbufsize;
  public:
    c_httpsrv(char * rxbuf, char * txbuf,unsigned int maxbufsize);
    c_httpsrv(char * rxbuf, char * txbuf,unsigned int maxbufsize,bool debug);
    EthernetClient available();
//    void response(EthernetClient client, DynamicJsonDocument jsondoc);
    void response(EthernetClient client, String);

};
