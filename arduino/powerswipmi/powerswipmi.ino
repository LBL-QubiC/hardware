#include "board.h"

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Serial start");
  c_board *pbd;
  Serial.print("init board...");
  pbd = new c_board();
  
  int loopcnt = 0;
  int chan6615 = 0;
  int chan6615loop = 0;
  int chanloop = 0;
  int max6615idloop = 0;
  unsigned long lastupdate=0;
  unsigned long abssecond=0;
  time64 abstime;
  Serial.println("init-board done");
  while (true) {
    
    abstime=pbd->ntp->abstime();
    for (int i=0;i<8;i++) Serial.println(abstime.bytes[i]);
    abssecond=abstime.tl[1];
    pbd->buttonsw->checkbutton(abstime);
    
    switch (loopcnt) {
      case 0: {
          pbd->ds18b20s->reuestTemperatures(-1);
          break;
        };
      case 1: {
          pbd->lm35->readtemperature();
          break;
        };
      case 2: {
          pbd->dht22->readhardware();
          break;
        }
      case 3: {
          pbd->max31855->readmax31855();
          break;
        };
      case 4: {
          chan6615loop = (chan6615loop == 5) ? 0 : chan6615loop + 1;
          max6615idloop = chan6615loop >> 1;
          chanloop = chan6615loop & 0x1;
        }
      case 5: {
          pbd->max6615[max6615idloop]->readtemperature(chanloop);
          break;
        };
      case 6: {
          pbd->max6615[max6615idloop]->readinstantaneousdutycycle(chanloop);
          break;
        };
      case 7: {
          pbd->max6615[max6615idloop]->readtach(chanloop);
          break;
        };
      case 8: {
          pbd->max6615[max6615idloop]->readotstatus(chanloop);
          break;
        };
      case 9: {
          pbd->ds18b20s->getTemperatures(0);
          break;
        };
      case 10: {
          pbd->ds18b20s->getTemperatures(1);
          break;
        };

      case 11: {
          pbd->updateboardvalue(pbd->ntp->unixtimestamp(abssecond));
          break;
        }
      case 12:{
        
//        Serial.print(abssecond);
//        Serial.print(">?");
//        Serial.println(lastupdate);
        if ((abssecond>lastupdate)&&(abssecond%5==0)){
          lastupdate=abssecond;
          pbd->ntp->request();
        }
        
      }
      default: {
          break;
        }
    }
    loopcnt = (loopcnt == 12) ? 0 : loopcnt + 1;
    //    Serial.print(loopcnt);
    //    Serial.print(":");
    //    Serial.println(millis());
    
    if (EthernetClient client = pbd->httpsrv->available()) {
      Serial.println("http alive");
      pbd->httpsrv->response(client,pbd->serialize());
      
    }
    else if (pbd->udprmcp->parsePacket()){
      pbd->udprmcp->response();
    }
    else if (pbd->ntp->parsePacket()){
      Serial.println("NTP");
    }

      int ipmipoweraction = pbd->udprmcp->rmcp->ipmi.get_poweraction();
  //  Serial.println(ipmipoweraction);
  if (ipmipoweraction == 0) {
    pbd->buttonsw->powerreq(false, abstime,pbd->udprmcp->info());
    pbd->udprmcp->rmcp->ipmi.set_powernoaction();
  }
  else if (ipmipoweraction == 1) {
    pbd->buttonsw->powerreq(true,abstime,pbd->udprmcp->info());
    pbd->udprmcp->rmcp->ipmi.set_powernoaction();
  }
  else if (ipmipoweraction == 3) {
    if (pbd->buttonsw->powerreset(abstime,5, pbd->udprmcp->info())){
      pbd->udprmcp->rmcp->ipmi.set_powernoaction();
    }
  }
  else {
    //    rmcp->ipmi.set_powernoaction();

  }
  pbd->buttonsw->powerprocess(abstime);

    


  }
}

void loop() {
  // put your main code here, to run repeatedly:
  //  pbd->ds18b20_readhardware(0);
  //  Serial.println(pbd->ds18b20_temperature(0));

}
