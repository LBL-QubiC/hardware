#include "board.h"

c_board::c_board(bool debug) {
  _debug = debug;
  init();
}
c_board::c_board() {
  _debug = false;
  init();
}
void c_board::init() {
  ds18b20s = new c_ds18b20s(ONE_WIRE_BUS, _debug);
  lm35 = new c_lm35(LM35Pin, _debug);
  dht22 = new c_dht22(DHTPIN, _debug);

  max6615[0]->i2cinit(400000);
  for (int i = 0; i < 3; i++) {
    max6615[i] = new c_max6615(0x18 + i);
    max6615[i]->softreset();
    max6615[i]->set_freqselect(0x20);
    max6615[i]->set_tempch2sourcelocal(1);
    max6615[i]->fanmanual(1, 0.12 + i / 10.0);
    max6615[i]->fanmanual(0, 0.28 + i / 10.0);
  }
  max31855 = new c_max31855(MAXCS);
  eeprometh = new c_eeprometh(ETHCS);
  //  initjsontx();

  rxbuf = new char [MAX_UDP_PACKET_SIZE];
  txbuf = new char [MAX_UDP_PACKET_SIZE];
  httpsrv = new c_httpsrv(rxbuf, txbuf, MAX_UDP_PACKET_SIZE);
  udprmcp = new c_udprmcp(rxbuf, txbuf, MAX_UDP_PACKET_SIZE);
  ntp = new c_ntp(rxbuf, txbuf, MAX_UDP_PACKET_SIZE);
  ntp->request();
  buttonsw = new c_buttonsw(PUSHBUTTON, PS_ON_N);
  //  mqtt = new c_mqtt();

}
//
//void c_board::initjsontx() {
//  //  const size_t capacity = JSON_ARRAY_SIZE(2) + JSON_ARRAY_SIZE(3) + JSON_OBJECT_SIZE(2) + 3 * JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5);
//  jsontx = new DynamicJsonDocument(1000);
//
//  JsonArray ds18b20 = jsontx->createNestedArray("ds18b20");
//  ds18b20.add(0.0);
//  ds18b20.add(0.0);
//  (*jsontx)["LM35"] = 0.0;
//  (*jsontx)["MAX31855"] = 0.0;
//
//  JsonObject dht22 = jsontx->createNestedObject("dht22");
//  dht22["Humidity"] = 0.0;
//  dht22["Temperature"] = 0.0;
//
//  JsonArray max6615s = jsontx->createNestedArray("max6615s");
//  JsonObject max6615[6];
//  for (int i = 0; i < 6; i++) {
//    max6615[i] = max6615s.createNestedObject();
//    max6615[i]["T"] = 0.0;
//    max6615[i]["Dutycycle"] = 0.0;
//    max6615[i]["Tach"] = 0;
//    max6615[i]["OT"] = false;
//  }
//}

//void c_board::updatejsontx(unsigned long unixsecond) {
//  (*jsontx)["secondsince1970"] = unixsecond;
//  for (int i = 0; i < 2; i++) {
//    (*jsontx)["ds18b20"][i] = ds18b20s->temperature(i);
//  }
//  (*jsontx)["LM35"] = lm35->get_temperatureC();
//  (*jsontx)["MAX31855"] = max31855->get_internal_temperature();
//  (*jsontx)["dht22"]["Humidity"] = dht22->get_humidity();
//  (*jsontx)["dht22"]["Temperature"] = dht22->get_temperature();
//  for (int max6615id = 0; max6615id < 3; max6615id++) {
//    for (int chan = 0; chan < 2; chan++) {
//      (*jsontx)["max6615s"][max6615id * 2 + chan]["T"] = max6615[max6615id]->get_temperature(chan);
//      (*jsontx)["max6615s"][max6615id * 2 + chan]["Dutycycle"] = max6615[max6615id]->get_dutycycle(chan);
//      (*jsontx)["max6615s"][max6615id * 2 + chan]["Tach"] = max6615[max6615id]->get_tach(chan);
//      (*jsontx)["max6615s"][max6615id * 2 + chan]["OT"] = max6615[max6615id]->get_otstatus(chan);
//    }
//  }
//  //  serializeJson((*jsontx), Serial);
//  //  Serial.println();
//}


void c_board::updateboardvalue(unsigned long unixsecond) {
  boardvalue.unixsecond = unixsecond;
  for (int i = 0; i < 2; i++) {
    boardvalue.ds18b20[i] = ds18b20s->temperature(i);
  }
  boardvalue.LM35 = lm35->get_temperatureC();
  boardvalue.MAX31855 = max31855->get_internal_temperature();
  boardvalue.dht22.Humidity = dht22->get_humidity();
  boardvalue.dht22.Temperature = dht22->get_temperature();
  for (int max6615id = 0; max6615id < 3; max6615id++) {
    for (int chan = 0; chan < 2; chan++) {
      boardvalue.max6615s[max6615id].chan[chan].temperature = max6615[max6615id]->get_temperature(chan);
      boardvalue.max6615s[max6615id].chan[chan].dutycycle = max6615[max6615id]->get_dutycycle(chan);
      boardvalue.max6615s[max6615id].chan[chan].tach = max6615[max6615id]->get_tach(chan);
      boardvalue.max6615s[max6615id].chan[chan].otstatus = max6615[max6615id]->get_otstatus(chan);
    }
  }
}

String c_board::serialize() {
  const char * templete = R"=====({
"ds18b20":[
%s,
%s
],
"LM35":%s,
"MAX31855":%s,
"dht22":{
"Humidity":%s,
"Temperature":%s
},
"max6615s":[
{
"T":%s,
"Dutycycle":%s,
"Tach":%s,
"OT":%s
},
{
"T":%s,
"Dutycycle":%s,
"Tach":%s,
"OT":%s
},
{
"T":%s,
"Dutycycle":%s,
"Tach":%s,
"OT":%s
}
],
"secondsince1970":%s
}
  )=====";
//  {
//"T":%s,
//"Dutycycle":%s,
//"Tach":%s,
//"OT":%s
//},
//{
//"T":%s,
//"Dutycycle":%s,
//"Tach":%s,
//"OT":%s
//},
//{
//"T":%s,
//"Dutycycle":%s,
//"Tach":%s,
//"OT":%s
//},

  char  ret[1000];
  sprintf(ret, templete
          , String(boardvalue.ds18b20[0]).c_str()
          , String(boardvalue.ds18b20[1]).c_str()
            , String(boardvalue.LM35).c_str()
            , String(boardvalue.MAX31855).c_str()
            , String(boardvalue.dht22.Humidity).c_str()
            , String(boardvalue.dht22.Temperature).c_str()
            , String(boardvalue.max6615s[0].chan[0].temperature).c_str()
            , String(boardvalue.max6615s[0].chan[0].dutycycle).c_str()
            , String(boardvalue.max6615s[0].chan[0].tach).c_str()
            , String(boardvalue.max6615s[0].chan[0].otstatus).c_str()
            , String(boardvalue.max6615s[0].chan[1].temperature).c_str()
            , String(boardvalue.max6615s[0].chan[1].dutycycle).c_str()
            , String(boardvalue.max6615s[0].chan[1].tach).c_str()
            , String(boardvalue.max6615s[0].chan[1].otstatus).c_str()
//            , String(boardvalue.max6615s[1].chan[0].temperature).c_str()
//            , String(boardvalue.max6615s[1].chan[0].dutycycle).c_str()
//            , String(boardvalue.max6615s[1].chan[0].tach).c_str()
//            , String(boardvalue.max6615s[1].chan[0].otstatus).c_str()
//            , String(boardvalue.max6615s[1].chan[1].temperature).c_str()
//            , String(boardvalue.max6615s[1].chan[1].dutycycle).c_str()
//            , String(boardvalue.max6615s[1].chan[1].tach).c_str()
//            , String(boardvalue.max6615s[1].chan[1].otstatus).c_str()
//            , String(boardvalue.max6615s[2].chan[0].temperature).c_str()
//            , String(boardvalue.max6615s[2].chan[0].dutycycle).c_str()
//            , String(boardvalue.max6615s[2].chan[0].tach).c_str()
//            , String(boardvalue.max6615s[2].chan[0].otstatus).c_str()
            , String(boardvalue.max6615s[2].chan[1].temperature).c_str()
            , String(boardvalue.max6615s[2].chan[1].dutycycle).c_str()
            , String(boardvalue.max6615s[2].chan[1].tach).c_str()
            , String(boardvalue.max6615s[2].chan[1].otstatus).c_str()
            
            
          
            , String(boardvalue.unixsecond).c_str()          
         );

  Serial.println(String(ret));
  Serial.println("exit serialize");
  
  return String(ret);
}
