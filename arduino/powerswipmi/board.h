
#include <stdint.h>

#include "max6615.h"
#include "dht22.h"
#include "lm35.h"
#include "ds18b20s.h"
#include "max31855.h"
#include "eeprometh.h"
#include <ArduinoJson.h>
#include "httpsrv.h"
#include "udprmcp.h"
#include "ntp.h"
#include "buttonsw.h"
//#include "mqtt.h"

//#include <ArduinoJson.h>
//#include <ArduinoMqttClient.h>
//#include <EthernetUdp.h>
//#include <EEPROM.h>

typedef struct dht22_t{
  float Temperature;
  float Humidity;  
};
typedef struct chan_max6615_t{
  float temperature;
  float dutycycle;
  float tach;
  bool otstatus;
};
typedef struct para_max6615{
  chan_max6615_t chan[2];  
};
typedef struct boardvalue_t{
  unsigned long unixsecond;
  float ds18b20[2];
  float LM35;
  float MAX31855;
  dht22_t dht22;
  para_max6615 max6615s[3];
};
  
  
  

#define LM35Pin A3
#define PS_ON_N 16
#define PUSHBUTTON 7
#define ONE_WIRE_BUS 4
#define DHTPIN 3
#define MAXCS   6
#define ETHCS 8
#define MAX_UDP_PACKET_SIZE 512
class c_board {
  private:
    bool _debug;

//    void initjsontx();
    void init();
    char * rxbuf;
    char * txbuf;
  protected:
  public:
//    DynamicJsonDocument * jsontx;
//    void updatejsontx(unsigned long);
    boardvalue_t boardvalue;
    void updateboardvalue(unsigned long);
    c_ds18b20s *ds18b20s;
    c_lm35 *lm35;
    c_dht22 *dht22;
    c_max6615 *max6615[3];
    c_max31855 *max31855;
    c_eeprometh *eeprometh;
    c_board();
    c_board(bool debug);
    c_httpsrv *httpsrv;
    c_udprmcp *udprmcp;
    c_ntp *ntp;
    c_buttonsw *buttonsw;
//    c_mqtt * mqtt;
    String serialize();
};
