#include "rmcp.h"
#include <EthernetUdp.h>
#include <Ethernet.h>
c_rmcp::c_rmcp() {
  asf= c_asf();
  ipmi= c_ipmi();
}
int c_rmcp::port() {
  return _port;
}
int c_rmcp::process(unsigned char* rxbuf, int rxlen, unsigned char * txbuf, int maxtxlen) {
  int txlen;
  _version = rxbuf[0];
  _reserved = rxbuf[1];
  _sequencenumber = rxbuf[2];
  _classofmessage = rxbuf[3];

  int payloadlen;
  if (_classofmessage == 0x06) {
    txlen=0;
    if (_sequencenumber != 0xff) {
      txlen += ack(&rxbuf[0], &txbuf[0]);
    }
    else {
      txbuf[0] = _version;
      txbuf[1] = _reserved;
      txbuf[2] = _sequencenumber;
      txbuf[3] = _classofmessage;
      txlen = 4;
      txlen += asf.process(&rxbuf[4], &txbuf[4]);
    }

  }
  else if (_classofmessage == 0x07) {
    
      txbuf[0] = _version;
      txbuf[1] = _reserved;
      txbuf[2] = _sequencenumber;
      txbuf[3] = _classofmessage;

      txlen=4;

    txlen += ipmi.process15wrap(&rxbuf[4], &txbuf[txlen]);
  }
//  Serial.print("rmcp process txlen:");
//  Serial.println(txlen);
  return txlen;
}
int c_rmcp::ack(unsigned char * rxbuf, unsigned char * txbuf) {
  int txcnt = 0;
  txbuf[0] = _version;
  txbuf[1] = _reserved;
  txbuf[2] = _sequencenumber;
  txbuf[3] = _classofmessage | 0x80;
  txcnt = 4;

  return txcnt;
}


c_asf::c_asf() {

}
int c_asf::process(unsigned char * rxbuf, unsigned char* txbuf) {
  int txcnt = 0;
  for (int i = 0; i < 4; i++) {
    _ianaenterprisenumber[i] = rxbuf[i];
  }
  _messagetype = rxbuf[4];
  _messagetag = rxbuf[5];
  _reserved = rxbuf[6];
  _datalength = rxbuf[7];

  if (_messagetype == 0x80) {
    txcnt += pingpong(&rxbuf[0], &txbuf[0]);
  }
  else {

  }
  return txcnt;
}
int c_asf::pingpong(unsigned char * rxbuf, unsigned char * txbuf) {
  int txcnt = 0;
  txcnt = 24;
  for (int i = 0; i < txcnt; i++) {
    if (i < 4 || i == 5) {
      txbuf[i] = rxbuf[i];
    }
    else if (i == 4) {
      txbuf[i] = 0x40;
    }
    else if (i == 7) {
      txbuf[i] = 16;
    }
    else if (i == 8 || i == 9) {
      txbuf[i] = 0;
    }
    else if (i == 10) {
      txbuf[i] = 0x11;
    }
    else if (i == 11) {
      txbuf[i] = 0xbe;
    }
    else if (i == 6 || (i >= 12 && i < 16) || (i > 16)) {
      txbuf[i] = 0;
    }
    else if (i == 16) {
      txbuf[i] = 0x81;
    }
  }
  return txcnt;
}
