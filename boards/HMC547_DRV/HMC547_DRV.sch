EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2850 3300 0    50   Input ~ 0
CTRL_CMOS
$Comp
L 000_symbol:YDZVFHTR5.1 D1
U 1 1 5D33FF6F
P 3000 3300
F 0 "D1" H 3000 3516 50  0000 C CNN
F 1 "YDZVFHTR5.1" H 3000 3425 50  0000 C CNN
F 2 "" H 3000 3300 50  0001 C CNN
F 3 "~" H 3000 3300 50  0001 C CNN
	1    3000 3300
	1    0    0    -1  
$EndComp
$Comp
L dk_Logic-Gates-and-Inverters:SN74HC04N U?
U 1 1 5D340619
P 4150 3600
F 0 "U?" H 4150 4403 60  0000 C CNN
F 1 "SN74HC04N" H 4150 4297 60  0000 C CNN
F 2 "digikey-footprints:DIP-14_W3mm" H 4350 3800 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc04" H 4350 3900 60  0001 L CNN
F 4 "296-1566-5-ND" H 4350 4000 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74HC04N" H 4350 4100 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4350 4200 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 4350 4300 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc04" H 4350 4400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74HC04N/296-1566-5-ND/277212" H 4350 4500 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER 6CH 6-INP 14DIP" H 4350 4600 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 4350 4700 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4350 4800 60  0001 L CNN "Status"
	1    4150 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3300 4450 3350
Wire Wire Line
	4450 3350 3850 3350
Wire Wire Line
	3850 3350 3850 3400
$Comp
L Device:R R1
U 1 1 5D3441FE
P 3400 3450
F 0 "R1" H 3470 3496 50  0000 L CNN
F 1 "R" H 3470 3405 50  0000 L CNN
F 2 "" V 3330 3450 50  0001 C CNN
F 3 "~" H 3400 3450 50  0001 C CNN
	1    3400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3300 3400 3300
Connection ~ 3400 3300
Wire Wire Line
	3400 3300 3850 3300
Text HLabel 3900 3000 0    50   Input ~ 0
gnd
Text HLabel 3250 4100 0    50   Input ~ 0
DC-5V
Text HLabel 4750 3300 2    50   Output ~ 0
A
Text HLabel 4750 3400 2    50   Output ~ 0
B
Wire Wire Line
	4450 3300 4750 3300
Connection ~ 4450 3300
Wire Wire Line
	4450 3400 4750 3400
Wire Wire Line
	3900 3000 4150 3000
Wire Wire Line
	3400 3600 3400 4100
Wire Wire Line
	3400 4100 4150 4100
Wire Wire Line
	3250 4100 3400 4100
Connection ~ 3400 4100
$EndSCHEMATC
