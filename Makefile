SUBMODULES:=boards/downconverter boards/powerdistributionboard boards/upconverter_noamp boards/upconverter_yesamp 
AUTOCOMMITMSG=AUTOUPDATESUBMODULE
COMMITMSG = $(shell git log --pretty=oneline --abbrev-commit |head -1 | awk '{print $$2}')

submoduleupdate:
	$(for f in $(SUBMODULES); do echo $f; git add $f; done)
ifeq ($(COMMITMSG),$(AUTOCOMMITMSG))
	echo yes
	git reset --soft HEAD^
	-git commit -a -m $(AUTOCOMMITMSG)
else
	echo no,
	echo 'autocommitmsg',$(AUTOCOMMITMSG)
	echo 'commitmsg',$(COMMITMSG)
	-git commit -a -m $(AUTOCOMMITMSG)
endif
	-git commit -a -m $(AUTOCOMMITMSG)
	#for f in $(SUBMODULES); do git add $(f); done
